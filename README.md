# jweave

A suite of open source weaving tools - Warp/Weave yardage calculator, WIF Reader and Writer

* Drafts
    * [Draft Making](#draft-making) 
* WIF Format
    * [WIF Reading](#wif-reading)
    * [WIF Optimization](#wif-optimization)
        * [Optimize Everything](#optimize-everything)
        * [Optimize Particular Sections](#optimize-particular-sections)


### License 
![License](https://gitlab.com/ccook/jweave/-/raw/2228466383f385aff0e765a647379942077e4037/docs/license.png)


### Draft Making

* Example: [test1.wif](https://gitlab.com/ccook/jweave/-/blob/primary/src/test/resources/test1.wif)
* Comprehensive Test: [LoomSimulatorTest.java](https://gitlab.com/ccook/jweave/-/blob/primary/src/test/java/com/gitlab/ccook/jweave/LoomSimulatorTest.java)

```java
WIFReader r = new WIFReader(is);
LoomSimulator sim = new LoomSimulator(r);
DrawDownSimulator drawDown = sim.getDrawDown();
//2480 pixels x 3508 pixels
int width = 3508;
int height = 2480;
// save the draw down image
BufferedImage bufferedImage = drawDown.makeImage(width, height);
ImageIO.write(bufferedImage, "png", new File(test1Directory, "test1.png"));

width = 3508;
height = 500;
// save the threading image
BufferedImage im = sim.getThreading().makeImage(width, height);
ImageIO.write(im, "png", new File(test1Directory, "test1_threading.png"));

width = 200;
height = 2480;
// save the treadling image
im = sim.getTreadling().makeImage(width, height);
ImageIO.write(im, "png", new File(test1Directory, "test1_treadling.png"));

width = 500;
height = 500;
// save the tie up image
im = sim.getTieUp().makeImage(width, height);
ImageIO.write(im, "png", new File(test1Directory, "test1_tieup.png"));

```

#### Drawn Down

<img src="https://gitlab.com/ccook/jweave/-/raw/primary/src/test/resources/test1.png?inline=false"  width="500" height="500">

#### Threading

<img src="https://gitlab.com/ccook/jweave/-/raw/primary/src/test/resources/test1_threading.png?inline=false"  width="900" height="300">

#### Treadling

<img src="https://gitlab.com/ccook/jweave/-/raw/primary/src/test/resources/test1_treadling.png?inline=false"  width="200" height="900">

#### Tieup

<img src="https://gitlab.com/ccook/jweave/-/raw/primary/src/test/resources/test1_tieup.png?inline=false"  width="500" height="500">



### WIF Reading

Use the WIFReader to access the low level details of your WIF file.

* Example: [test1.wif](https://gitlab.com/ccook/jweave/-/blob/primary/src/test/resources/test1.wif)
* Comprehensive Test: [WIFReaderTest.java](https://gitlab.com/ccook/jweave/-/blob/primary/src/test/java/com/gitlab/ccook/jweave/WIFReaderTest.java)

```java
WIFReader r = new WIFReader(inputStream);

// HEADER
WIFHeader header = r.getHeader();
String version = header.getVersion(); // 1.1
String developers = header.getDevelopers(); // wif@mh.com
String sourceProgram = header.getSourceProgram(); // Fiberworks PCW
Option<String> sourceVersion = header.getSourceVersion(); // 4.2

// CONTENTS
WIFContents contents = r.getContents(); // THREADING, WEFT, TIEUP, WARP_SPACING
                                        // TEXT, COLOR_TABLE, TREADLING, WEAVING
                                        // COLOR_PALETTE, WARP_COLORS, WEFT_COLORS
                                        // WARP, WEFT_SPACING

// COLOR PALETTE
Option<WIFColorPalette> colorPaletteOption = r.getColorPalette();
WIFColorPalette colorPalette = colorPaletteOption.get();
Pair<Integer, Integer> range = colorPalette.getRange(); // 0 - 999

// TEXT
Option<WIFText> textOption = r.getText();
WIFText text = textOption.get();
Option<String> address = text.getAddress(); // not defined
Option<String> author = text.getAuthor(); // not defined
Option<String> email = text.getEmail(); // not defined
Option<String> fax = text.getFax(); // not defined
Option<String> telephone = text.getTelephone(); // not defined
Option<String> title = text.getTitle(); 
// #1 Shadow weave thick and thin based on honeysuckle long runsl

// WEAVING
Option<WIFWeaving> weavingOption = r.getWeaving();
WIFWeaving weaving = weavingOption.get();
Option<Boolean> risingShed = weaving.getRisingShed(); // true
int shafts = weaving.getShafts(); // 8
int treadles = weaving.getTreadles(); // 8

// WARP
Option<WIFWarp> warpOption = r.getWarp();
WIFWarp warp = warpOption.get();
int threads = warp.getThreads(); // 459
Option<Integer> colorPaletteIndexOption = warp.getColorPaletteIndex(); // 1
Option<Color> rgbTwoColor = warp.getRgbTwoColor(); // not defined
Option<String> symbol = warp.getSymbol(); // not defined
Option<Integer> symbolNumber = warp.getSymbolNumber(); // not defined
Option<WIFUnit> units = warp.getUnits(); // CENTIMETERS
Option<Double> spacing = warp.getSpacing(); //0.212
Option<Double> thickness = warp.getThickness(); //0.212
Option<Integer> spacingZoom = warp.getSpacingZoom(); // not defined
Option<Integer> thicknessZoom = warp.getThicknessZoom(); // not defined

 // WEFT
Option<WIFWeft> weftOption = r.getWeft();
WIFWeft weft = weftOption.get();
int picks = weft.getThreads(); // 552
Option<Integer> colorPaletteIndex = weft.getColorPaletteIndex(); // 6
Option<Color> rgbTwoColor = weft.getRgbTwoColor(); // not defined
Option<String> symbol = weft.getSymbol(); // not defined
Option<Integer> symbolNumber = weft.getSymbolNumber(); // not defined
Option<WIFUnit> units = weft.getUnits(); // CENTIMETERS
Option<Double> spacing = weft.getSpacing(); // 0.212
Option<Double> thickness = weft.getThickness(); // 0.212
Option<Integer> spacingZoom = weft.getSpacingZoom(); // not defined
Option<Integer> thicknessZoom = weft.getThicknessZoom(); // not defined

// TIEUP
Option<WIFTieup> tieupOption = r.getTieup();
WIFTieup tieup = tieupOption.get();
tieup.getTreadles().size(); // 8
for (Integer treadle : tieup.getTreadles()) {
    List<Integer> shaftsForTreadle = tieup.getShaftsForTreadle(treadle);
}

// COLOR TABLE
Option<WIFColorTable> colorTableOption = r.getColorTable();
WIFColorTable colorTable = colorTableOption.get();
for (Integer lookupIndex : colorTable.getLookupIndices()) {
    Option<Color> colorOption = colorTable.lookupColor(lookupIndex);
}

// THREADING
Option<WIFThreading> threadingOption = r.getThreading();
WIFThreading threading = threadingOption.get();
Map<Integer, List<Integer>> m = threading.getThreading();
for (int i = 1; i <= threads; i++) { // WIFs are 1-indexed
    List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
}

// WARP SPACING
Option<WIFWarpSpacing> warpSpacingOption = r.getWarpSpacing();
WIFWarpSpacing warpSpacing = warpSpacingOption.get();
for (int i = 1; i <= threads; i++) {
    Option<Pair<Double, WIFUnit>> spacing = warpSpacing.getSpacing(i);
}

// WARP COLORS
Option<WIFWarpColors> warpColorsOption = r.getWarpColors();
WIFWarpColors warpColors = warpColorsOption.get();
for (int i = 1; i <= threads; i++) {
    Option<Color> color = warpColors.getWarpColor(i);
}

// TREADLING
Option<WIFTreadling> treadlingOption = r.getTreadling();
WIFTreadling treadling = treadlingOption.get();
for (int i = 1; i <= picks; i++) {
    List<Integer> treadling = treadling.getTreadling(i).isEmpty());
}

// WEFT SPACING
Option<WIFWeftSpacing> weftSpacingOption = r.getWeftSpacing();
WIFWeftSpacing weftSpacing = weftSpacingOption.get();
for (int i = 1; i <= picks; i += 1) {
    Option<Pair<Double, WIFUnit>>  spacing = warpSpacing.getSpacing(i);
}

// WEFT COLORS
Option<WIFWeftColors> weftColorsOption = r.getWeftColors();
WIFWeftColors weftColors = weftColorsOption.get();
for (int i = 1; i <= picks; i += 2) {
    Option<Color> color = weftColors.getWeftColor(i);
}

r.close();
inputStream.close();
```


### WIF Optimization

Use the WIFReader to optimize the WIF file (e.g removing unused colors, reducing patterns, removing unused symbols etc)

* Example: [test1.wif](https://gitlab.com/ccook/jweave/-/blob/primary/src/test/resources/test1.wif)
* Comprehensive Test: [WIFReaderTest.java](https://gitlab.com/ccook/jweave/-/blob/primary/src/test/java/com/gitlab/ccook/jweave/WIFReaderTest.java)

### Optimize Everything

```java
WIFReader r = new WIFReader(inputStream);
r.optimize();
r.close();
inputStream.close();
```

### Optimize Particular Sections

| WIF Section | Access from `WIFReader` | Optimization Description |
| ------ | ------ |------ |
| [CONTENTS](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/WIFContents.java#L213) | `.getContents() `| Fills out the WIF Contents with every possible section |
| [COLOR PALETTE](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/WIFColorPalette.java#L128) |`.getColorPalette()` |  Removes any unused Colors |
| [WEFT SYMBOL PALETTE](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFWeftSymbolPalette.java#L36) | `.getWeftSymbolPalette()` | Reduces the number of max entries in the weft symbol table to just those threads that aren't repeats | 
| [WARP SYMBOL PALETTE](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/warp/WIFWarpSymbolPalette.java#L36) | `.getWarpSymbolPalette()` | Reduces the number of max entries in the weft symbol table to just those threads that aren't repeats | 
| [TEXT](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/WIFText.java#L163) | `.getText()` | NOOP; Nothing to optimize |
| [WEAVING](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/WIFWeaving.java#L121) | `.getWeaving()` | Reduces the number of shafts and treadles to only those that are used |
| [WARP](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/warp/WIFWarp.java#L63) | `.getWarp()` | Reduces warp threads by removing tail-end repeating patterns |
| [WEFT](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFWeft.java#L49) | `.getWeft()` |  Reduces weft threads by removing tail-end repeating patterns |
| [NOTES](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/WIFNotes.java#L94) | `.getNotes()` | Removes extraneous UNIX new line characters |
| [TIEUP](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/WIFTieup.java#L119) | `.getTieup()` | Removes unused treadles |
| [COLOR TABLE](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/WIFColorTable.java#L152) | `.getColorTable()` | Removes any unused colors |
| [WARP SYMBOL TABLE](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/warp/WIFWarpSymbolTable.java#L38) | `.getWarpSymbolTable()` | Removes unused symbols |
| [WEFT SYMBOL TABLE](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFWeftSymbolTable.java#L37) | `.getWeftSymbolTable()` | Removes unused symbols |
| [THREADING](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/warp/WIFThreading.java#L115) | `.getThreading()` | Removes unused threads |
| [WARP THICKNESS](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/warp/WIFWarpThickness.java#L77) | `.getWarpThickness()` | Removes unused threads |
| [WARP THICKNESS ZOOM](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/warp/WIFWarpThickness.java#L77) | `.getWarpThicknessZoom()`| Removes unused threads |
| [WARP SPACING](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/warp/WIFWarpSpacing.java#L69) | `.getWarpSpacing()` | Removes unused threads |
| [WARP SPACING ZOOM](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/warp/WIFWarpSpacingZoom.java#L57) | `.getWarpSpacingZoom()` | Removes unused threads |
| [WARP COLORS](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/warp/WIFWarpColors.java#L74) | `.getWarpColors()` | Removes unused threads/colors |
| [WARP SYMBOLS](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/warp/WIFWarpSymbols.java#L95) | `.getWarpSymbols()` | Removes unused threads |
| [TREADLING](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFTreadling.java#L79) | `.getTreadling()` | Removes unused threads |
| [LIFTPLAN](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFLiftPlan.java#L82) | `.getLiftPlan()` | Removes unused threads |
| [WEFT THICKNESS](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFWeftThickness.java#L77) | `.getWeftThickness()` | Removes unused threads |
| [WEFT THICKNESS ZOOM](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFWeftThicknessZoom.java#L58) | `.getWeftThicknessZoom()` | Removes unused threads |
| [WEFT SPACING](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFWeftSpacing.java#L72) |  `.getWeftSpacing()` | Removes unused threads |
| [WEFT SPACING ZOOM](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFWeftSpacingZoom.java#L57) | `.getWeftSpacingZoom()` |  Removes unused threads |
| [WEFT COLORS](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFWeftColors.java#L74) | `.getWeftColors()` | Removes unused threads/colors |
| [WEFT SYMBOLS](https://gitlab.com/ccook/jweave/-/blob/primary/src/main/java/com/gitlab/ccook/jweave/wif/model/weft/WIFWeftSymbols.java#L93) | `.getWeftSymbols()` | Removes unused symbols |


