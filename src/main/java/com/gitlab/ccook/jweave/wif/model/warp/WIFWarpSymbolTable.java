/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.warp;

import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.WIFWarpOrWeftSymbolTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Mapping between an index and a symbol for the warp
 */
public class WIFWarpSymbolTable extends WIFWarpOrWeftSymbolTable {
    public WIFWarpSymbolTable(List<String> lines) {
        super(lines);
    }

    public WIFWarpSymbolTable(Map<Integer, String> table) {
        super(table);
    }


    /**
     * Removes unused symbols
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWarp().isDefined()) {
            setWarp(r.getWarp());
            int threads = r.getWarp().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, String> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
        if (r.getWarpSymbolPalette().isDefined()) {
            warpSymbolPalette = r.getWarpSymbolPalette();
        }
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WARP_SYMBOL_TABLE;
    }

    public class Builder {

        Map<Integer, String> table = new TreeMap<>();

        public Builder symbol(Integer index, String symbol) {
            if (symbol != null) {
                this.table.put(index, symbol);
            }
            return this;
        }


        public WIFWarpSymbolTable build() {
            return new WIFWarpSymbolTable(table);
        }
    }
}
