/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFObject;

import java.util.List;
import java.util.Objects;

/**
 * A WIF section that provides information about the weaving and artist
 * <p>
 * e.g
 * <p>
 * [TEXT]
 * Title=example
 * Author=Ralph E. Griswold
 * Address=5302 E. 4th St., Tucson, AZ 85711
 * EMail=ralph@cs.arizona.edu
 * Telephone=520-881-1470
 * FAX=520-325-3948
 */
public class WIFText extends AbstractWIFObject {

    private Option<String> title = Option.NONE;
    private Option<String> author = Option.NONE;
    private Option<String> address = Option.NONE;
    private Option<String> email = Option.NONE;
    private Option<String> telephone = Option.NONE;
    private Option<String> fax = Option.NONE;

    public WIFText(List<String> lines) {
        Builder builder = new Builder();
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim();
                    switch (key.toLowerCase().trim()) {
                        case "title":
                            builder.title(value);
                            break;
                        case "author":
                            builder.author(value);
                            break;
                        case "address":
                            builder.address(value);
                            break;
                        case "email":
                            builder.email(value);
                            break;
                        case "telephone":
                            builder.telephone(value);
                            break;
                        case "fax":
                            builder.fax(value);
                            break;
                    }
                }
            }
        }
        setText(builder);
    }


    public void setText(Builder builder) {
        this.title = builder.title;
        this.author = builder.author;
        this.address = builder.address;
        this.email = builder.email;
        this.telephone = builder.telephone;
        this.fax = builder.fax;
    }

    public WIFText(Option<String> title,
                   Option<String> author,
                   Option<String> address,
                   Option<String> email,
                   Option<String> telephone,
                   Option<String> fax) {
        this.title = title;
        this.author = author;
        this.address = address;
        this.email = email;
        this.telephone = telephone;
        this.fax = fax;
    }

    public Option<String> getTitle() {
        return title;
    }

    public Option<String> getAuthor() {
        return author;
    }

    public Option<String> getAddress() {
        return address;
    }

    public Option<String> getEmail() {
        return email;
    }

    public Option<String> getTelephone() {
        return telephone;
    }

    public Option<String> getFax() {
        return fax;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFText wifText = (WIFText) o;
        return Objects.equals(title, wifText.title) &&
                Objects.equals(author, wifText.author) &&
                Objects.equals(address, wifText.address) &&
                Objects.equals(email, wifText.email) &&
                Objects.equals(telephone, wifText.telephone) &&
                Objects.equals(fax, wifText.fax);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, address, email, telephone, fax);
    }

    /**
     * NOOP; Nothing to optimize
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        //noop
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        if (title.isDefined()) addKeyValue(sb, "Title", title);
        if (author.isDefined()) addKeyValue(sb, "Author", author);
        if (address.isDefined()) addKeyValue(sb, "Address", address);
        if (email.isDefined()) addKeyValue(sb, "EMail", email);
        if (telephone.isDefined()) addKeyValue(sb, "Telephone", email);
        if (fax.isDefined()) addKeyValue(sb, "Fax", email);
        return sb.toString().trim();
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.TEXT;
    }


    public static class Builder {
        private Option<String> title = Option.NONE;
        private Option<String> author = Option.NONE;
        private Option<String> address = Option.NONE;
        private Option<String> email = Option.NONE;
        private Option<String> telephone = Option.NONE;
        private Option<String> fax = Option.NONE;

        public Builder() {

        }

        public Builder title(String title) {
            this.title = new Option<>(title);
            return this;
        }

        public Builder author(String author) {
            this.author = new Option<>(author);
            return this;
        }

        public Builder address(String address) {
            this.address = new Option<>(address);
            return this;
        }

        public Builder email(String email) {
            this.email = new Option<>(email);
            return this;
        }

        public Builder telephone(String telephone) {
            this.telephone = new Option<>(telephone);
            return this;
        }

        public Builder fax(String fax) {
            this.fax = new Option<>(fax);
            return this;
        }

        public WIFText build() {
            return new WIFText(title, author, address, email, telephone, fax);
        }

    }
}
