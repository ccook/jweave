/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.weft;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFWarpOrWeft;
import com.gitlab.ccook.jweave.wif.model.helper.WIFUnit;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Optional.
 * <p>
 * Information about the weft
 *
 * @see AbstractWIFWarpOrWeft
 */
public class WIFWeft extends AbstractWIFWarpOrWeft {
    public WIFWeft(List<String> lines) {
        super(lines);
    }

    public WIFWeft(int threads, Option<Integer> colorPaletteIndex, Option<Color> rgbTwoColor, Option<String> symbol, Option<Integer> symbolNumber,
                   Option<WIFUnit> units, Option<Double> spacing, Option<Double> thickness, Option<Integer> spacingZoom,
                   Option<Integer> thicknessZoom) {
        super(threads, colorPaletteIndex, rgbTwoColor, symbol, symbolNumber, units, spacing, thickness, spacingZoom, thicknessZoom);
    }

    /**
     * Reduces weft threads by removing tail-end repeating patterns
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        optimize(r, false);
    }

    public void optimize(WIFReader r, boolean ignoreColors) {
        if (r.getTreadling().isDefined()) {
            WIFTreadling treadling = r.getTreadling().get();
            int picks = getThreads();
            List<Pair<List<Integer>, Option<Color>>> reducedPattern = new ArrayList<>();
            for (int p = picks; p >= 1; p--) {
                List<Integer> t = treadling.getTreadling(p);
                Option<Color> color = Option.NONE;
                if (!ignoreColors) {
                    color = getColor(r, p);
                }
                reducedPattern.add(new Pair<>(t, color));
                if (reducedPattern.size() % 2 == 0) {
                    reducedPattern = reduce(reducedPattern);
                }
            }
            Collections.reverse(reducedPattern);
            this.threads = reducedPattern.size();
        }
    }


    private Option<Color> getColor(WIFReader r, int t) {
        if (r.getWeftColors().isDefined()) {
            return r.getWeftColors().get().getWeftColor(t);
        }
        if (r.getColorTable().isDefined()) {
            return r.getColorTable().get().lookupColor(getColorPaletteIndex().get());
        }
        return Option.NONE;
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WEFT;
    }


    public static class Builder extends AbstractWIFWarpOrWeft.Builder<Builder> {

        public Builder(int threads) {
            super(threads);
        }

        @Override
        public WIFWeft build() {
            return new WIFWeft(threads, colorPaletteIndex, rgbTwoColor, symbol, symbolNumber, units, spacing, thickness, spacingZoom, thicknessZoom);
        }
    }
}
