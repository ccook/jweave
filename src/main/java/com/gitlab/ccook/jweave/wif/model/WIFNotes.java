/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model;

import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFObject;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Notes associated with a WIF pattern
 * <p>
 * e.g
 * <p>
 * [NOTES]
 * 1=oelsner833_4blattd.wif
 * 2=WIF Version 1.1
 * 3=Source Program: Fiberworks PCW 4.0
 * 4=
 */
public class WIFNotes extends AbstractWIFObject {
    private String notes;

    public WIFNotes(List<String> lines) {
        Map<Integer, String> lineNumToString = new TreeMap<>();
        int largest = 0;
        int smallest = Integer.MAX_VALUE;
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1];
                    int lineNumber = parseIntOrDefault(key, -1);
                    if (lineNumber >= 0) {
                        lineNumToString.put(lineNumber, value);
                        if (lineNumber > largest) {
                            largest = lineNumber;
                        }
                        if (lineNumber < smallest) {
                            smallest = lineNumber;
                        }

                    }
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = smallest; i <= largest; i++) {
            if (lineNumToString.containsKey(i)) {
                sb.append(lineNumToString.get(i)).append("\n");
            }
        }
        notes = sb.toString().trim();
    }

    public WIFNotes(String s) {
        this.notes = s;
    }

    /**
     * @return notes associated with the pattern
     */
    public String getNotes() {
        return notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFNotes wifNotes = (WIFNotes) o;
        return Objects.equals(notes, wifNotes.notes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(notes);
    }

    /**
     * Removes extraneous UNIX new line characters
     *
     * @param r reader to optimixe from
     */
    @Override
    public void optimize(WIFReader r) {
        StringBuilder sb = new StringBuilder();
        for (String line : notes.split("\n")) {
            if (!line.trim().isEmpty()) {
                sb.append(line);
            }
        }
        notes = sb.toString();
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        String[] split = notes.split("\n");
        for (int i = 0; i < split.length; i++) {
            addKeyValue(sb, i + 1, split[i]);
        }
        return sb.toString().trim();
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.NOTES;
    }

    public static class Builder {
        StringBuilder sb = new StringBuilder();

        public Builder notes(String s) {
            if (s != null) {
                sb.append(s).append("\n");
            }
            return this;
        }

        public WIFNotes build() {
            return new WIFNotes(sb.toString().trim());
        }
    }

}
