/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFEntriesObject;
import com.gitlab.ccook.jweave.wif.model.helper.WIFColorTripplet;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarpColors;
import com.gitlab.ccook.jweave.wif.model.weft.WIFWeftColors;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Optional
 * <p>&nbsp;</p>
 * Contains: entries (0..n) AND range ([0..n], [first...m])
 * <p>&nbsp;</p>
 * e.g
 * [COLOR PALETTE]
 * Range=0,999
 * Entries=82
 * <p>&nbsp;</p>
 * <p>&nbsp;</p>
 * The example above says 'There are 82 entries in the COLOR TABLE, Their RGB values range from 0 to 999.
 *
 * @see WIFColorTable
 * @see WIFColorTripplet
 * @see WIFWarpColors
 * @see WIFWeftColors
 */
public class WIFColorPalette extends AbstractWIFEntriesObject {
    private Pair<Integer, Integer> range = new Pair<>(0, 0);

    public WIFColorPalette(List<String> lines) {
        super(lines);
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim();
                    if (key.trim().toLowerCase().equalsIgnoreCase("range")) {
                        String[] rangeParts = value.split(",");
                        int first = 0;
                        int second = 0;
                        if (rangeParts.length >= 1) {
                            first = parseIntOrDefault(rangeParts[0].trim(), 0);
                            if (first < 0) {
                                first = 0;
                            }
                            second = first;

                        }
                        if (rangeParts.length >= 2) {
                            second = parseIntOrDefault(rangeParts[1].trim(), 0);
                            if (second < 0) {
                                second = 0;
                            }
                            if (second < first) {
                                second = first;
                            }
                        }
                        range = new Pair<>(first, second);
                    }
                }
            }
        }
    }


    public WIFColorPalette(int entries, Pair<Integer, Integer> range) {
        super(entries);
        this.range = range;

    }

    /**
     * The range of RGB values in the RGBTripplet
     *
     * @return range, first = range start, second = range end
     * @see WIFColorTripplet
     */
    public Pair<Integer, Integer> getRange() {
        return range;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFColorPalette that = (WIFColorPalette) o;
        return entries == that.entries &&
                Objects.equals(range, that.range);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entries, range);
    }

    /**
     * Removes any unused Colors
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        Set<Integer> colorsUsed = new HashSet<>();
        if (r.getWeft().isDefined()) {
            if (r.getWeft().get().getColorPaletteIndex().isDefined()) {
                colorsUsed.add(r.getWeft().get().getColorPaletteIndex().get());
            }
            if (r.getWeftColors().isDefined()) {
                int threads = r.getWeft().get().getThreads();
                for (int i = 0; i < threads; i++) {
                    Option<Integer> weftColorIndex = r.getWeftColors().get().getWeftColorIndex(i);
                    if (weftColorIndex.isDefined()) {
                        colorsUsed.add(weftColorIndex.get());
                    }
                }
            }
        }
        if (r.getWarp().isDefined()) {
            if (r.getWarp().get().getColorPaletteIndex().isDefined()) {
                colorsUsed.add(r.getWarp().get().getColorPaletteIndex().get());
            }
            if (r.getWarpColors().isDefined()) {
                int threads = r.getWarp().get().getThreads();
                for (int i = 0; i < threads; i++) {
                    Option<Integer> warpColorIndex = r.getWarpColors().get().getWarpColorIndex(i);
                    if (warpColorIndex.isDefined()) {
                        colorsUsed.add(warpColorIndex.get());
                    }
                }
            }

        }
        this.entries = colorsUsed.size();
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.COLOR_PALETTE;
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        addKeyValue(sb, "Entries", entries);
        addKeyValue(sb, "Range", range.getFirst() + "," + range.getSecond());

        return sb.toString().trim();
    }

    public static class Builder {
        private final int entries;
        private final Pair<Integer, Integer> range;

        public Builder(int entries, Pair<Integer, Integer> range) {
            this.entries = entries;
            this.range = range;
        }


        public WIFColorPalette build() {
            return new WIFColorPalette(entries, range);
        }
    }
}
