/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.warp;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Optional.
 * <p>
 * The zoom of the spacing of the warp, for a given warp
 */
public class WIFWarpSpacingZoom extends AbstractWIFTable<Integer> {

    public WIFWarpSpacingZoom(List<String> lines) {
        super(lines, 0);
    }

    public WIFWarpSpacingZoom(Map<Integer, Integer> table) {
        super(table, 0);
    }

    @Override
    public Integer parseOrDefault(String value) {
        return parseIntOrDefault(value, getDefaultValue());
    }

    public Option<Integer> getZoom(int warp) {
        Option<Integer> spacingZoom = new Option<>(table.get(warp));
        if (spacingZoom.isDefined()) {
            return spacingZoom;
        }
        if (getWarp().isDefined()) {
            return getWarp().get().getSpacingZoom();
        }
        return Option.NONE;
    }

    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWarp().isDefined()) {
            setWarp(r.getWarp());
            int threads = r.getWarp().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, Integer> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Integer> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WARP_SPACING_ZOOM;
    }

    public class Builder {
        private Map<Integer, Integer> table = new TreeMap<>();

        public Builder warpSpacingZoom(int warp, int zoom) {
            table.put(warp, zoom);
            return this;
        }

        public WIFWarpSpacingZoom build() {
            return new WIFWarpSpacingZoom(table);
        }
    }
}
