/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.weft;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;
import com.gitlab.ccook.jweave.wif.model.helper.WIFUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * A mapping between the weft, and its spacing
 */
public class WIFWeftSpacing extends AbstractWIFTable<Double> {
    private WIFUnit units = WIFUnit.UNKNOWN;

    public WIFWeftSpacing(List<String> lines) {
        super(lines, 0.0);
    }

    public WIFWeftSpacing(Map<Integer, Double> table) {
        super(table, 0.0);
    }

    @Override
    public Double parseOrDefault(String value) {
        return parseDoubleOrDefault(value, getDefaultValue());
    }


    public void setUnits(WIFUnit units) {
        this.units = units;
    }


    public Option<Pair<Double, WIFUnit>> getSpacing(int weft) {
        if (getWeft().isDefined()) {
            Option<WIFUnit> units = getWeft().get().getUnits();
            if (units.isDefined()) {
                setUnits(units.get());
            }
        }
        if (table.containsKey(weft)) {
            return new Option<>(new Pair<>(table.get(weft), units));
        }
        if (getWeft().isDefined()) {
            Option<Double> spacing = getWeft().get().getSpacing();
            if (spacing.isDefined()) {
                return new Option<>(new Pair<>(spacing.get(), units));
            }
        }
        return Option.NONE;

    }

    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWeft().isDefined()) {
            setWeft(r.getWeft());
            int threads = r.getWeft().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, Double> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Double> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WEFT_SPACING;
    }

    public class Builder {

        private Map<Integer, Double> table = new TreeMap<>();
        private WIFUnit units = WIFUnit.UNKNOWN;

        public Builder() {
        }

        public Builder(WIFUnit units) {
            if (units != null) {
                this.units = units;
            }
        }

        public Builder units(WIFUnit units) {
            if (units != null) {
                this.units = units;
            }
            return this;
        }

        public Builder weftSpacing(int weft, double spacing) {
            table.put(weft, spacing);
            return this;
        }

        public WIFWeftSpacing build() {
            WIFWeftSpacing wifWeftSpacing = new WIFWeftSpacing(table);
            wifWeftSpacing.setUnits(units);
            return wifWeftSpacing;
        }
    }

}