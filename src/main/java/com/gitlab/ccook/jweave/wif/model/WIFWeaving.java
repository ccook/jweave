/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFObject;

import java.util.*;

/**
 * information on the weaving
 */
public class WIFWeaving extends AbstractWIFObject {

    private int shafts = 1;
    private int treadles = 1;
    private Option<Boolean> risingShed = Option.NONE;

    public WIFWeaving(List<String> lines) {
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim();
                    switch (key.toLowerCase().replace(" ", "")) {
                        case "shafts":
                            shafts = parseIntOrDefault(value, 1);
                            break;
                        case "treadles":
                            treadles = parseIntOrDefault(value, 1);
                            break;
                        case "risingshed":
                            risingShed = parseBoolean(value);
                            break;
                    }
                }
            }
        }
    }

    public WIFWeaving(Builder builder) {
        setWeaving(builder.shafts, builder.treadles, builder.risingShed);
    }

    private void setWeaving(int shafts, int treadles, Option<Boolean> risingShed) {
        this.shafts = shafts;
        this.treadles = treadles;
        this.risingShed = risingShed;
    }

    /**
     * Number of shafts
     *
     * @return Number of shafts
     */
    public int getShafts() {
        return shafts;
    }

    /**
     * Number of treadles
     *
     * @return Number of treadles
     */
    public int getTreadles() {
        return treadles;
    }

    /**
     * If defined, if the weaving uses a rising shed
     *
     * @return  if the weaving uses a rising shed
     */
    public Option<Boolean> getRisingShed() {
        return risingShed;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFWeaving that = (WIFWeaving) o;
        return shafts == that.shafts &&
                treadles == that.treadles &&
                Objects.equals(risingShed, that.risingShed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shafts, treadles, risingShed);
    }

    /**
     * Reduces the number of shafts and treadles to only those that are used
     *
     * @param r the reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        Set<Integer> treadlesUsed = new LinkedHashSet<>();
        Set<Integer> shaftsUsed = new LinkedHashSet<>();
        if (!r.getTreadling().isDefined() && !r.getTieup().isDefined() && !r.getLiftPlan().isDefined()) {
            return;
        }
        if (r.getTreadling().isDefined()) {
            for (Map.Entry<Integer, List<Integer>> treadling : r.getTreadling().get().getTable().entrySet()) {
                treadlesUsed.addAll(treadling.getValue());
            }
        }
        if (r.getTieup().isDefined()) {
            for (Map.Entry<Integer, List<Integer>> integerListEntry : r.getTieup().get().getTieUp().entrySet()) {
                treadlesUsed.add(integerListEntry.getKey());
                shaftsUsed.addAll(integerListEntry.getValue());
            }
        }
        if (r.getLiftPlan().isDefined()) {
            Map<Integer, List<Integer>> table = r.getLiftPlan().get().getTable();
            for (Map.Entry<Integer, List<Integer>> ent : table.entrySet()) {
                shaftsUsed.addAll(ent.getValue());
            }
        }
        this.shafts = shaftsUsed.size();
        this.treadles = treadlesUsed.size();
        // for the case where just a lift plan is defined (in those cases every shaft is also a treadle)
        if (treadlesUsed.size() == 0) {
            this.treadles = shaftsUsed.size();
        }
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WEAVING;
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        addKeyValue(sb, "Shafts", shafts);
        addKeyValue(sb, "Treadles", treadles);
        if (risingShed.isDefined()) addKeyValue(sb, "Rising Shed", risingShed.get().toString().toLowerCase());
        return sb.toString().trim();
    }

    public static class Builder {
        private final int shafts;
        private final int treadles;
        private Option<Boolean> risingShed = Option.NONE;

        public Builder(int shafts, int treadles) {
            this.shafts = shafts;
            this.treadles = treadles;
        }

        public Builder risingShed(boolean isRisingShed) {
            this.risingShed = new Option<>(isRisingShed);
            return this;
        }

        public WIFWeaving build() {
            return new WIFWeaving(this);
        }
    }
}
