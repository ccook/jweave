/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.helper;

/**
 * A helper object for the valid units for spacing/thickness
 */
public enum WIFUnit {
    DECIPOINTS,
    INCHES,
    CENTIMETERS,
    UNKNOWN;

    @Override
    public String toString() {
        return name().charAt(0) + name().replaceAll("^" + name().charAt(0), "").toLowerCase();
    }

    public static WIFUnit fromString(String parse) {
        if (parse != null) {
            for (WIFUnit unit : WIFUnit.values()) {
                if (unit.name().toLowerCase().equalsIgnoreCase(parse.toLowerCase().trim())) {
                    return unit;
                }
            }
        }
        return UNKNOWN;
    }
}
