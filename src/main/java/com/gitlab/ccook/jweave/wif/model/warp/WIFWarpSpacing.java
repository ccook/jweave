/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.warp;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;
import com.gitlab.ccook.jweave.wif.model.helper.WIFUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * A mapping between the warp, and its spacing
 */
public class WIFWarpSpacing extends AbstractWIFTable<Double> {
    private WIFUnit units = WIFUnit.UNKNOWN;

    public WIFWarpSpacing(List<String> lines) {
        super(lines, 0.0);
    }

    public WIFWarpSpacing(Map<Integer, Double> spacing) {
        super(spacing, 0.0);
    }

    public void setUnits(WIFUnit units) {
        this.units = units;
    }

    @Override
    public Double parseOrDefault(String value) {
        return parseDoubleOrDefault(value, getDefaultValue());
    }

    public Option<Pair<Double, WIFUnit>> getSpacing(int warp) {
        if (getWarp().isDefined()) {
            Option<WIFUnit> units = getWarp().get().getUnits();
            if (units.isDefined()) {
                setUnits(units.get());
            }
        }
        if (table.containsKey(warp)) {
            return new Option<>(new Pair<>(table.get(warp), units));
        }
        if (getWarp().isDefined()) {
            Option<Double> spacing = getWarp().get().getSpacing();
            if (spacing.isDefined()) {
                return new Option<>(new Pair<>(spacing.get(), units));
            }
        }
        return Option.NONE;
    }

    public WIFUnit getUnits() {
        return units;
    }

    /**
     * Removes unused threads
     *
     * @param r the WIFReader to optimize
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWarp().isDefined()) {
            setWarp(r.getWarp());
            int threads = r.getWarp().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, Double> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
    }


    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Double> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WARP_SPACING;
    }

    public static class Builder {
        private WIFUnit units = WIFUnit.UNKNOWN;
        private Map<Integer, Double> spacing = new TreeMap<>();

        public Builder(WIFUnit units) {
            units(units);
        }

        public Builder() {
        }

        public WIFWarpSpacing.Builder units(WIFUnit units) {
            if (units != null) {
                this.units = units;
            }
            return this;
        }

        public WIFWarpSpacing.Builder spacing(int warpThread, double spacing) {
            this.spacing.put(warpThread, spacing);
            return this;
        }

        public WIFWarpSpacing build() {
            WIFWarpSpacing s = new WIFWarpSpacing(spacing);
            s.setUnits(units);
            return s;
        }
    }

}
