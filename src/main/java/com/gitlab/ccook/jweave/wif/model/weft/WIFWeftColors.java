/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.weft;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFColorTable;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;

import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * Mapping between the weft thread and its color in the color table
 */
public class WIFWeftColors extends AbstractWIFTable<Integer> {
    private Option<WIFColorTable> colorTable = Option.NONE;

    public WIFWeftColors(List<String> lines) {
        super(lines, 0);
    }

    public WIFWeftColors(Map<Integer, Integer> table) {
        super(table, 0);
    }

    public Option<Integer> getWeftColorIndex(int weft) {
        if (table.containsKey(weft)) {
            return new Option<>(table.get(weft));
        }
        if (getWeft().isDefined()) {
            return getWeft().get().getColorPaletteIndex();
        }
        return Option.NONE;
    }

    public Option<Color> getWeftColor(int weft) {
        if (colorTable.isDefined()) {
            if (getWeftColorIndex(weft).isDefined()) {
                return colorTable.get().lookupColor(getWeftColorIndex(weft).get());
            }
        }
        return Option.NONE;
    }


    @Override
    public Integer parseOrDefault(String value) {
        return parseIntOrDefault(value, getDefaultValue());
    }

    public void setColorTable(WIFColorTable wifColorTable) {
        this.colorTable = new Option<>(wifColorTable);
    }

    public Option<WIFColorTable> getColorTable() {
        return colorTable;
    }

    /**
     * Removes unused threads/colors
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWeft().isDefined()) {
            Set<Map.Entry<Integer, Integer>> entries = table.entrySet();
            Set<Integer> toRemove = new HashSet<>();
            for (Map.Entry<Integer, Integer> ent : entries) {
                if (ent.getKey() > r.getWeft().get().getThreads()) {
                    toRemove.add(ent.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WIFWeftColors that = (WIFWeftColors) o;
        return Objects.equals(colorTable, that.colorTable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), colorTable);
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WEFT_COLORS;
    }


    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Integer> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }

    public class Builder {
        private Map<Integer, Integer> table = new TreeMap<>();

        public Builder weftColor(int weft, int colorIndex) {
            table.put(weft, colorIndex);
            return this;
        }

        public WIFWeftColors build() {
            return new WIFWeftColors(table);
        }
    }
}
