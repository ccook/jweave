/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.weft;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * A magnification factor to be applied to the base thickness
 */
public class WIFWeftThicknessZoom extends AbstractWIFTable<Integer> {
    public WIFWeftThicknessZoom(List<String> lines) {
        super(lines, 1);
    }

    public WIFWeftThicknessZoom(Map<Integer, Integer> table) {
        super(table, 1);
    }

    @Override
    public Integer parseOrDefault(String value) {
        return parseIntOrDefault(value, getDefaultValue());
    }

    /**
     * magnification factor to be applied to the base thickness
     *
     * @param weft weft number to get zoom for
     * @return magnification factor to be applied to the base thickness
     */
    public Option<Integer> getZoom(int weft) {
        if (table.containsKey(weft)) {
            return new Option<>(table.get(weft));
        } else if (getWeft().isDefined()) {
            return getWeft().get().getThicknessZoom();
        }
        return Option.NONE;
    }

    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWeft().isDefined()) {
            setWeft(r.getWeft());
            int threads = r.getWeft().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, Integer> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Integer> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WEFT_THICKNESS_ZOOM;
    }


    public class Builder {

        private Map<Integer, Integer> table = new TreeMap<>();

        public Builder weftThicknessZoom(int weft, int zoom) {
            table.put(weft, zoom);
            return this;
        }

        public WIFWeftThicknessZoom build() {
            return new WIFWeftThicknessZoom(table);
        }
    }

}
