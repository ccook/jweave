/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.weft;

import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.WIFWarpOrWeftSymbolTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Mapping between an index and a symbol for the weft
 */
public class WIFWeftSymbolTable extends WIFWarpOrWeftSymbolTable {
    public WIFWeftSymbolTable(List<String> lines) {
        super(lines);
    }

    /**
     * Removes unused symbols
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWeft().isDefined()) {
            setWeft(r.getWeft());
            int threads = r.getWeft().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, String> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
        if (r.getWeftSymbolPalette().isDefined()) {
            weftSymbolPalette = r.getWeftSymbolPalette();
        }
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WEFT_SYMBOL_TABLE;
    }

}
