/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.weft;

import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * A mapping between the pick/weft and the treadles engaged for that pick.
 * <p>
 * e.g
 * <p>
 * [TREADLING]
 * 1=1
 * 2=2
 * 3=3
 * 4=4
 * 5=5
 * 6=6
 * 7=7
 * 8=8
 * 9=1
 * 10=2
 * 11=3
 * 12=4
 * ...
 */
public class WIFTreadling extends AbstractWIFTable<List<Integer>> {
    public WIFTreadling(List<String> lines) {
        super(lines, new ArrayList<>());
    }

    public WIFTreadling(Map<Integer, List<Integer>> table) {
        super(table, new ArrayList<>());
    }

    @Override
    public List<Integer> parseOrDefault(String value) {
        String[] parts = value.split(",");
        List<Integer> parsed = new ArrayList<>();
        for (String part : parts) {
            parsed.add(parseIntOrDefault(part, 0));
        }
        return parsed;
    }

    /**
     * For a given pick, return the treadles engaged
     *
     * @param pick pick for treadling
     * @return return the treadles engaged
     */
    public List<Integer> getTreadling(int pick) {
        List<Integer> tread = table.get(pick);
        if (tread != null) {
            return tread;
        }
        return new ArrayList<>();
    }

    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    public void optimize(WIFReader r) {
        if (r.getWeft().isDefined()) {
            WIFWeft weft = r.getWeft().get();
            int picks = weft.getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, List<Integer>> entry : table.entrySet()) {
                if (entry.getKey() > picks) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer re : toRemove) {
                table.remove(re);
            }
        }
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.TREADLING;
    }


    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        int size = table.keySet().size();
        for (int i = 1; i <= size; i++) {
            List<Integer> shaftsForPick = getTreadling(i);
            sb.append(i).append("=");
            for (Integer shaft : shaftsForPick) {
                sb.append(shaft).append(",");
            }
            if (!shaftsForPick.isEmpty()) {
                sb = new StringBuilder(sb.substring(0, sb.length() - 1));
            }
            sb.append("\n");
        }
        return sb.toString().trim();
    }

    public static class Builder {

        private Map<Integer, List<Integer>> table = new TreeMap<>();

        public Builder treadling(int pick, int... treadles) {
            List<Integer> treadlesList = new ArrayList<>();
            for (int t : treadles) {
                treadlesList.add(t);
            }
            return treadling(pick, treadlesList);

        }

        public Builder treadling(int pick, List<Integer> treadles) {
            table.put(pick, treadles);
            return this;
        }

        public WIFTreadling build() {
            return new WIFTreadling(table);
        }
    }
}
