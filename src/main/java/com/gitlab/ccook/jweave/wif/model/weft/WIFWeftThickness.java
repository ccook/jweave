/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.weft;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;
import com.gitlab.ccook.jweave.wif.model.helper.WIFUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The thickness of each weft thread in the specified unit.
 */
public class WIFWeftThickness extends AbstractWIFTable<Double> {
    private WIFUnit units = WIFUnit.UNKNOWN;


    public WIFWeftThickness(List<String> lines) {
        super(lines, 0.0);
    }

    public WIFWeftThickness(Map<Integer, Double> table) {
        super(table, 0.0);
    }

    public void setUnits(WIFUnit units) {
        this.units = units;
    }

    /**
     * If defined, returns the thickness for a weft thread
     *
     * @param weft weft number to get thickness for
     * @return  returns the thickness for a weft thread
     */
    public Option<Pair<Double, WIFUnit>> getThickness(int weft) {
        if (getWeft().isDefined()) {
            Option<WIFUnit> units = getWeft().get().getUnits();
            if (units.isDefined()) {
                setUnits(units.get());
            }
        }
        if (table.containsKey(weft)) {
            return new Option<>(new Pair<>(table.get(weft), this.units));
        }
        if (getWeft().isDefined()) {
            Option<Double> thickness = getWeft().get().getThickness();
            if (thickness.isDefined()) {
                return new Option<>(new Pair<>(thickness.get(), units));
            }
        }
        return Option.NONE;
    }

    @Override
    public Double parseOrDefault(String value) {
        return parseDoubleOrDefault(value, getDefaultValue());
    }

    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWeft().isDefined()) {
            setWeft(r.getWeft());
            Option<WIFUnit> units = getWeft().get().getUnits();
            if (units.isDefined()) {
                setUnits(units.get());
            }
            int threads = r.getWeft().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, Double> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Double> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WEFT_THICKNESS;
    }


    public class Builder {
        private WIFUnit units = WIFUnit.UNKNOWN;
        private Map<Integer, Double> table = new TreeMap<>();

        public Builder() {

        }

        public Builder(WIFUnit units) {
            this.units = units;
        }

        public WIFWeftThickness.Builder units(WIFUnit units) {
            this.units = units;
            return this;
        }

        public WIFWeftThickness.Builder thickness(int warp, double thickness) {
            table.put(warp, thickness);
            return this;
        }

        public WIFWeftThickness build() {
            WIFWeftThickness wifWeftThickness = new WIFWeftThickness(table);
            wifWeftThickness.setUnits(units);
            return wifWeftThickness;
        }
    }

}
