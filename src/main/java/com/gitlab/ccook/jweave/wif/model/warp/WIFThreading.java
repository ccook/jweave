/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.warp;

import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFObject;

import java.util.*;

/**
 * The threading for the warp.
 * <p>
 * A mapping between the warp # and the shaft(s) to thread through the heddles
 * e.g [1, 1] means warp # 1 goes through shaft #1
 * <p>
 * e.g
 * <p>
 * [THREADING]
 * 1=1
 * 2=2
 * 3=3
 * 4=4
 * 5=5
 * 6=6
 * 7=7
 * 8=8
 * 9=9
 * 10=10
 * 11=11
 * 12=12
 * 13=13
 * 14=14
 * 15=15
 * 16=16
 * 17=17
 * 18=18
 * 19=19
 * 20=20
 * 21=21
 * 22=22
 * 23=23
 * 24=24
 * 25=25
 * 26=26
 * 27=27
 * 28=28
 * 29=29
 * 30=30
 * 31=31
 * 32=32
 * 33=1
 * ....
 */
public class WIFThreading extends AbstractWIFObject {
    private Map<Integer, List<Integer>> threading = new TreeMap<>();

    public WIFThreading(List<String> lines) {
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1];
                    int index = parseIntOrDefault(key, -1);
                    if (index > 0) {
                        List<Integer> ints = new ArrayList<>();
                        for (String i : value.split(",")) {
                            int warp = parseIntOrDefault(i, -1);
                            if (warp >= 0) {
                                ints.add(warp);
                            }
                        }
                        threading.put(index, ints);
                    }
                }
            }
        }
    }

    public WIFThreading(Builder builder) {
        this.threading = builder.threading;
    }

    /**
     * @return mapping between warp thread and shafts to put the thread through its heddle
     */
    public Map<Integer, List<Integer>> getThreading() {
        return threading;
    }

    /**
     * @param warpNum the warp number
     * @return the shafts whose heddles the shaft goes through
     */
    public List<Integer> getShaftsForWarp(int warpNum) {
        List<Integer> t = threading.get(warpNum);
        if (t != null) {
            return t;
        }
        return new ArrayList<>();
    }

    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWarp().isDefined()) {
            int threads = r.getWarp().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, List<Integer>> entry : threading.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer remove : toRemove) {
                threading.remove(remove);
            }
        }
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.THREADING;
    }


    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, List<Integer>> ent : this.threading.entrySet()) {
            StringBuilder value = new StringBuilder();
            for (Integer i : ent.getValue()) {
                value.append(i).append(",");
            }
            if (!ent.getValue().isEmpty()) {
                addKeyValue(sb, ent.getKey(), value.substring(0, value.length() - 1));
            }
        }
        return sb.toString().trim();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFThreading that = (WIFThreading) o;
        return Objects.equals(threading, that.threading);
    }

    @Override
    public int hashCode() {
        return Objects.hash(threading);
    }


    public static class Builder {
        Map<Integer, List<Integer>> threading = new TreeMap<>();

        public Builder threading(int warpThreadNumber, int... shaftNumbers) {
            if (threading.containsKey(warpThreadNumber)) {
                List<Integer> integers = threading.get(warpThreadNumber);
                for (int s : shaftNumbers) {
                    integers.add(s);
                }
                threading.put(warpThreadNumber, integers);
            } else {
                List<Integer> integers = new ArrayList<>();
                for (int s : shaftNumbers) {
                    integers.add(s);
                }
                threading.put(warpThreadNumber, integers);
            }
            return this;
        }

        public WIFThreading build() {
            return new WIFThreading(this);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Builder builder = (Builder) o;
            return Objects.equals(threading, builder.threading);
        }

        @Override
        public int hashCode() {
            return Objects.hash(threading);
        }
    }

}
