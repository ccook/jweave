/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.weft;

import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFEntriesObject;

import java.util.List;

/**
 * Optional
 * The max entries in the weft symbol table
 */
public class WIFWeftSymbolPalette extends AbstractWIFEntriesObject {
    public WIFWeftSymbolPalette(List<String> lines) {
        super(lines);
    }

    /**
     * Reduces the number of max entries in the weft symbol table to just those threads that aren't repeats
     *
     * @param r the reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWeft().isDefined()) {
            if (this.entries > r.getWeft().get().getThreads()) {
                this.entries = r.getWeft().get().getThreads();
            }
        }
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        addKeyValue(sb, "Entries", entries);

        return sb.toString().trim();
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WEFT_SYMBOL_PALETTE;
    }

}
