/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFObject;
import com.gitlab.ccook.jweave.wif.model.helper.WIFColorTripplet;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarpColors;
import com.gitlab.ccook.jweave.wif.model.weft.WIFWeftColors;

import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * Optional
 * <p>
 * A table linking an index to an RGB color.
 * The number of entries and the range of each RGB value is specified in WIFColorPalette
 * The WIFReader inserts a WIFColorPalette to calculate the 'real' RGB (with each value between 0 and 255)
 * <p>
 * e.g
 * <p>
 * [COLOR TABLE]
 * 1=999,999,999
 * 2=0,0,999
 * 3=199,0,999
 * 4=0,266,999
 * 5=0,999,999
 * 6=0,999,0
 * 7=466,999,0
 * 8=999,999,0
 * 9=999,532,0
 * 10=999,0,0
 * 11=999,0,599
 * 12=799,0,999
 * 13=517,399,999
 * 14=399,607,999
 * 15=399,999,999
 * 16=399,999,399
 * 17=787,999,399
 * 18=999,999,399
 * 19=999,677,399
 * 20=999,399,399
 * ...
 *
 * @see WIFColorPalette
 * @see WIFColorTripplet
 * @see WIFWeftColors
 * @see WIFWarpColors
 */
@SuppressWarnings("unchecked")
public class WIFColorTable extends AbstractWIFObject {
    private Map<Integer, WIFColorTripplet> table = new TreeMap<>();
    private Option<WIFColorPalette> colorPalette = Option.NONE;

    public WIFColorTable(List<String> lines) {
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1];
                    int index = parseIntOrDefault(key, -1);
                    if (index > 0) {
                        String[] valueParts = value.split(",");
                        if (valueParts.length >= 3) {
                            int r = parseIntOrDefault(valueParts[0], 0);
                            int g = parseIntOrDefault(valueParts[1], 0);
                            int b = parseIntOrDefault(valueParts[2], 0);
                            table.put(index, new WIFColorTripplet(r, g, b));
                        }
                    }
                }
            }
        }
        setColorTable(table, Option.NONE);
    }

    public WIFColorTable(Builder b) {
        this(b.t, b.palette);
    }

    public WIFColorTable(Map<Integer, WIFColorTripplet> table, Option<WIFColorPalette> colorPalette) {
        setColorTable(table, colorPalette);
    }

    private void setColorTable(Map<Integer, WIFColorTripplet> table, Option<WIFColorPalette> colorPalette) {
        this.table = table;
        this.colorPalette = colorPalette;
    }


    /**
     * The table keys, should be equal to WIFColorPalette.entries
     *
     * @return All the indices in the table (index = RGB color)
     */
    public Set<Integer> getLookupIndices() {
        return table.keySet();
    }

    /**
     * Returns a Color for a given index (assuming its defined)
     *
     * @param index the lookup index in the Color table
     * @return a java Color RGB object
     */
    public Option<Color> lookupColor(int index) {
        if (colorPalette.isDefined()) {
            return new Option<>(table.get(index).getColor(colorPalette.get()));
        }
        return Option.NONE;
    }

    /**
     * Sets the WIFColorPalette used to decode the RGB values
     * This is used mostly by the WIFReader to update the color palette when it's eventually read
     *
     * @param colorPalette The WIFColorPalette used to decode the RGB values
     * @see com.gitlab.ccook.jweave.wif.WIFReader
     * @see WIFColorPalette
     */
    public void setColorPalette(Option<WIFColorPalette> colorPalette) {
        this.colorPalette = colorPalette;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFColorTable that = (WIFColorTable) o;
        return Objects.equals(table, that.table) &&
                Objects.equals(colorPalette, that.colorPalette);
    }

    @Override
    public int hashCode() {
        return Objects.hash(table, colorPalette);
    }

    /**
     * Removes any unused colors
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        List<Integer> colorsUsed = new ArrayList<>();
        if (r.getWeft().isDefined()) {
            if (r.getWeft().get().getColorPaletteIndex().isDefined()) {
                colorsUsed.add(r.getWeft().get().getColorPaletteIndex().get());
            }
            if (r.getWeftColors().isDefined()) {
                int threads = r.getWeft().get().getThreads();
                for (int i = 0; i < threads; i++) {
                    Option<Integer> weftColorIndex = r.getWeftColors().get().getWeftColorIndex(i);
                    if (weftColorIndex.isDefined()) {
                        colorsUsed.add(weftColorIndex.get());
                    }
                }
            }
        }
        if (r.getWarp().isDefined()) {
            if (r.getWarp().get().getColorPaletteIndex().isDefined()) {
                colorsUsed.add(r.getWarp().get().getColorPaletteIndex().get());
            }
            if (r.getWarpColors().isDefined()) {
                int threads = r.getWarp().get().getThreads();
                for (int i = 0; i < threads; i++) {
                    Option<Integer> warpColorIndex = r.getWarpColors().get().getWarpColorIndex(i);
                    if (warpColorIndex.isDefined()) {
                        colorsUsed.add(warpColorIndex.get());
                    }
                }
            }

        }
        List<Integer> unusedColors = new ArrayList<>();
        for (Map.Entry<Integer, WIFColorTripplet> ent : this.table.entrySet()) {
            if (!colorsUsed.contains(ent.getKey())) {
                unusedColors.add(ent.getKey());
            }
        }
        for (int i : unusedColors) {
            table.remove(i);
        }
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.COLOR_TABLE;
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, WIFColorTripplet> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue().getRawR() + "," + ent.getValue().getRawG() + "," + ent.getValue().getRawB());
        }

        return sb.toString().trim();
    }


    public static class Builder {

        private Map<Integer, WIFColorTripplet> t = new HashMap<>();
        private Option<WIFColorPalette> palette = Option.NONE;

        public Builder color(int index, WIFColorTripplet color) {
            t.put(index, color);
            return this;
        }

        public Builder palette(WIFColorPalette palette) {
            this.palette = new Option<>(palette);
            return this;
        }

        public Builder palette(int entries, Pair<Integer, Integer> range) {
            return palette(new WIFColorPalette.Builder(entries, range).build());
        }

        public Builder palette(WIFColorPalette.Builder palette) {
            this.palette = new Option<>(palette.build());
            return this;
        }

        public WIFColorTable build() {
            return new WIFColorTable(t, palette);
        }

        public Option<WIFColorPalette> getPalette() {
            return palette;
        }
    }
}
