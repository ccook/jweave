/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.model.*;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFObject;
import com.gitlab.ccook.jweave.wif.model.warp.*;
import com.gitlab.ccook.jweave.wif.model.weft.*;

import java.util.Map;
import java.util.TreeMap;

@SuppressWarnings("unchecked")
public class WIFBuilder {

    private WIFHeader.Builder wifHeader = new WIFHeader.Builder();
    private WIFContents.Builder wifContents = new WIFContents.Builder();
    private Option<WIFColorPalette.Builder> colorPalette = Option.NONE;
    private Option<WIFText.Builder> text = Option.NONE;
    private Option<WIFWeaving.Builder> weaving = Option.NONE;
    private Option<WIFWarp.Builder> warp = Option.NONE;
    private Option<WIFWeft.Builder> weft = Option.NONE;
    private Option<WIFTieup.Builder> tieup = Option.NONE;
    private Option<WIFColorTable.Builder> colorTable = Option.NONE;
    private Option<WIFThreading.Builder> threading = Option.NONE;
    private Option<WIFLiftPlan.Builder> liftPlan = Option.NONE;
    private Option<WIFWarpSpacing.Builder> warpSpacing = Option.NONE;
    private Option<WIFWarpColors.Builder> warpColors = Option.NONE;
    private Option<WIFWarpSymbolTable.Builder> warpSymbolTable = Option.NONE;
    private Option<WIFWarpThickness.Builder> warpThickness = Option.NONE;
    private Option<WIFWarpSpacingZoom.Builder> warpSpacingZoom = Option.NONE;
    private Option<WIFWarpSymbols.Builder> warpSymbols = Option.NONE;
    private Option<WIFTreadling.Builder> treadling = Option.NONE;
    private Option<WIFNotes.Builder> notes = Option.NONE;
    private Option<WIFWeftThickness.Builder> weftThickness = Option.NONE;
    private Option<WIFWeftThicknessZoom.Builder> weftThicknessZoom = Option.NONE;
    private Option<WIFWeftSpacing.Builder> weftSpacing = Option.NONE;
    private Option<WIFWeftColors.Builder> weftColors = Option.NONE;
    private Option<WIFWeftSymbols.Builder> weftSymbols = Option.NONE;
    private Map<Pair<String, String>, String> privateData = new TreeMap<>();

    public WIFBuilder header(WIFHeader.Builder wifHeaderBuilder) {
        if (wifHeaderBuilder != null) {
            wifHeader = wifHeaderBuilder;
        }
        return this;
    }

    public WIFBuilder colorPalette(WIFColorPalette.Builder colorPaletteBuilder) {
        this.colorPalette = new Option<>(colorPaletteBuilder);
        if (colorPalette.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.COLOR_PALETTE, true);
        }
        return this;
    }

    public WIFBuilder text(WIFText.Builder text) {
        this.text = new Option<>(text);
        if (this.text.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.TEXT, true);
        }
        return this;
    }

    public WIFBuilder weaving(WIFWeaving.Builder weaving) {
        this.weaving = new Option<>(weaving);
        if (this.weaving.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WEAVING, true);
        }
        return this;
    }

    public WIFBuilder warp(WIFWarp.Builder warp) {
        this.warp = new Option<>(warp);
        if (this.warp.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WARP, true);
        }
        return this;
    }

    public WIFBuilder weft(WIFWeft.Builder weft) {
        this.weft = new Option<>(weft);
        if (this.weft.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WEFT, true);
        }
        return this;
    }

    public WIFBuilder tieup(WIFTieup.Builder tieUp) {
        tieup = new Option<>(tieUp);
        if (this.tieup.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.TIEUP, true);
        }
        return this;
    }

    public WIFBuilder colorTable(WIFColorTable.Builder colorTable) {
        if (colorTable != null) {
            this.colorTable = new Option<>(colorTable);
            if (colorTable.getPalette().isDefined()) {
                this.colorPalette(new WIFColorPalette.Builder(colorTable.getPalette().get().getEntries(), colorTable.getPalette().get().getRange()));
            }
            if (this.colorPalette.isDefined()) {
                wifContents.contents(WIFContents.WIFContentsKey.COLOR_TABLE, true);
            }

        }
        return this;
    }

    public WIFBuilder threading(WIFThreading.Builder threading) {
        this.threading = new Option<>(threading);
        if (this.threading.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.THREADING, true);
        }
        return this;
    }

    public WIFBuilder liftPlan(WIFLiftPlan.Builder liftPlan) {
        this.liftPlan = new Option<>(liftPlan);
        if (this.liftPlan.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.LIFTPLAN, true);
        }
        return this;
    }

    public WIFBuilder warpSpacing(WIFWarpSpacing.Builder spacing) {
        this.warpSpacing = new Option<>(spacing);
        if (this.warpSpacing.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WARP_SPACING, true);
        }
        return this;
    }

    public WIFBuilder warpColors(WIFWarpColors.Builder colors) {
        this.warpColors = new Option<>(colors);
        if (this.warpColors.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WARP_COLORS, true);
        }
        return this;
    }

    public WIFBuilder warpSymbolTable(WIFWarpSymbolTable.Builder symbolTable) {
        this.warpSymbolTable = new Option<>(symbolTable);
        if (this.warpSymbolTable.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WARP_SYMBOL_TABLE, true);
        }
        return this;
    }

    public WIFBuilder warpThickness(WIFWarpThickness.Builder warpThickness) {
        this.warpThickness = new Option<>(warpThickness);
        if (this.warpThickness.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WARP_THICKNESS, true);
        }
        return this;
    }

    public WIFBuilder warpSpacingZoom(WIFWarpSpacingZoom.Builder warpSpacingZoom) {
        this.warpSpacingZoom = new Option<>(warpSpacingZoom);
        if (this.warpSpacingZoom.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WARP_SPACING_ZOOM, true);
        }
        return this;
    }

    public WIFBuilder warpSymbols(WIFWarpSymbols.Builder warpSymbols) {
        this.warpSymbols = new Option<>(warpSymbols);
        if (this.warpSymbols.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WARP_SYMBOLS, true);
        }
        return this;
    }

    public WIFBuilder treadling(WIFTreadling.Builder treadling) {
        this.treadling = new Option<>(treadling);
        if (this.treadling.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.TREADLING, true);
        }
        return this;
    }

    public WIFBuilder notes(WIFNotes.Builder notes) {
        this.notes = new Option<>(notes);
        if (this.notes.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.NOTES, true);
        }
        return this;
    }

    public WIFBuilder weftThickness(WIFWeftThickness.Builder weftThickness) {
        this.weftThickness = new Option<>(weftThickness);
        if (this.weftThickness.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WEFT_THICKNESS, true);
        }
        return this;
    }

    public WIFBuilder weftThicknessZoom(WIFWeftThicknessZoom.Builder weftThicknessZoom) {
        this.weftThicknessZoom = new Option<>(weftThicknessZoom);
        if (this.weftThicknessZoom.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WEFT_THICKNESS_ZOOM, true);
        }
        return this;
    }

    public WIFBuilder weftSpacing(WIFWeftSpacing.Builder weftSpacing) {
        this.weftSpacing = new Option<>(weftSpacing);
        if (this.weftSpacing.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WEFT_SPACING, true);
        }
        return this;
    }

    public WIFBuilder weftColors(WIFWeftColors.Builder weftColors) {
        this.weftColors = new Option<>(weftColors);
        if (this.weftColors.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WEFT_COLORS, true);
        }
        return this;
    }

    public WIFBuilder weftSymbols(WIFWeftSymbols.Builder weftSymbols) {
        this.weftSymbols = new Option<>(weftSymbols);
        if (this.weftSymbols.isDefined()) {
            wifContents.contents(WIFContents.WIFContentsKey.WEFT_SYMBOLS, true);
        }
        return this;
    }

    public WIFBuilder privateData(String sourceID, String sectionPart, String value) {
        if (sourceID != null && sectionPart != null && value != null) {
            this.privateData.put(new Pair<>(sourceID, sectionPart), value);
        }
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(wifHeader.build().toWIFSection()).append(System.lineSeparator()).append(System.lineSeparator());
        WIFContents c = wifContents.build();
        sb.append(wifContents.build().toWIFSection()).append(System.lineSeparator()).append(System.lineSeparator());
        for (WIFContents.WIFContentsKey k : c.getDefinedContents()) {
            if (k != null) {
                switch (k) {
                    case WEAVING:
                        appendContents(sb, weaving.get().build());
                        break;
                    case WARP:
                        appendContents(sb, warp.get().build());
                        break;
                    case WEFT:
                        appendContents(sb, weft.get().build());
                        break;
                    case NOTES:
                        appendContents(sb, notes.get().build());
                        break;
                    case TIEUP:
                        appendContents(sb, tieup.get().build());
                        break;
                    case COLOR_TABLE:
                        appendContents(sb, colorTable.get().build());
                        break;
                    case WARP_SYMBOL_TABLE:
                        appendContents(sb, warpSymbolTable.get().build());
                        break;
                    case THREADING:
                        appendContents(sb, threading.get().build());
                        break;
                    case WARP_THICKNESS:
                        appendContents(sb, warpThickness.get().build());
                        break;
                    case WARP_SPACING_ZOOM:
                        appendContents(sb, warpSpacingZoom.get().build());
                        break;
                    case WARP_SPACING:
                        appendContents(sb, warpSpacing.get().build());
                        break;
                    case WARP_COLORS:
                        appendContents(sb, warpColors.get().build());
                        break;
                    case WARP_SYMBOLS:
                        appendContents(sb, warpSymbols.get().build());
                        break;
                    case TREADLING:
                        appendContents(sb, treadling.get().build());
                        break;
                    case LIFTPLAN:
                        appendContents(sb, liftPlan.get().build());
                        break;
                    case WEFT_THICKNESS:
                        appendContents(sb, weftThickness.get().build());
                        break;
                    case WEFT_THICKNESS_ZOOM:
                        appendContents(sb, weftThicknessZoom.get().build());
                        break;
                    case WEFT_SPACING:
                        appendContents(sb, weftSpacing.get().build());
                        break;
                    case WEFT_COLORS:
                        appendContents(sb, weftColors.get().build());
                        break;
                    case WEFT_SYMBOLS:
                        appendContents(sb, weftSymbols.get().build());
                        break;
                    case TEXT:
                        appendContents(sb, text.get().build());
                        break;
                }
            }
        }
        for (Map.Entry<Pair<String, String>, String> ent : privateData.entrySet()) {
            sb.append("[PRIVATE ").append(ent.getKey().getFirst()).append(" ").append(ent.getKey().getSecond()).append("]").append(System.lineSeparator())
                    .append(ent.getValue()).append(System.lineSeparator());
        }
        sb.trimToSize();
        return sb.toString();
    }

    private void appendContents(StringBuilder sb, AbstractWIFObject obj) {
        sb.append(obj.toWIFSection()).append(System.lineSeparator()).append(System.lineSeparator());
    }

    public WIFReader build() {
        WIFReader r = new WIFReader(this.toString());
        r.optimize();
        return r;
    }

}
