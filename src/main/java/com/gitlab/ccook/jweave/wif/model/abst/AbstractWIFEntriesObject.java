/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.abst;

import java.util.List;
import java.util.Objects;

/**
 * Several objects define the max entries
 */
public abstract class AbstractWIFEntriesObject extends AbstractWIFObject {
    public int entries = 0;

    protected AbstractWIFEntriesObject(List<String> lines) {
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim();
                    if (key.trim().toLowerCase().equalsIgnoreCase("entries")) {
                        try {
                            entries = Integer.parseInt(value.trim());
                            if (entries < 0) {
                                entries = 0;
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }

    }

    protected AbstractWIFEntriesObject(int entries) {
        this.entries = entries;
    }

    public int getEntries() {
        return entries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractWIFEntriesObject that = (AbstractWIFEntriesObject) o;
        return entries == that.entries;
    }

    @Override
    public int hashCode() {
        return Objects.hash(entries);
    }

}
