/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.abst;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarpSymbolPalette;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarpSymbolTable;
import com.gitlab.ccook.jweave.wif.model.weft.WIFWeftSymbolPalette;
import com.gitlab.ccook.jweave.wif.model.weft.WIFWeftSymbolTable;

import java.util.*;

/**
 * Helper class which does the bulk of the work for the symbol tables for warp of weft
 */
public abstract class WIFWarpOrWeftSymbolTable extends AbstractWIFObject {
    protected Map<Integer, String> table = new TreeMap<>();
    protected Option<WIFWarpSymbolPalette> warpSymbolPalette = Option.NONE;
    protected Option<WIFWeftSymbolPalette> weftSymbolPalette = Option.NONE;

    protected WIFWarpOrWeftSymbolTable(List<String> lines) {
        parse(lines);
    }

    protected WIFWarpOrWeftSymbolTable(Map<Integer, String> table) {
        this.table = table;
    }

    private void parse(List<String> lines) {
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1];
                    int index = parseIntOrDefault(key, -1);
                    if (index > 0) {
                        table.put(index, value);
                    }
                }
            }
        }
    }

    public Map<Integer, String> getTable() {
        return table;
    }

    /**
     * Given an index, return the symbol, if defined
     *
     * @param index index of the symbol
     * @return the symbol for the given index
     */
    public Option<String> getSymbolForIndex(int index) {
        Option<String> symbolOption = new Option<>(table.get(index));
        if (symbolOption.isDefined()) {
            return symbolOption;
        }
        if (this.getClass().equals(WIFWarpSymbolTable.class) && getWarp().isDefined()) {
            Option<String> symbol = getWarp().get().getSymbol();
            if (symbol.isDefined()) {
                return symbol;
            }
            Option<Integer> symbolNumber = getWarp().get().getSymbolNumber();
            if (symbolNumber.isDefined()) {
                return new Option<>(table.get(symbolNumber.get()));
            }

        }
        if (this.getClass().equals(WIFWeftSymbolTable.class) && getWeft().isDefined()) {
            Option<String> symbol = getWeft().get().getSymbol();
            if (symbol.isDefined()) {
                return symbol;
            }
            Option<Integer> symbolNumber = getWeft().get().getSymbolNumber();
            if (symbolNumber.isDefined()) {
                return new Option<>(table.get(symbolNumber.get()));
            }
        }
        return Option.NONE;
    }

    /**
     * Returns all the indices in the symbol table
     *
     * @return all the indices in the symbol table
     */
    public List<Integer> getIndices() {
        List<Integer> indices = new ArrayList<>();
        if (this.getClass().equals(WIFWarpSymbolTable.class) && warpSymbolPalette.isDefined()) {
            for (int i = 1; i <= warpSymbolPalette.get().entries; i++) {
                indices.add(i);
            }
        } else if (this.getClass().equals(WIFWeftSymbolTable.class) && weftSymbolPalette.isDefined()) {
            for (int i = 1; i <= weftSymbolPalette.get().entries; i++) {
                indices.add(i);
            }
        } else {
            indices.addAll(table.keySet());
        }
        return indices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFWarpOrWeftSymbolTable that = (WIFWarpOrWeftSymbolTable) o;
        return Objects.equals(table, that.table) &&
                Objects.equals(warpSymbolPalette, that.warpSymbolPalette) &&
                Objects.equals(weftSymbolPalette, that.weftSymbolPalette);
    }

    @Override
    public int hashCode() {
        return Objects.hash(table, warpSymbolPalette, weftSymbolPalette);
    }


    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, String> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }

    public void setSymbolPalette(AbstractWIFEntriesObject symbolPalette) {
        if (symbolPalette instanceof WIFWarpSymbolPalette) {
            warpSymbolPalette = new Option<>((WIFWarpSymbolPalette) symbolPalette);
        } else if (symbolPalette instanceof WIFWeftSymbolPalette) {
            weftSymbolPalette = new Option<>((WIFWeftSymbolPalette) symbolPalette);
        }

    }
}
