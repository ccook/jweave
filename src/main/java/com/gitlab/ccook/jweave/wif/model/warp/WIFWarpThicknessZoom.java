/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.warp;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A magnification factor to be applied to the base thickness
 */
@SuppressWarnings("ALL")
public class WIFWarpThicknessZoom extends AbstractWIFTable<Integer> {
    public WIFWarpThicknessZoom(List<String> lines) {
        super(lines, 1);
    }

    @Override
    public Integer parseOrDefault(String value) {
        return parseIntOrDefault(value, getDefaultValue());
    }

    /**
     * If defined, magnification factor to be applied to the base thickness
     *
     * @param warp warp number to get zoom for
     * @return magnification factor to be applied to the base thickness
     */
    public Option<Integer> getZoom(int warp) {
        if (table.containsKey(warp)) {
            return new Option<>(table.get(warp));
        } else if (getWarp().isDefined()) {
            return getWarp().get().getThicknessZoom();
        }
        return Option.NONE;
    }

    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWarp().isDefined()) {
            setWarp(r.getWarp());
            int threads = r.getWarp().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, Integer> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Integer> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WARP_THICKNESS_ZOOM;
    }
}

