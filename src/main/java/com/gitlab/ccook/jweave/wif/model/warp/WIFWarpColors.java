/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.warp;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFColorTable;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;

import java.awt.*;
import java.util.List;
import java.util.*;


/**
 * Optional.
 * <p>
 * A mapping between the warp thread and the color of that warp thread
 */
public class WIFWarpColors extends AbstractWIFTable<Integer> {
    private Option<WIFColorTable> colorTable = Option.NONE;

    public WIFWarpColors(List<String> lines) {
        super(lines, 0);
    }

    public WIFWarpColors(Map<Integer, Integer> m) {
        super(m, 0);
    }

    public Option<Integer> getWarpColorIndex(int warp) {
        if (table.containsKey(warp)) {
            return new Option<>(table.get(warp));
        }
        if (getWarp().isDefined()) {
            return getWarp().get().getColorPaletteIndex();
        }
        return Option.NONE;
    }

    public Option<Color> getWarpColor(int warp) {
        if (colorTable.isDefined()) {
            if (getWarpColorIndex(warp).isDefined()) {
                return colorTable.get().lookupColor(getWarpColorIndex(warp).get());
            }
        }
        return Option.NONE;
    }

    @Override
    public Integer parseOrDefault(String value) {
        return parseIntOrDefault(value, getDefaultValue());
    }

    public void setColorTable(WIFColorTable wifColorTable) {
        this.colorTable = new Option<>(wifColorTable);
    }


    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWarp().isDefined()) {
            Set<Map.Entry<Integer, Integer>> entries = table.entrySet();
            Set<Integer> toRemove = new HashSet<>();
            for (Map.Entry<Integer, Integer> ent : entries) {
                if (ent.getKey() > r.getWarp().get().getThreads()) {
                    toRemove.add(ent.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
    }

    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WARP_COLORS;
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Integer> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WIFWarpColors that = (WIFWarpColors) o;
        return Objects.equals(colorTable, that.colorTable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), colorTable);
    }


    public Option<WIFColorTable> getColorTable() {
        return colorTable;
    }

    public static class Builder {
        private Map<Integer, Integer> colors = new TreeMap<>();

        public Builder() {
        }

        public Builder warpColor(int warpThread, int colorPaletteIndex) {
            colors.put(warpThread, colorPaletteIndex);
            return this;
        }


        public WIFWarpColors build() {
            return new WIFWarpColors(colors);
        }


    }

}
