/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.abst;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.model.helper.WIFUnit;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarpSymbolTable;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarpSymbols;
import com.gitlab.ccook.jweave.wif.model.weft.WIFWeftSymbolTable;
import com.gitlab.ccook.jweave.wif.model.weft.WIFWeftSymbols;

import java.awt.*;
import java.util.List;
import java.util.Objects;

/**
 * Information about the warp or weft.
 * Many of these can be considered default values; which may be individually overwritten
 */
@SuppressWarnings("ALL")
public abstract class AbstractWIFWarpOrWeft extends AbstractWIFObject {
    protected int threads = 1;
    private Option<Integer> colorPaletteIndex = Option.NONE;
    //TODO FIXME implement this
    private Option<Color> rgbTwoColor = Option.NONE;
    private Option<String> symbol = Option.NONE;
    private Option<Integer> symbolNumber = Option.NONE;
    private Option<WIFUnit> units = Option.NONE;
    private Option<Double> spacing = Option.NONE;
    private Option<Double> thickness = Option.NONE;
    private Option<Integer> spacingZoom = Option.NONE;
    private Option<Integer> thicknessZoom = Option.NONE;

    public AbstractWIFWarpOrWeft(List<String> lines) {
        parse(lines);
    }

    public AbstractWIFWarpOrWeft(int threads, Option<Integer> colorPaletteIndex, Option<Color> rgbTwoColor, Option<String> symbol, Option<Integer> symbolNumber,
                                 Option<WIFUnit> units, Option<Double> spacing, Option<Double> thickness, Option<Integer> spacingZoom,
                                 Option<Integer> thicknessZoom) {
        this.threads = threads;
        this.colorPaletteIndex = colorPaletteIndex;
        this.rgbTwoColor = rgbTwoColor;
        this.symbol = symbol;
        this.symbolNumber = symbolNumber;
        this.units = units;
        this.spacing = spacing;
        this.thickness = thickness;
        this.spacingZoom = spacingZoom;
        this.thicknessZoom = thicknessZoom;
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        addKeyValue(sb, "Threads", threads);
        if (colorPaletteIndex.isDefined()) {
            if (rgbTwoColor.isDefined()) {
                addKeyValue(sb, "Color", colorPaletteIndex.get() + "," + rgbTwoColor.get().getRed() + "," + rgbTwoColor.get().getGreen() + "," + rgbTwoColor.get().getBlue());
            } else {
                addKeyValue(sb, "Color", colorPaletteIndex.get());
            }
        }
        if (symbol.isDefined()) addKeyValue(sb, "Symbol", symbol);
        if (symbolNumber.isDefined()) addKeyValue(sb, "Symbol Number", symbolNumber);
        if (units.isDefined()) addKeyValue(sb, "Units", units);
        if (spacing.isDefined()) addKeyValue(sb, "Spacing", spacing);
        if (thickness.isDefined()) addKeyValue(sb, "Thickness", thickness);
        if (spacingZoom.isDefined()) addKeyValue(sb, "Spacing Zoom", spacingZoom);
        if (thicknessZoom.isDefined()) addKeyValue(sb, "Thickness Zoom", thicknessZoom);
        return sb.toString().trim();
    }

    private void parse(List<String> lines) {
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim();
                    switch (key.toLowerCase().replace(" ", "")) {
                        case "threads":
                            threads = parseIntOrDefault(value, 0);
                            break;
                        case "color":
                            String[] colorParts = value.split(",");
                            if (colorParts.length >= 1) {
                                colorPaletteIndex = new Option<>(parseIntOrDefault(colorParts[0], 0));
                            }
                            if (colorParts.length >= 4) {
                                rgbTwoColor = new Option<>(new Color(
                                        parseIntOrDefault(colorParts[1], 0),
                                        parseIntOrDefault(colorParts[2], 0),
                                        parseIntOrDefault(colorParts[3], 0)));
                            }
                            break;
                        case "symbol":
                            symbol = new Option<>(value);
                            break;
                        case "symbolnumber":
                            symbolNumber = new Option<>(parseIntOrDefault(value, 0));
                            break;
                        case "units":
                            units = new Option<>(WIFUnit.fromString(value));
                            break;
                        case "spacing":
                            spacing = new Option<>(parseDoubleOrDefault(value, 0.0));
                            break;
                        case "thickness":
                            thickness = new Option<>(parseDoubleOrDefault(value, 0.0));
                            break;
                        case "spacingzoom":
                            spacingZoom = new Option<>(parseIntOrDefault(value, 0));
                            break;
                        case "thicknesszoom":
                            thicknessZoom = new Option<>(parseIntOrDefault(value, 0));
                            break;
                    }
                }
            }
        }
    }

    /**
     * @return number of threads in the threading section
     */
    public int getThreads() {
        return threads;
    }

    //TODO FIXME use the color palette

    /**
     * If defined, returns a color index found in the ColorTable
     *
     * @return the color palette index
     */
    public Option<Integer> getColorPaletteIndex() {
        return colorPaletteIndex;
    }

    //TODO use color tripplet with color palette

    /**
     * If defined, returns the RGB value
     *
     * @return RGB Two Color
     */
    public Option<Color> getRgbTwoColor() {
        return rgbTwoColor;
    }

    /**
     * If defined, Symbol used for warp/weft
     *
     * @return Symbol used for warp/weft
     * @see WIFWarpSymbols
     * @see WIFWeftSymbols
     */
    public Option<String> getSymbol() {
        return symbol;
    }

    /**
     * If defined, lookup # for a symbol in the SymbolTable
     *
     * @return # for a symbol in the SymbolTable
     * @see WIFWarpSymbolTable
     * @see WIFWeftSymbolTable
     */
    public Option<Integer> getSymbolNumber() {
        return symbolNumber;
    }

    /**
     * If defined, units used for the warp/weft
     *
     * @return units used for the warp/weft
     */
    public Option<WIFUnit> getUnits() {
        return units;
    }

    /**
     * If defined, spacing used for the warp/weft
     *
     * @return spacing used for the warp/weft
     */
    public Option<Double> getSpacing() {
        return spacing;
    }

    /**
     * If defined, thickness used for the warp/weft
     *
     * @return thickness used for the warp/weft
     */
    public Option<Double> getThickness() {
        return thickness;
    }

    /**
     * If defined, the zoom used for the spacing for the warp/weft
     *
     * @return the zoom used for the spacing for the warp/weft
     */
    public Option<Integer> getSpacingZoom() {
        return spacingZoom;
    }

    /**
     * If defined, the zoom used for the thickness for the warp/weft
     *
     * @return the zoom used for the thickness for the warp/weft
     */
    public Option<Integer> getThicknessZoom() {
        return thicknessZoom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractWIFWarpOrWeft that = (AbstractWIFWarpOrWeft) o;
        return threads == that.threads &&
                Objects.equals(colorPaletteIndex, that.colorPaletteIndex) &&
                Objects.equals(rgbTwoColor, that.rgbTwoColor) &&
                Objects.equals(symbol, that.symbol) &&
                Objects.equals(symbolNumber, that.symbolNumber) &&
                Objects.equals(units, that.units) &&
                Objects.equals(spacing, that.spacing) &&
                Objects.equals(thickness, that.thickness) &&
                Objects.equals(spacingZoom, that.spacingZoom) &&
                Objects.equals(thicknessZoom, that.thicknessZoom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(threads, colorPaletteIndex, rgbTwoColor, symbol, symbolNumber, units, spacing, thickness, spacingZoom, thicknessZoom);
    }


    @SuppressWarnings("unchecked")
    public static abstract class Builder<T> {
        protected final int threads;
        protected Option<Integer> colorPaletteIndex = Option.NONE;
        protected Option<Color> rgbTwoColor = Option.NONE;
        protected Option<String> symbol = Option.NONE;
        protected Option<Integer> symbolNumber = Option.NONE;
        protected Option<WIFUnit> units = Option.NONE;
        protected Option<Double> spacing = Option.NONE;
        protected Option<Double> thickness = Option.NONE;
        protected Option<Integer> spacingZoom = Option.NONE;
        protected Option<Integer> thicknessZoom = Option.NONE;

        public Builder(int threads) {
            this.threads = threads;
        }

        public T rgbTwoColor(Color c) {
            this.rgbTwoColor = new Option<>(c);
            return (T) this;
        }

        public T symbol(String symbol) {
            this.symbol = new Option<>(symbol);
            return (T) this;
        }

        public T symbolNumber(Integer symbolNumber) {
            this.symbolNumber = new Option<>(symbolNumber);
            return (T) this;
        }

        public T units(WIFUnit unit) {
            this.units = new Option<>(unit);
            return (T) this;
        }

        public T spacing(Double spacing) {
            this.spacing = new Option<>(spacing);
            return (T) this;
        }

        public T thickness(Double thickness) {
            this.thickness = new Option<>(thickness);
            return (T) this;
        }

        public T spacingZoom(Integer spacingZoom) {
            this.spacingZoom = new Option<>(spacingZoom);
            return (T) this;
        }

        public T thicknessZoom(Integer thicknessZoom) {
            this.thicknessZoom = new Option<>(thicknessZoom);
            return (T) this;
        }

        public T colorPaletteIndex(Integer colorPaletteIndex) {
            this.colorPaletteIndex = new Option<>(colorPaletteIndex);
            return (T) this;
        }

        public abstract AbstractWIFWarpOrWeft build();

    }

    protected List<Pair<List<Integer>, Option<Color>>> reduce(List<Pair<List<Integer>, Option<Color>>> reducedPattern) {
        List<Pair<List<Integer>, Option<Color>>> firstHalf = reducedPattern.subList(0, reducedPattern.size() / 2);
        List<Pair<List<Integer>, Option<Color>>> secondHalf = reducedPattern.subList(reducedPattern.size() / 2, reducedPattern.size());
        if (firstHalf.equals(secondHalf)) {
            return secondHalf;
        }
        return reducedPattern;
    }
}

