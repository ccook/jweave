/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.warp;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;
import com.gitlab.ccook.jweave.wif.model.helper.WIFUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * The thickness of each warp thread in the specified unit.
 */
public class WIFWarpThickness extends AbstractWIFTable<Double> {
    private WIFUnit units = WIFUnit.UNKNOWN;


    public WIFWarpThickness(List<String> lines) {
        super(lines, 0.0);
    }

    public WIFWarpThickness(Map<Integer, Double> table) {
        super(table, 0.0);
    }

    public void setUnits(WIFUnit units) {
        this.units = units;
    }

    /**
     * If defined, returns the thickness for a warp thread
     *
     * @param warp warp number to get thickness for
     * @return returns the thickness for a warp thread
     */
    public Option<Pair<Double, WIFUnit>> getThickness(int warp) {
        if (getWarp().isDefined()) {
            Option<WIFUnit> units = getWarp().get().getUnits();
            if (units.isDefined()) {
                setUnits(units.get());
            }
        }
        if (table.containsKey(warp)) {
            return new Option<>(new Pair<>(table.get(warp), this.units));
        }
        if (getWarp().isDefined()) {
            Option<Double> thickness = getWarp().get().getThickness();
            if (thickness.isDefined()) {
                return new Option<>(new Pair<>(thickness.get(), units));
            }
        }
        return Option.NONE;
    }

    @Override
    public Double parseOrDefault(String value) {
        return parseDoubleOrDefault(value, 0.0);
    }

    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWarp().isDefined()) {
            setWarp(r.getWarp());
            Option<WIFUnit> units = getWarp().get().getUnits();
            if (units.isDefined()) {
                setUnits(units.get());
            }
            int threads = r.getWarp().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, Double> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Double> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WARP_THICKNESS;
    }

    public class Builder {
        private WIFUnit units = WIFUnit.UNKNOWN;
        private Map<Integer, Double> table = new TreeMap<>();

        public Builder() {

        }

        public Builder(WIFUnit units) {
            this.units = units;
        }

        public Builder units(WIFUnit units) {
            this.units = units;
            return this;
        }

        public Builder thickness(int warp, double thickness) {
            table.put(warp, thickness);
            return this;
        }

        public WIFWarpThickness build() {
            WIFWarpThickness wifWarpThickness = new WIFWarpThickness(table);
            wifWarpThickness.setUnits(units);
            return wifWarpThickness;
        }
    }
}
