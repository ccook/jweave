/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.helper;

import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.model.WIFColorPalette;
import com.gitlab.ccook.jweave.wif.model.WIFColorTable;

import java.awt.*;
import java.util.Objects;

/**
 * A helper object that holds the raw RGB values found in the WIF file
 * Given a WIFColorPalette, it will decode the raw RGB value to a real RGB value (with values between 0 and 255)
 *
 * @see WIFColorPalette
 * @see WIFColorTable
 */
public class WIFColorTripplet {

    private final int r;
    private final int g;
    private final int b;

    public WIFColorTripplet(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public Color getColor(int range) {
        return getColor(0, range);
    }

    public Color getColor(int rangeStart, int rangeEnd) {
        int range = rangeEnd - rangeStart;
        int newR = (int) Math.round(255.0 * r / range);
        int newG = (int) Math.round(255.0 * g / range);
        int newB = (int) Math.round(255.0 * b / range);
        return new Color(newR, newG, newB);
    }

    public Color getColor(WIFColorPalette palette) {
        Pair<Integer, Integer> range = palette.getRange();
        return getColor(range.getFirst(), range.getSecond());
    }

    public int getRawR() {
        return r;
    }

    public int getRawG() {
        return g;
    }

    public int getRawB() {
        return b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFColorTripplet that = (WIFColorTripplet) o;
        return r == that.r &&
                g == that.g &&
                b == that.b;
    }

    @Override
    public int hashCode() {
        return Objects.hash(r, g, b);
    }

    @Override
    public String toString() {
        return "WIFColorTripplet{" +
                "r=" + r +
                ", g=" + g +
                ", b=" + b +
                '}';
    }
}
