/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.weft;

import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;

import java.util.*;

/**
 * Optional
 * <p>
 * Lift Plans are drafts meant for table looms (or any loom without treadles/tieups).
 * WIF Lift plans are mappings between the pick and shafts raised
 * <p>
 * e.g
 * <p>
 * [LIFTPLAN]
 * 1=1,2,3,4,9,10,11,12,17,18,19,20,25,26,27,28
 * 2=1,2,7,8,9,10,15,16,17,18,23,24,25,26,31,32
 * 3=1,4,5,7,9,12,13,15,17,20,21,23,25,28,29,31
 * 4=1,3,5,8,9,11,13,16,17,19,21,24,25,27,29,32
 * 5=3,4,5,6,11,12,13,14,19,20,21,22,27,28,29,30
 * 6=5,6,7,8,13,14,15,16,21,22,23,24,29,30,31,32
 * 7=2,3,6,8,10,11,14,16,18,19,22,24,26,27,30,32
 * 8=2,4,6,7,10,12,14,15,18,20,22,23,26,28,30,31
 * 9=1,2,3,4,9,10,11,12,17,18,19,20,25,26,27,28
 * 10=1,2,7,8,9,10,15,16,17,18,23,24,25,26,31,32
 * 11=1,4,5,7,9,12,13,15,17,20,21,23,25,28,29,31
 * 12=1,3,5,8,9,11,13,16,17,19,21,24,25,27,29,32
 * 13=3,4,5,6,11,12,13,14,19,20,21,22,27,28,29,30
 * 14=5,6,7,8,13,14,15,16,21,22,23,24,29,30,31,32
 * 15=2,3,6,8,10,11,14,16,18,19,22,24,26,27,30,32
 * 16=2,4,6,7,10,12,14,15,18,20,22,23,26,28,30,31
 * ...
 */
public class WIFLiftPlan extends AbstractWIFTable<List<Integer>> {
    public WIFLiftPlan(List<String> lines) {
        super(lines, new ArrayList<>());
    }

    public WIFLiftPlan(Map<Integer, List<Integer>> liftPlanParts) {
        super(makeLines(liftPlanParts), new ArrayList<>());
    }

    private static List<String> makeLines(Map<Integer, List<Integer>> liftPlanParts) {
        List<String> toReturn = new ArrayList<>();
        for (Map.Entry<Integer, List<Integer>> ent : liftPlanParts.entrySet()) {
            Integer pick = ent.getKey();
            List<Integer> shaftsEngaged = ent.getValue();
            StringBuilder sb = new StringBuilder(pick + "=");
            for (Integer i : shaftsEngaged) {
                sb.append(i).append(" ");
            }
            toReturn.add(sb.toString().trim().replace(" ", ","));
        }
        return toReturn;
    }


    /**
     * @param pick the pick number
     * @return shafts raised for a given pick (if rising shed = true, otherwise shafts lowered)
     */
    public List<Integer> getShaftsForPick(int pick) {
        if (table.containsKey(pick)) {
            return table.get(pick);
        }
        return new ArrayList<>();
    }

    @Override
    public List<Integer> parseOrDefault(String value) {
        String[] parts = value.split(",");
        List<Integer> parsed = new ArrayList<>();
        for (String part : parts) {
            parsed.add(parseIntOrDefault(part, 0));
        }
        return parsed;
    }

    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWeft().isDefined()) {
            Set<Map.Entry<Integer, List<Integer>>> entries = table.entrySet();
            Set<Integer> toRemove = new HashSet<>();
            for (Map.Entry<Integer, List<Integer>> ent : entries) {
                if (ent.getKey() > r.getWeft().get().getThreads()) {
                    toRemove.add(ent.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.LIFTPLAN;
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        int size = table.keySet().size();
        for (int i = 1; i <= size; i++) {
            List<Integer> shaftsForPick = getShaftsForPick(i);
            sb.append(i).append("=");
            for (Integer shaft : shaftsForPick) {
                sb.append(shaft).append(",");
            }
            if (!shaftsForPick.isEmpty()) {
                sb = new StringBuilder(sb.substring(0, sb.length() - 1));
            }
            sb.append("\n");
        }
        return sb.toString().trim();
    }

    public static class Builder {
        private final Map<Integer, List<Integer>> liftplan = new HashMap<>();

        public Builder() {
        }


        public Builder liftPlan(int pick, List<Integer> shafts) {
            if (liftplan.containsKey(pick)) {
                liftplan.get(pick).addAll(shafts);
            } else {
                liftplan.put(pick, shafts);
            }
            return this;
        }

        public Builder liftPlan(int pick, int... shafts) {
            List<Integer> shaftList = new ArrayList<>();
            for (int shaft : shafts) {
                shaftList.add(shaft);

            }
            liftPlan(pick, shaftList);
            return this;
        }

        public Builder liftPlan(Map<Integer, List<Integer>> liftplan) {
            this.liftplan.putAll(liftplan);
            return this;
        }

        public WIFLiftPlan build() {
            return new WIFLiftPlan(liftplan);
        }
    }

}
