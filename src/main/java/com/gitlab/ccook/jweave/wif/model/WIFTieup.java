/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model;

import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFObject;

import java.util.*;

/**
 * Optional.
 * A table mapping the treadle to the shafts
 * <p>
 * e.g
 * <p>
 * [TIEUP]
 * 1=1,4,7,8
 * 2=1,2,8
 * 3=1,2,3
 * 4=2,3,4
 * 5=3,4,5,8
 * 6=1,4,6
 * 7=2,5,7
 * 8=3,6,8
 */
public class WIFTieup extends AbstractWIFObject {
    private Map<Integer, List<Integer>> tieUp = new TreeMap<>();

    public WIFTieup(List<String> lines) {
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim();
                    int treadle = parseIntOrDefault(key, 1);
                    String[] valueParts = value.split(",");
                    List<Integer> shafts = new ArrayList<>();
                    for (String shaft : valueParts) {
                        shafts.add(parseIntOrDefault(shaft.trim(), 0));
                    }
                    tieUp.put(treadle, shafts);
                }
            }
        }
    }

    public WIFTieup(Map<Integer, List<Integer>> tieUp) {
        this.tieUp = tieUp;
    }

    public Map<Integer, List<Integer>> getTieUp() {
        return tieUp;
    }

    /**
     * Returns the shafts tied up to a given treadle
     *
     * @param treadle given treadle to get shafts tied
     * @return the shafts tied up to a given treadle
     */
    public List<Integer> getShaftsForTreadle(int treadle) {
        List<Integer> shafts = tieUp.get(treadle);
        if (shafts != null) {
            return shafts;
        }

        return new ArrayList<>();
    }

    /**
     * Returns the list of treadles
     *
     * @return the list of treadles
     */
    public List<Integer> getTreadles() {
        List<Integer> treadles = new ArrayList<>();
        if (getWeaving().isDefined()) {
            for (int i = 1; i <= getWeaving().get().getTreadles(); i++) {
                treadles.add(i);
            }
            return treadles;
        } else {
            treadles.addAll(tieUp.keySet());
            return treadles;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFTieup wifTieup = (WIFTieup) o;
        return Objects.equals(tieUp, wifTieup.tieUp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tieUp);
    }

    /**
     * Removes unused treadles
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        List<Integer> treadlesUsed = new ArrayList<>();
        if (r.getTreadling().isDefined()) {
            for (Map.Entry<Integer, List<Integer>> treadling : r.getTreadling().get().getTable().entrySet()) {
                treadlesUsed.addAll(treadling.getValue());
            }
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, List<Integer>> ent : tieUp.entrySet()) {
                if (!treadlesUsed.contains(ent.getKey())) {
                    toRemove.add(ent.getKey());
                }
            }
            for (Integer i : toRemove) {
                tieUp.remove(i);
            }
        }
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, List<Integer>> ent : tieUp.entrySet()) {
            StringBuilder value = new StringBuilder();
            for (Integer i : ent.getValue()) {
                value.append(i).append(" ");
            }
            addKeyValue(sb, ent.getKey(), value.toString().trim().replace(" ", ","));
        }
        return sb.toString().trim();
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.TIEUP;
    }


    public static class Builder {
        private final Map<Integer, List<Integer>> tieup = new HashMap<>();

        public Builder() {
        }

        public Builder tieup(int treadle, int... shafts) {
            if (!tieup.containsKey(treadle)) {
                tieup.put(treadle, new ArrayList<>());
            }
            for (int shaft : shafts) {
                tieup.get(treadle).add(shaft);
            }
            return this;
        }

        public WIFTieup build() {
            return new WIFTieup(tieup);
        }

        @Override
        public String toString() {
            return build().toString();
        }
    }
}
