/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.abst;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.WIFWeaving;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarp;
import com.gitlab.ccook.jweave.wif.model.weft.WIFWeft;


/**
 * Abstract object for all WIF objects
 */
@SuppressWarnings("unchecked")
public abstract class AbstractWIFObject {
    private Option<WIFWeaving> weaving = Option.NONE;
    private Option<WIFWarp> warp = Option.NONE;
    private Option<WIFWeft> weft = Option.NONE;


    protected Integer parseIntOrDefault(String toParse, int defaultInt) {
        try {
            return Integer.parseInt(toParse.trim());
        } catch (Exception e) {
            return defaultInt;
        }
    }

    protected Double parseDoubleOrDefault(String value, Double defaultValue) {
        try {
            return Double.parseDouble(value.trim());
        } catch (Exception e) {
        }
        return defaultValue;
    }

    protected Option<Boolean> parseBoolean(String value) {
        value = value.toLowerCase().trim();
        boolean val = value.equalsIgnoreCase("true") ||
                value.equalsIgnoreCase("on") ||
                value.equalsIgnoreCase("yes") ||
                value.equalsIgnoreCase("1") ||
                value.equalsIgnoreCase("t");
        return new Option<>(val);

    }

    protected StringBuilder addKeyValue(StringBuilder sb, Object key, Object value) {
        sb.append(key).append("=").append(value).append("\n");
        return sb;
    }

    public void setWeaving(Option<WIFWeaving> weaving) {
        this.weaving = weaving;
    }

    public void setWarp(Option<WIFWarp> warp) {
        this.warp = warp;
    }

    public void setWeft(Option<WIFWeft> weft) {
        this.weft = weft;
    }


    public Option<WIFWeaving> getWeaving() {
        return weaving;
    }

    public Option<WIFWarp> getWarp() {
        return warp;
    }

    public Option<WIFWeft> getWeft() {
        return weft;
    }

    @Override
    public abstract boolean equals(Object o);

    @Override
    public abstract int hashCode();

    public abstract void optimize(WIFReader r);

    public abstract WIFContents.WIFContentsKey getWIFSectionName();

    public abstract String toWIFSection();

    @Override
    public String toString() {
        return toWIFSection();
    }
}

