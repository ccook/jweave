/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif;

import java.io.*;

public class WIFWriter {

    private StringBuilder sb = new StringBuilder();


    public WIFWriter(WIFReader r) {
        sb = new StringBuilder(r.toString());
    }

    public WIFWriter(WIFBuilder b) {
        b.toString();

    }

    public void write(OutputStream os) {
        PrintStream printStream = new PrintStream(os);
        printStream.print(sb.toString());
        printStream.flush();
        printStream.close();
    }

    public void write(File f) throws FileNotFoundException {
        if (!f.getParentFile().exists()) {
            f.getParentFile().mkdirs();
        }
        FileOutputStream fos = new FileOutputStream(f);
        write(fos);
    }
}
