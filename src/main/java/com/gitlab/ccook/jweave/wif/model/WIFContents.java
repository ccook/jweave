/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Required
 * <p>
 * A section which defines which sections are found in the WIF file.
 * While required, I wouldn't solely trust the CONTENTS section, as
 * I have found instances where it lies. (e.g WARP COLORS=Yes when no WARP COLORS section exists)
 * <p>
 * The WIFReader does read the CONTENTS section; but does not rely on it.
 * <p>
 * e.g
 * <p>
 * [CONTENTS]
 * COLOR PALETTE=true
 * TEXT=true
 * WEAVING=true
 * WARP=true
 * WEFT=true
 * COLOR TABLE=true
 * THREADING=true
 * TIEUP=true
 * TREADLING=true
 * WARP COLORS=true
 * WEFT COLORS=true
 * <p>
 * This object does support PRIVATE sections, which are found in privateData.
 * <p>
 * This object is interacted with using WIFContentsKey, an enum with all the allowed values (except PRIVATE)
 *
 * @see WIFContentsKey
 */
public class WIFContents extends AbstractWIFObject {

    private static Logger log = LoggerFactory.getLogger(WIFContents.class);


    public enum WIFContentsKey {
        COLOR_PALETTE("COLOR PALETTE"),
        WARP_SYMBOL_PALETTE("WARP SYMBOL PALETTE"),
        WEFT_SYMBOL_PALETTE("WEFT SYMBOL PALETTE"),
        TEXT("TEXT"),
        WEAVING("WEAVING"),
        WARP("WARP"),
        WEFT("WEFT"),
        BITMAP_IMAGE("BITMAP IMAGE"),
        BITMAP_FILE("BITMAP FILE"),
        NOTES("NOTES"),
        TIEUP("TIEUP"),
        COLOR_TABLE("COLOR TABLE"),
        WARP_SYMBOL_TABLE("WARP SYMBOL TABLE"),
        WEFT_SYMBOL_TABLE("WEFT SYMBOL TABLE"),
        THREADING("THREADING"),
        WARP_THICKNESS("WARP THICKNESS"),
        WARP_THICKNESS_ZOOM("WARP THICKNESS ZOOM"),
        WARP_SPACING("WARP SPACING"),
        WARP_SPACING_ZOOM("WARP SPACING ZOOM"),
        WARP_COLORS("WARP COLORS"),
        WARP_SYMBOLS("WARP SYMBOLS"),
        TREADLING("TREADLING"),
        LIFTPLAN("LIFTPLAN"),
        WEFT_THICKNESS("WEFT THICKNESS"),
        WEFT_THICKNESS_ZOOM("WEFT THICKNESS ZOOM"),
        WEFT_SPACING("WEFT SPACING"),
        WEFT_SPACING_ZOOM("WEFT SPACING ZOOM"),
        WEFT_COLORS("WEFT COLORS"),
        WEFT_SYMBOLS("WEFT SYMBOLS");
        private final String title;

        WIFContentsKey(String title) {
            this.title = title;
        }

        @Override
        public String toString() {
            return title;
        }

        public static Option<WIFContentsKey> fromString(String thing) {
            if (thing != null) {
                for (WIFContentsKey k : WIFContentsKey.values()) {
                    if (k.title.toLowerCase().replace(" ", "")
                            .equalsIgnoreCase(thing.trim().replace(" ", "").toLowerCase())) {
                        return new Option<>(k);
                    }
                }
            }
            return Option.NONE;
        }
    }

    private Map<WIFContentsKey, Boolean> contents = new LinkedHashMap<>();
    private Map<Pair<String, String>, Boolean> privateData = new LinkedHashMap<>();

    public WIFContents(List<String> lines) {
        Builder builder = new Builder();
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim();
                    Option<WIFContentsKey> k = WIFContentsKey.fromString(key.toLowerCase().replace(" ", ""));
                    if (k.isDefined()) {
                        builder.contents(k.get(), parseBoolean(value).get());
                    } else {
                        if (key.toUpperCase().trim().startsWith("PRIVATE")) {
                            String[] privateParts = key.split(" ", 3);
                            String sourceID = UUID.randomUUID().toString();
                            String sectionName = UUID.randomUUID().toString();
                            if (privateParts.length > 1) {
                                sourceID = privateParts[1];
                            }
                            if (privateParts.length > 2) {
                                sectionName = privateParts[2];
                            }
                            builder.privateData(sourceID, sectionName, parseBoolean(value).get());
                        }
                    }
                }
            }
        }
        setWIFContents(builder.contents, builder.privateData);

    }

    public WIFContents(Builder builder) {
        this(builder.contents, builder.privateData);
    }

    public WIFContents(Map<WIFContentsKey, Boolean> contents, Map<Pair<String, String>, Boolean> privateData) {
        setWIFContents(contents, privateData);
    }

    private void setWIFContents(Map<WIFContentsKey, Boolean> contents, Map<Pair<String, String>, Boolean> privateData) {
        this.contents = contents;
        this.privateData = privateData;
    }

    public Set<WIFContentsKey> getDefinedContents() {
        return contents.keySet();
    }

    /**
     * Returns defined and true if the CONTENTS section says that section is present
     * Returns defined and false if the CONTENTS section knows that section is absent
     * Returns undefined if the CONTENTS section does not mention if that section is present/absent
     * In theory, undefined should be interpreted as false, but always check.
     * Use getPrivateDataSections()/hasPrivateDataSection() for PRIVATE sections
     *
     * @param k the section name to look for in the CONTENTS section
     * @return if the contents of the section are defined
     * @see Option
     */
    public Option<Boolean> hasContents(WIFContentsKey k) {
        return new Option<>(contents.get(k));
    }


    /**
     * Retrieves private data, if found, located by its source id and section.
     * <p>
     * e.g PRIVATE [SourceID] [SectionName]
     *
     * @param sourceID part of the private coordinates
     * @param secionName part of the private coordinates
     * @return value of the private coordinates
     */
    public Option<Boolean> hasPrivateDataSection(String sourceID, String secionName) {
        return new Option<>(privateData.get(new Pair<>(sourceID, secionName)));
    }

    /**
     * Returns all the private data sections in the CONTENTS section
     *
     * @return all the private data sections in the CONTENTS section
     */
    public Set<Pair<String, String>> getPrivateDataSections() {
        return privateData.keySet();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFContents that = (WIFContents) o;
        return Objects.equals(contents, that.contents) &&
                Objects.equals(privateData, that.privateData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contents, privateData);
    }


    /**
     * Fills out the WIF Contents with every possible section
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        contents.put(WIFContentsKey.TIEUP, r.getTieup().isDefined());
        contents.put(WIFContentsKey.WEAVING, r.getWeaving().isDefined());
        contents.put(WIFContentsKey.LIFTPLAN, r.getLiftPlan().isDefined());
        contents.put(WIFContentsKey.THREADING, r.getThreading().isDefined());
        contents.put(WIFContentsKey.WEFT, r.getWeft().isDefined());
        contents.put(WIFContentsKey.COLOR_PALETTE, r.getColorPalette().isDefined());
        contents.put(WIFContentsKey.WARP_SYMBOL_PALETTE, r.getWarpSymbolPalette().isDefined());
        contents.put(WIFContentsKey.WEFT_SYMBOL_PALETTE, r.getWeftSymbolPalette().isDefined());
        contents.put(WIFContentsKey.TEXT, r.getText().isDefined());
        contents.put(WIFContentsKey.WARP, r.getWarp().isDefined());
        contents.put(WIFContentsKey.NOTES, r.getNotes().isDefined());
        contents.put(WIFContentsKey.COLOR_TABLE, r.getColorTable().isDefined());
        contents.put(WIFContentsKey.TREADLING, r.getTreadling().isDefined());
        contents.put(WIFContentsKey.WARP_SYMBOL_TABLE, r.getWarpSymbolTable().isDefined());
        contents.put(WIFContentsKey.WEFT_SYMBOL_TABLE, r.getWeftSymbolTable().isDefined());
        contents.put(WIFContentsKey.WARP_THICKNESS, r.getWarpThickness().isDefined());
        contents.put(WIFContentsKey.WARP_THICKNESS_ZOOM, r.getWarpThicknessZoom().isDefined());
        contents.put(WIFContentsKey.WARP_SPACING, r.getWarpSpacing().isDefined());
        contents.put(WIFContentsKey.WARP_SPACING_ZOOM, r.getWarpSpacingZoom().isDefined());
        contents.put(WIFContentsKey.WARP_COLORS, r.getWarpColors().isDefined());
        contents.put(WIFContentsKey.WARP_SYMBOLS, r.getWarpSymbols().isDefined());
        contents.put(WIFContentsKey.WEFT_THICKNESS, r.getWeftThickness().isDefined());
        contents.put(WIFContentsKey.WEFT_THICKNESS_ZOOM, r.getWeftThicknessZoom().isDefined());
        contents.put(WIFContentsKey.WEFT_SPACING, r.getWeftSpacing().isDefined());
        contents.put(WIFContentsKey.WEFT_SPACING_ZOOM, r.getWeftSpacingZoom().isDefined());
        contents.put(WIFContentsKey.WEFT_COLORS, r.getWeftColors().isDefined());
    }


    @Override
    public WIFContentsKey getWIFSectionName() {
        return null;
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append("CONTENTS").append("]").append("\n");
        for (Map.Entry<WIFContentsKey, Boolean> ent : contents.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue().toString().toLowerCase());
        }
        for (Map.Entry<Pair<String, String>, Boolean> ent : privateData.entrySet()) {
            addKeyValue(sb, "PRIVATE " + ent.getKey().getFirst() + " " + ent.getKey().getSecond(), ent.getValue().toString().toLowerCase());
        }

        return sb.toString().trim();
    }

    public static class Builder {

        private final HashMap<WIFContentsKey, Boolean> contents = new LinkedHashMap<>();
        private final Map<Pair<String, String>, Boolean> privateData = new LinkedHashMap<>();

        public Builder() {
        }

        public Builder doesContain(WIFContentsKey... ks) {
            if (ks != null) {
                for (WIFContentsKey k : ks) {
                    if (k != null) {
                        contents.put(k, true);
                    }
                }
            }
            return this;
        }

        public Builder contents(WIFContentsKey k, boolean exists) {
            if (k != null) {
                contents.put(k, exists);
            }
            return this;
        }

        public Builder privateData(String sourceID, String section, boolean exists) {
            if (sourceID == null) {
                sourceID = UUID.randomUUID().toString();
            }
            if (section == null) {
                section = UUID.randomUUID().toString();
            }
            privateData.put(new Pair<>(sourceID, section), exists);
            return this;
        }

        public WIFContents build() {
            return new WIFContents(contents, privateData);
        }
    }

}

