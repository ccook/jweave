/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.weft;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;

import java.util.*;


/**
 * Mapping between the weft number and the symbol table index
 */
public class WIFWeftSymbols extends AbstractWIFTable<Integer> {
    private Option<WIFWeftSymbolTable> symbolTable = Option.NONE;

    public WIFWeftSymbols(List<String> lines) {
        super(lines, 0);
    }

    public WIFWeftSymbols(Map<Integer, Integer> table) {
        super(table, 0);
    }

    @Override
    public Integer parseOrDefault(String value) {
        return parseIntOrDefault(value, getDefaultValue());
    }

    public Option<Integer> getSymbolTableIndex(int weft) {
        Option<Integer> symbolIndex = new Option<>(table.get(weft));
        if (symbolIndex.isDefined()) {
            return symbolIndex;
        }
        if (getWeft().isDefined()) {
            return getWeft().get().getSymbolNumber();
        }
        return Option.NONE;
    }

    public Option<String> getSymbol(int weft) {
        if (symbolTable.isDefined() && getSymbolTableIndex(weft).isDefined()) {
            if (getSymbolTableIndex(weft).isDefined()) {
                return symbolTable.get().getSymbolForIndex(getSymbolTableIndex(weft).get());
            }
            if (getWeft().isDefined()) {
                Option<String> defaultSymbol = getWeft().get().getSymbol();
                if (defaultSymbol.isDefined()) {
                    return defaultSymbol;
                }
            }
        }

        return Option.NONE;
    }


    public void setSymbolTable(WIFWeftSymbolTable symbolTable) {
        this.symbolTable = new Option<>(symbolTable);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WIFWeftSymbols that = (WIFWeftSymbols) o;
        return Objects.equals(symbolTable, that.symbolTable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), symbolTable);
    }

    /**
     * Removes unused symbols
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWeft().isDefined()) {
            setWeft(r.getWeft());
            int threads = r.getWeft().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, Integer> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
        if (r.getWeftSymbolTable().isDefined()) {
            this.symbolTable = r.getWeftSymbolTable();
        }
    }

    public Option<WIFWeftSymbolTable> getSymbolTable() {
        return symbolTable;
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WEFT_SYMBOLS;
    }


    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Integer> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }

    public class Builder {
        private Map<Integer, Integer> table = new TreeMap<>();

        public Builder weftSymbol(Integer weft, Integer symbolIndex) {
            table.put(weft, symbolIndex);
            return this;
        }

        public WIFWeftSymbols build() {
            return new WIFWeftSymbols(table);
        }
    }

}
