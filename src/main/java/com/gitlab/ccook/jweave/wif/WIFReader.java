/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.model.*;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFObject;
import com.gitlab.ccook.jweave.wif.model.warp.*;
import com.gitlab.ccook.jweave.wif.model.weft.*;

import java.io.InputStream;
import java.util.*;

/**
 * A reader for WIF files; it takes as little assumptions as possible
 * Please call close() when finished loading
 */
@SuppressWarnings("ALL")
public class WIFReader {
    private Scanner scan;
    private WIFHeader header;
    private WIFContents contents;
    private Option<WIFColorPalette> colorPalette = Option.NONE;
    private Option<WIFWarpSymbolPalette> warpSymbolPalette = Option.NONE;
    private Option<WIFWeftSymbolPalette> weftSymbolPalette = Option.NONE;
    private Option<WIFText> text = Option.NONE;
    private Option<WIFWeaving> weaving = Option.NONE;
    private Option<WIFWarp> warp = Option.NONE;
    private Option<WIFWeft> weft = Option.NONE;
    private Option<WIFNotes> notes = Option.NONE;
    private Option<WIFTieup> tieup = Option.NONE;
    private Option<WIFColorTable> colorTable = Option.NONE;
    private Option<WIFWarpSymbolTable> warpSymbolTable = Option.NONE;
    private Option<WIFWeftSymbolTable> weftSymbolTable = Option.NONE;
    private Option<WIFThreading> threading = Option.NONE;
    private Option<WIFWarpThickness> warpThickness = Option.NONE;
    private Option<WIFWarpThicknessZoom> warpThicknessZoom = Option.NONE;
    private Option<WIFWarpSpacing> warpSpacing = Option.NONE;
    private Option<WIFWarpSpacingZoom> warpSpacingZoom = Option.NONE;
    private Option<WIFWarpColors> warpColors = Option.NONE;
    private Option<WIFWarpSymbols> warpSymbols = Option.NONE;
    private Option<WIFTreadling> treadling = Option.NONE;
    private Option<WIFLiftPlan> liftPlan = Option.NONE;
    private Option<WIFWeftThickness> weftThickness = Option.NONE;
    private Option<WIFWeftThicknessZoom> weftThicknessZoom = Option.NONE;
    private Option<WIFWeftSpacing> weftSpacing = Option.NONE;
    private Option<WIFWeftSpacingZoom> weftSpacingZoom = Option.NONE;
    private Option<WIFWeftColors> weftColors = Option.NONE;
    private Option<WIFWeftSymbols> weftSymbols = Option.NONE;
    private Map<Pair<String, String>, String> privateData = new LinkedHashMap<>();
    private Set<WIFContents.WIFContentsKey> orderRead = new LinkedHashSet<>();

    WIFReader(Scanner scan) {
        this.scan = scan;
        scan.useDelimiter("\\r?\\n");
        Option<String> section = Option.NONE;
        List<String> lines = new ArrayList<>();
        while (scan.hasNext()) {
            String line = scan.next();
            if (line.trim().toLowerCase().startsWith("[") && line.trim().toLowerCase().endsWith("]")) {
                if (section.isDefined()) {
                    handleSection(section, lines);
                    lines = new ArrayList<>();
                }
                section = new Option<>(line.replace("[", "").replace("]", "").trim());
            } else {
                lines.add(line);
            }
        }
        if (section.isDefined()) {
            handleSection(section, lines);
        }
        scan.close();
        updateFinalValues(false);
    }

    public WIFReader(InputStream is) {
        this(new Scanner(is));
    }

    public WIFReader(String input) {
        this(new Scanner(input));
    }

    public void optimize() {
        updateFinalValues(true);
        updateFinalValues(true);
    }

    private void updateFinalValues(boolean shouldOptimize) {
        if (shouldOptimize) {
            contents.optimize(this);
        }
        /*
         * COLOR_PALETTE
         *     -> COLOR_TABLE
         *         -> WARP_COLORS
         *         -> WEFT_COLORS
         */
        if (colorPalette.isDefined()) {
            if (shouldOptimize) {
                colorPalette.get().optimize(this);
            }
            if (colorTable.isDefined()) {
                colorTable.get().setColorPalette(colorPalette);
                if (shouldOptimize) {
                    colorTable.get().optimize(this);
                }
                if (warpColors.isDefined()) {
                    warpColors.get().setColorTable(colorTable.get());
                    if (shouldOptimize) {
                        warpColors.get().optimize(this);
                    }
                }
                if (weftColors.isDefined()) {
                    weftColors.get().setColorTable(colorTable.get());
                    if (shouldOptimize) {
                        weftColors.get().optimize(this);
                    }
                }
            }
        }

        /*
         * WARP_SYMBOL_PALETTE
         *     -> WARP_SYMBOL_TABLE
         *         -> WARP_SYMBOLS
         */
        if (warpSymbolPalette.isDefined()) {
            if (shouldOptimize) {
                warpSymbolPalette.get().optimize(this);
            }
            if (warpSymbolTable.isDefined()) {
                warpSymbolTable.get().setSymbolPalette(warpSymbolPalette.get());
                if (shouldOptimize) {
                    warpSymbolTable.get().optimize(this);
                }
                if (warpSymbols.isDefined()) {
                    warpSymbols.get().setSymbolTable(warpSymbolTable.get());
                    if (shouldOptimize) {
                        warpSymbols.get().optimize(this);
                    }
                }
            }
        }
        /*
            WEFT_SYMBOL_PALETTE
                -> WEFT_SYMBOL_TABLE
                     -> WEFT_SYMBOLS
         */
        if (weftSymbolPalette.isDefined()) {
            if (shouldOptimize) {
                weftSymbolPalette.get().optimize(this);
            }
            if (weftSymbolTable.isDefined()) {
                weftSymbolTable.get().setSymbolPalette(weftSymbolPalette.get());
                if (shouldOptimize) {
                    weftSymbolTable.get().optimize(this);
                }
                if (weftSymbols.isDefined()) {
                    weftSymbols.get().setSymbolTable(weftSymbolTable.get());
                    if (shouldOptimize) {
                        weftSymbols.get().optimize(this);
                    }
                }
            }
        }

        /*
             WEAVING
                -> TIEUP
                -> THREADING
                -> TREADLING
                -> LIFTPLAN
         */
        if (weaving.isDefined()) {
            if (shouldOptimize) {
                weaving.get().optimize(this);
            }
            if (tieup.isDefined()) {
                tieup.get().setWeaving(weaving);
                if (shouldOptimize) {
                    tieup.get().optimize(this);
                }
            }
            if (threading.isDefined()) {
                threading.get().setWeaving(weaving);
                if (shouldOptimize) {
                    threading.get().optimize(this);
                }
            }
            if (treadling.isDefined()) {
                treadling.get().setWeaving(weaving);
                if (shouldOptimize) {
                    treadling.get().optimize(this);
                }
            }
            if (liftPlan.isDefined()) {
                liftPlan.get().setWeaving(weaving);
                if (shouldOptimize) {
                    liftPlan.get().optimize(this);
                }
            }
            if (weft.isDefined()) {
                weft.get().setWeaving(weaving);
                if (shouldOptimize) {
                    weft.get().optimize(this);
                }
            }
            if (warp.isDefined()) {
                warp.get().setWeaving(weaving);
                if (shouldOptimize) {
                    warp.get().optimize(this);
                }
            }
        }
        /*
            WARP
            -> THREADING
            -> WARP_THICKNESS
            -> WARP_THICKNESS_ZOOM
            -> WARP_SPACING
            -> WARP_SPACING_ZOOM
            -> WARP_COLORS
            -> WARP_SYMBOLS
         */
        if (warp.isDefined()) {
            if (shouldOptimize) {
                warp.get().optimize(this);
            }
            if (threading.isDefined()) {
                threading.get().setWarp(warp);
                if (shouldOptimize) {
                    threading.get().optimize(this);
                }
            }
            if (warpThickness.isDefined()) {
                warpThickness.get().setWarp(warp);
                if (shouldOptimize) {
                    warpThickness.get().optimize(this);
                }
            }
            if (warpThicknessZoom.isDefined()) {
                warpThicknessZoom.get().setWarp(warp);
                if (shouldOptimize) {
                    warpThicknessZoom.get().optimize(this);
                }
            }
            if (warpSpacing.isDefined()) {
                warpSpacing.get().setWarp(warp);
                if (shouldOptimize) {
                    warpSpacing.get().optimize(this);
                }
            }
            if (warpSpacingZoom.isDefined()) {
                warpSpacingZoom.get().setWarp(warp);
                if (shouldOptimize) {
                    warpSpacingZoom.get().optimize(this);
                }
            }
            if (warpColors.isDefined()) {
                warpColors.get().setWarp(warp);
                if (shouldOptimize) {
                    warpColors.get().optimize(this);
                }
            }
            if (warpSymbolTable.isDefined()) {
                warpSymbolTable.get().setWarp(warp);
                if (shouldOptimize) {
                    warpSymbolTable.get().optimize(this);
                }
            }
            if (warpSymbols.isDefined()) {
                warpSymbols.get().setWarp(warp);
                if (shouldOptimize) {
                    warpSymbols.get().optimize(this);
                }
            }
        }

        /*
         * WEFT
         *     -> WEFT_THICKNESS
         *     -> WEFT_THICKNESS_ZOOM
         *     -> WEFT_SPACING
         *     -> WEFT_SPACING_ZOOM
         *     -> WEFT_COLORS
         *     -> WARP_SYMBOLS
         *     -> TREADLING
         *     -> LIFTPLAN
         */

        if (weft.isDefined()) {
            if (shouldOptimize) {
                weft.get().optimize(this);
            }
            if (weftThickness.isDefined()) {
                weftThickness.get().setWeft(weft);
                if (shouldOptimize) {
                    weftThickness.get().optimize(this);
                }
            }
            if (weftThicknessZoom.isDefined()) {
                weftThicknessZoom.get().setWeft(weft);
                if (shouldOptimize) {
                    weftThicknessZoom.get().optimize(this);
                }
            }
            if (weftSpacing.isDefined()) {
                weftSpacing.get().setWeft(weft);
                if (shouldOptimize) {
                    weftSpacing.get().optimize(this);
                }
            }
            if (weftSpacingZoom.isDefined()) {
                weftSpacingZoom.get().setWeft(weft);
                if (shouldOptimize) {
                    weftSpacingZoom.get().optimize(this);
                }
            }
            if (weftColors.isDefined()) {
                weftColors.get().setWeft(weft);
                if (shouldOptimize) {
                    weftColors.get().optimize(this);
                }
            }
            if (weftSymbolTable.isDefined()) {
                weftSymbolTable.get().setWeft(weft);
                if (shouldOptimize) {
                    weftSymbolTable.get().optimize(this);
                }
            }
            if (weftSymbols.isDefined()) {
                weftSymbols.get().setWeft(weft);
                if (shouldOptimize) {
                    weftSymbols.get().optimize(this);
                }
            }
            if (treadling.isDefined()) {
                treadling.get().setWeft(weft);
                if (shouldOptimize) {
                    treadling.get().optimize(this);
                }
            }
            if (liftPlan.isDefined()) {
                liftPlan.get().setWeft(weft);
                if (shouldOptimize) {
                    liftPlan.get().optimize(this);
                }
            } else if (tieup.isDefined() && treadling.isDefined()) {
                liftPlan = makeLiftPlanFromTieUp(tieup.get(), treadling.get());
                if (shouldOptimize && liftPlan.isDefined()) {
                    liftPlan.get().optimize(this);
                }
            }
        }
    }

    private Option<WIFLiftPlan> makeLiftPlanFromTieUp(WIFTieup wifTieup, WIFTreadling wifTreadling) {
        Map<Integer, List<Integer>> liftPlanParts = new LinkedHashMap<>();
        for (int pick = 1; pick <= weft.get().getThreads(); pick++) {
            List<Integer> treadling = wifTreadling.getTreadling(pick);
            List<Integer> shaftsEngagedForPick = new ArrayList<>();
            for (Integer treadleEngaged : treadling) {
                List<Integer> shaftsForTreadle = wifTieup.getShaftsForTreadle(treadleEngaged);
                shaftsEngagedForPick.addAll(shaftsForTreadle);
            }
            if (!shaftsEngagedForPick.isEmpty()) {
                liftPlanParts.put(pick, shaftsEngagedForPick);
            }
        }
        if (!liftPlanParts.isEmpty()) {
            return new Option<>(new WIFLiftPlan(liftPlanParts));
        }
        return Option.NONE;
    }

    private void handleSection(Option<String> section, List<String> lines) {
        if (section.isDefined()) {
            orderRead.add(WIFContents.WIFContentsKey.fromString(section.get().toUpperCase().trim()).get());
            String sectionName = section.get().toUpperCase().trim();
            if (sectionName.equalsIgnoreCase("WIF")) {
                header = new WIFHeader(clone(lines));
            } else if (sectionName.equalsIgnoreCase("CONTENTS")) {
                contents = new WIFContents(clone(lines));
            } else {
                Option<WIFContents.WIFContentsKey> wifContentsKeyOption = WIFContents.WIFContentsKey.fromString(sectionName);
                if (wifContentsKeyOption.isDefined()) {
                    WIFContents.WIFContentsKey wifContentsKey = wifContentsKeyOption.get();
                    switch (wifContentsKey) {
                        case COLOR_PALETTE:
                            colorPalette = new Option<>(new WIFColorPalette(clone(lines)));
                            break;
                        case WARP_SYMBOL_PALETTE:
                            warpSymbolPalette = new Option<>(new WIFWarpSymbolPalette(clone(lines)));
                            break;
                        case WEFT_SYMBOL_PALETTE:
                            weftSymbolPalette = new Option<>(new WIFWeftSymbolPalette(clone(lines)));
                            break;
                        case TEXT:
                            text = new Option<>(new WIFText(clone(lines)));
                            break;
                        case WEAVING:
                            weaving = new Option<>(new WIFWeaving(clone(lines)));
                            break;
                        case WARP:
                            warp = new Option<>(new WIFWarp(clone(lines)));
                            break;
                        case WEFT:
                            weft = new Option<>(new WIFWeft(clone(lines)));
                            break;
                        case NOTES:
                            notes = new Option<>(new WIFNotes(clone(lines)));
                            break;
                        case TIEUP:
                            tieup = new Option<>(new WIFTieup(clone(lines)));
                            break;
                        case COLOR_TABLE:
                            colorTable = new Option<>(new WIFColorTable(clone(lines)));
                            break;
                        case WARP_SYMBOL_TABLE:
                            warpSymbolTable = new Option<>(new WIFWarpSymbolTable(clone(lines)));
                            break;
                        case WEFT_SYMBOL_TABLE:
                            weftSymbolTable = new Option<>(new WIFWeftSymbolTable(clone(lines)));
                            break;
                        case THREADING:
                            threading = new Option<>(new WIFThreading(clone(lines)));
                            break;
                        case WARP_THICKNESS:
                            warpThickness = new Option<>(new WIFWarpThickness(clone(lines)));
                            break;
                        case WARP_THICKNESS_ZOOM:
                            warpThicknessZoom = new Option<>(new WIFWarpThicknessZoom(clone(lines)));
                            break;
                        case WARP_SPACING:
                            warpSpacing = new Option<>(new WIFWarpSpacing(clone(lines)));
                            break;
                        case WARP_SPACING_ZOOM:
                            warpSpacingZoom = new Option<>(new WIFWarpSpacingZoom(clone(lines)));
                            break;
                        case WARP_COLORS:
                            warpColors = new Option<>(new WIFWarpColors(clone(lines)));
                            break;
                        case WARP_SYMBOLS:
                            warpSymbols = new Option<>(new WIFWarpSymbols(clone(lines)));
                            break;
                        case TREADLING:
                            treadling = new Option<>(new WIFTreadling(clone(lines)));
                            break;
                        case LIFTPLAN:
                            liftPlan = new Option<>(new WIFLiftPlan(clone(lines)));
                            break;
                        case WEFT_THICKNESS:
                            weftThickness = new Option<>(new WIFWeftThickness(clone(lines)));
                            break;
                        case WEFT_THICKNESS_ZOOM:
                            weftThicknessZoom = new Option<>(new WIFWeftThicknessZoom(clone(lines)));
                            break;
                        case WEFT_SPACING:
                            weftSpacing = new Option<>(new WIFWeftSpacing(clone(lines)));
                            break;
                        case WEFT_SPACING_ZOOM:
                            weftSpacingZoom = new Option<>(new WIFWeftSpacingZoom(clone(lines)));
                            break;
                        case WEFT_COLORS:
                            weftColors = new Option<>(new WIFWeftColors(clone(lines)));
                            break;
                        case WEFT_SYMBOLS:
                            weftSymbols = new Option<>(new WIFWeftSymbols(clone(lines)));
                            break;
                    }
                } else {
                    if (section.get().toUpperCase().trim().startsWith("PRIVATE")) {
                        String key = section.get().toUpperCase().trim();
                        String[] privateParts = key.split(" ", 3);
                        String sourceID = UUID.randomUUID().toString();
                        String sectionPart = UUID.randomUUID().toString();
                        if (privateParts.length > 1) {
                            sourceID = privateParts[1];
                        }
                        if (privateParts.length > 2) {
                            sectionPart = privateParts[2];
                        }
                        StringBuilder sb = new StringBuilder();
                        for (String line : lines) {
                            sb.append(line).append("\n");
                        }
                        privateData.put(new Pair<>(sourceID, sectionPart), sb.toString());
                    }
                }
            }
        }
    }

    private List<String> clone(List<String> lines) {
        return new ArrayList<>(lines);
    }

    /**
     * Closes the internal scanner
     */
    public void close() {
        scan.close();
    }

    /**
     * @return WIF [HEADER] section, required
     */
    public WIFHeader getHeader() {
        return header;
    }

    /**
     * @return WIF [WEFT SPACING ZOOM]
     */
    public Option<WIFWeftSpacingZoom> getWeftSpacingZoom() {
        return weftSpacingZoom;
    }

    /**
     * @return WIF [CONTENTS] section, required
     */
    public WIFContents getContents() {
        return contents;
    }

    /**
     * @return If defined, WIF [COLOR PALETTE] section
     */
    public Option<WIFColorPalette> getColorPalette() {
        return colorPalette;
    }

    /**
     * @return If defined, the WIF [WARP SYMBOL PALETTE] section
     */
    public Option<WIFWarpSymbolPalette> getWarpSymbolPalette() {
        return warpSymbolPalette;
    }

    /**
     * @return If defined, the WIF [WEFT SYMBOL PALETTE] section
     */
    public Option<WIFWeftSymbolPalette> getWeftSymbolPalette() {
        return weftSymbolPalette;
    }

    /**
     * @return If defined, the WIF [TEXT] section
     */
    public Option<WIFText> getText() {
        return text;
    }

    /**
     * @return If defined, the WIF [WEAVING] section
     */
    public Option<WIFWeaving> getWeaving() {
        return weaving;
    }

    /**
     * @return If defined, the WIF [WARP] section
     */
    public Option<WIFWarp> getWarp() {
        return warp;
    }

    /**
     * @return If defined, the WIF [WEFT] section
     */
    public Option<WIFWeft> getWeft() {
        return weft;
    }

    /**
     * @return If defined, the WIF [NOTES] section
     */
    public Option<WIFNotes> getNotes() {
        return notes;
    }

    /**
     * @return If defined, the WIF [TIEUP] section
     */
    public Option<WIFTieup> getTieup() {
        return tieup;
    }

    /**
     * @return If defined, the WIF [COLOR TABLE] section
     */
    public Option<WIFColorTable> getColorTable() {
        return colorTable;
    }

    /**
     * @return If defined, the WIF [WARP SYMBOL TABLE] section
     */
    public Option<WIFWarpSymbolTable> getWarpSymbolTable() {
        return warpSymbolTable;
    }

    /**
     * @return If defined, the WIF [WEFT SYMBOL TABLE] section
     */
    public Option<WIFWeftSymbolTable> getWeftSymbolTable() {
        return weftSymbolTable;
    }

    /**
     * @return If defined, the WIF [THREADING] section
     */
    public Option<WIFThreading> getThreading() {
        return threading;
    }

    /**
     * @return If defined, the WIF [WARP THICKNESS] section
     */
    public Option<WIFWarpThickness> getWarpThickness() {
        return warpThickness;
    }

    /**
     * @return If defined, the WIF [WARP THICKNESS ZOOM] section
     */
    public Option<WIFWarpThicknessZoom> getWarpThicknessZoom() {
        return warpThicknessZoom;
    }

    /**
     * @return If defined, the WIF [WARP SPACING] section
     */
    public Option<WIFWarpSpacing> getWarpSpacing() {
        return warpSpacing;
    }


    /**
     * @return If defined, the WIF [WARP SPACING ZOOM] section
     */
    public Option<WIFWarpSpacingZoom> getWarpSpacingZoom() {
        return warpSpacingZoom;
    }

    /**
     * @return If defined, the WIF [WARP COLORS] section
     */
    public Option<WIFWarpColors> getWarpColors() {
        return warpColors;
    }

    /**
     * @return If defined, the WIF [WARP SYMBOLS] section
     */
    public Option<WIFWarpSymbols> getWarpSymbols() {
        return warpSymbols;
    }

    /**
     * @return If defined, the WIF [TREADLING] section
     */
    public Option<WIFTreadling> getTreadling() {
        return treadling;
    }

    /**
     * @return If defined, the WIF [LIFTPLAN] section
     */
    public Option<WIFLiftPlan> getLiftPlan() {
        return liftPlan;
    }

    /**
     * @return If defined, the WIF [WEFT THICKNESS] section
     */
    public Option<WIFWeftThickness> getWeftThickness() {
        return weftThickness;
    }

    /**
     * @return If defined, the WIF [WEFT THICKNESS ZOOM] section
     */
    public Option<WIFWeftThicknessZoom> getWeftThicknessZoom() {
        return weftThicknessZoom;
    }

    /**
     * @return If defined, the WIF [WEFT SPACING] section
     */
    public Option<WIFWeftSpacing> getWeftSpacing() {
        return weftSpacing;
    }

    /**
     * @return If defined, the WIF [WEFT COLORS] section
     */
    public Option<WIFWeftColors> getWeftColors() {
        return weftColors;
    }


    /**
     * @return If defined, the WIF [WEFT SYMBOLS] section
     */
    public Option<WIFWeftSymbols> getWeftSymbols() {
        return weftSymbols;
    }


    /**
     * @return If defined, the WIF [PRIVATE (source id) (section)] section
     */
    public Map<Pair<String, String>, String> getPrivateData() {
        return privateData;
    }

    private void appendContents(StringBuilder sb, Option<? extends AbstractWIFObject> obj) {
        if (obj.isDefined()) {
            sb.append(obj.get().toWIFSection()).append(System.lineSeparator()).append(System.lineSeparator());
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(header.toWIFSection()).append(System.lineSeparator()).append(System.lineSeparator());
        sb.append(contents.toWIFSection()).append(System.lineSeparator()).append(System.lineSeparator());
        //preserve order
        for (WIFContents.WIFContentsKey k : orderRead) {
            if (k != null) {
                switch (k) {
                    case COLOR_PALETTE:
                        appendContents(sb, colorPalette);
                        break;
                    case WARP_SYMBOL_PALETTE:
                        appendContents(sb, warpSymbolPalette);
                        break;
                    case WEFT_SYMBOL_PALETTE:
                        appendContents(sb, weftSymbolPalette);
                        break;
                    case TEXT:
                        appendContents(sb, text);
                        break;
                    case WEAVING:
                        appendContents(sb, weaving);
                        break;
                    case WARP:
                        appendContents(sb, warp);
                        break;
                    case WEFT:
                        appendContents(sb, weft);
                        break;
                    case NOTES:
                        appendContents(sb, notes);
                        break;
                    case TIEUP:
                        appendContents(sb, tieup);
                        break;
                    case COLOR_TABLE:
                        appendContents(sb, colorTable);
                        break;
                    case WARP_SYMBOL_TABLE:
                        appendContents(sb, warpSymbolTable);
                        break;
                    case THREADING:
                        appendContents(sb, threading);
                        break;
                    case WARP_THICKNESS:
                        appendContents(sb, warpThickness);
                        break;
                    case WARP_SPACING_ZOOM:
                        appendContents(sb, warpSpacingZoom);
                        break;
                    case WARP_SPACING:
                        appendContents(sb, warpSpacing);
                        break;
                    case WARP_COLORS:
                        appendContents(sb, warpColors);
                        break;
                    case WARP_SYMBOLS:
                        appendContents(sb, warpSymbols);
                        break;
                    case TREADLING:
                        appendContents(sb, treadling);
                        break;
                    case LIFTPLAN:
                        appendContents(sb, liftPlan);
                        break;
                    case WEFT_THICKNESS:
                        appendContents(sb, weftThickness);
                        break;
                    case WEFT_THICKNESS_ZOOM:
                        appendContents(sb, weftThicknessZoom);
                        break;
                    case WEFT_SPACING:
                        appendContents(sb, weftSpacing);
                        break;
                    case WEFT_COLORS:
                        appendContents(sb, weftColors);
                        break;
                    case WEFT_SYMBOLS:
                        appendContents(sb, weftSymbols);
                        break;
                }
            }
        }
        for (Map.Entry<Pair<String, String>, String> ent : privateData.entrySet()) {
            sb.append("[PRIVATE ").append(ent.getKey().getFirst()).append(" ").append(ent.getKey().getSecond()).append("]").append(System.lineSeparator())
                    .append(ent.getValue()).append(System.lineSeparator());
        }
        sb.trimToSize();
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFReader wifReader = (WIFReader) o;
        return
                Objects.equals(header, wifReader.header) &&
                        Objects.equals(contents, wifReader.contents) &&
                        Objects.equals(colorPalette, wifReader.colorPalette) &&
                        Objects.equals(warpSymbolPalette, wifReader.warpSymbolPalette) &&
                        Objects.equals(weftSymbolPalette, wifReader.weftSymbolPalette) &&
                        Objects.equals(text, wifReader.text) &&
                        Objects.equals(weaving, wifReader.weaving) &&
                        Objects.equals(warp, wifReader.warp) &&
                        Objects.equals(weft, wifReader.weft) &&
                        Objects.equals(notes, wifReader.notes) &&
                        Objects.equals(tieup, wifReader.tieup) &&
                        Objects.equals(colorTable, wifReader.colorTable) &&
                        Objects.equals(warpSymbolTable, wifReader.warpSymbolTable) &&
                        Objects.equals(weftSymbolTable, wifReader.weftSymbolTable) &&
                        Objects.equals(threading, wifReader.threading) &&
                        Objects.equals(warpThickness, wifReader.warpThickness) &&
                        Objects.equals(warpThicknessZoom, wifReader.warpThicknessZoom) &&
                        Objects.equals(warpSpacing, wifReader.warpSpacing) &&
                        Objects.equals(warpSpacingZoom, wifReader.warpSpacingZoom) &&
                        Objects.equals(warpColors, wifReader.warpColors) &&
                        Objects.equals(warpSymbols, wifReader.warpSymbols) &&
                        Objects.equals(treadling, wifReader.treadling) &&
                        Objects.equals(liftPlan, wifReader.liftPlan) &&
                        Objects.equals(weftThickness, wifReader.weftThickness) &&
                        Objects.equals(weftThicknessZoom, wifReader.weftThicknessZoom) &&
                        Objects.equals(weftSpacing, wifReader.weftSpacing) &&
                        Objects.equals(weftColors, wifReader.weftColors) &&
                        Objects.equals(weftSymbols, wifReader.weftSymbols) &&
                        Objects.equals(privateData, wifReader.privateData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, contents, colorPalette, warpSymbolPalette, weftSymbolPalette, text, weaving, warp, weft, notes, tieup, colorTable, warpSymbolTable, weftSymbolTable, threading, warpThickness, warpThicknessZoom, warpSpacing, warpSpacingZoom, warpColors, warpSymbols, treadling, liftPlan, weftThickness, weftThicknessZoom, weftSpacing, weftColors, weftSymbols, privateData);
    }
}
