/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.abst;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Many WIF Objects are just tables that map an integer to a thing
 *
 * @param <T> Type of WIF Table
 */
public abstract class AbstractWIFTable<T> extends AbstractWIFObject {
    protected final Map<Integer, T> table = new TreeMap<>();
    private final T defaultValue;


    protected AbstractWIFTable(List<String> lines, T initialDefaultValue) {
        this.defaultValue = initialDefaultValue;
        parse(lines);
    }

    protected AbstractWIFTable(Map<Integer, T> table, T initDefault) {
        this.defaultValue = initDefault;
        this.table.putAll(table);
    }

    private void parse(List<String> lines) {
        table.clear();
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1];
                    int index = parseIntOrDefault(key, -1);
                    if (index > 0) {
                        table.put(index, parseOrDefault(value));
                    }
                }
            }
        }
    }

    public T getDefaultValue() {
        return defaultValue;
    }


    public abstract T parseOrDefault(String value);


    public Map<Integer, T> getTable() {
        return table;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractWIFTable<?> that = (AbstractWIFTable<?>) o;
        return Objects.equals(table, that.table) &&
                Objects.equals(defaultValue, that.defaultValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(table, defaultValue);
    }

}
