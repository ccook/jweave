/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFObject;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Required
 * <p>
 * An informational header found in every WIF file
 * <p>
 * e.g
 * <p>
 * [WIF]
 * Version=1.1
 * Date=April 20, 1997
 * Developers=wif@mhsoft.com
 * Source Program=SwiftWeave
 * Source Version=5.14
 * <p>
 * This object supports META-BEGIN, META-END header values found in
 * ArahWeave-made WIF files. These values are in getMetadata()
 * <p>
 * e.g
 * <p>
 * ...
 * ;META-BEGIN
 * ;Title=16 zu 16, 010
 * ;Source=Old German Pattern Book, Untitled and Unbound
 * ;Place=Germany
 * ;Date=Unknown
 * ;Categories=
 * ;WarpThreads=16
 * ;WeftThreads=16
 * ;WarpRepeats=3
 * ;WeftRepeats=3
 * ;META-END
 * ....
 */
public class WIFHeader extends AbstractWIFObject {
    private static final Logger log = LoggerFactory.getLogger(WIFHeader.class);

    private String version = "1.1";
    private String date = DateTime.now().toString();
    private String developers = System.getProperty("user.name");
    private String sourceProgram = "jweave";
    private Option<String> sourceVersion = new Option<>(WIFHeader.class.getPackage().getSpecificationVersion());
    private final Map<String, String> metadata = new TreeMap<>();

    public WIFHeader(List<String> lines) {
        for (String line : lines) {
            if (!line.trim().isEmpty()) {
                String[] parts = line.split("=", 2);
                if (parts.length >= 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim();
                    switch (key.toLowerCase().replace(" ", "")) {
                        case "version":
                            this.version = value;
                            break;
                        case "date":
                            this.date = value;
                            break;
                        case "developers":
                            this.developers = value;
                            break;
                        case "sourceprogram":
                            this.sourceProgram = value;
                            break;
                        case "sourceversion":
                            this.sourceVersion = new Option<>(value);
                            break;
                        default:
                            metadata.put(key.replace(";", ""), value);
                            break;
                    }
                }
            }
        }
    }


    public WIFHeader(Option<String> wifVersion,
                     Option<String> date,
                     List<String> developers,
                     Option<String> sourceProgram,
                     Option<String> sourceVersion,
                     Map<String, String> extraMetadata) {
        this.version = wifVersion.isDefined() ? wifVersion.get() : this.version;
        this.date = date.isDefined() ? date.get() : this.date;
        if (!developers.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (String dev : developers) {
                sb.append(dev).append(",");
            }
            this.developers = sb.substring(0, sb.length() - 1);
        }
        this.sourceProgram = sourceProgram.isDefined() ? sourceProgram.get() : this.sourceProgram;
        this.sourceVersion = sourceVersion.isDefined() ? sourceVersion : this.sourceVersion;
        for (Map.Entry<String, String> meta : extraMetadata.entrySet()) {
            this.metadata.put(meta.getKey(), meta.getValue());
        }
    }

    /**
     * @return WIF Version: Constant 1.1
     */
    public String getVersion() {
        return version;
    }

    /**
     * The Date field has no standard way to defining dates (so be careful parsing)
     * <p>
     * e.g Date=April 20, 1997 (most common) OR Date=2/12/2004 OR Date=Monday, April 17, 2000  9:06 am
     *
     * @return WIF Date
     */
    public String getDate() {
        return date;
    }

    /**
     * @return WIF Developers
     */
    public String getDevelopers() {
        return developers;
    }

    /**
     * Program which made the WIF
     * e.g
     * Source Program=Fiberworks PCW
     * Source Program=Handweaving.net Pattern Maker
     * Source Program=SwiftWeave
     *
     * @return WIF Source Program
     */
    public String getSourceProgram() {
        return sourceProgram;
    }

    /**
     * Version of the Program that made the WIF
     *
     * @return WIF Source Program's version
     */
    public Option<String> getSourceVersion() {
        return sourceVersion;
    }

    /**
     * Any non-standard metadata found in the header
     *
     * @return non-standard metadata found in the header
     */
    public Map<String, String> getMetadata() {
        return metadata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WIFHeader wifHeader = (WIFHeader) o;
        return Objects.equals(version, wifHeader.version) &&
                Objects.equals(date, wifHeader.date) &&
                Objects.equals(developers, wifHeader.developers) &&
                Objects.equals(sourceProgram, wifHeader.sourceProgram) &&
                Objects.equals(sourceVersion, wifHeader.sourceVersion) &&
                Objects.equals(metadata, wifHeader.metadata);
    }

    @Override
    public int hashCode() {
        return Objects.hash(version, date, developers, sourceProgram, sourceVersion, metadata);
    }

    @Override
    public void optimize(WIFReader r) {
        //noop
    }

    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append("WIF").append("]").append("\n");
        addKeyValue(sb, "Version", version);
        addKeyValue(sb, "Date", date);
        addKeyValue(sb, "Developers", developers);
        addKeyValue(sb, "Source Program", sourceProgram);
        if (sourceVersion.isDefined()) addKeyValue(sb, "Source Version", sourceVersion);
        Set<Map.Entry<String, String>> entries = metadata.entrySet();
        if (!entries.isEmpty()) {
            sb.append(";META-BEGIN").append("\n");
            for (Map.Entry<String, String> meta : entries) {
                addKeyValue(sb, ";" + meta.getKey(), meta.getValue());
            }
            sb.append(";META-END").append("\n");
        }
        return sb.toString().trim();
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return null;
    }


    public static class Builder {
        private Option<String> wifVersion = Option.NONE;
        private Option<String> date = Option.NONE;
        private final List<String> developers = new ArrayList<>();
        private Option<String> sourceProgram = Option.NONE;
        private Option<String> sourceVersion = Option.NONE;
        private final Map<String, String> extraMetadata = new HashMap<>();

        public Builder version(String wifVersion) {
            this.wifVersion = new Option<>(wifVersion);
            return this;
        }

        public Builder date(String date) {
            this.date = new Option<>(date);
            return this;
        }

        public Builder date(DateTime date) {
            this.date = date != null ? new Option<>(date.toString()) : Option.NONE;
            return this;
        }

        public Builder developer(String developer) {
            if (developer != null) {
                this.developers.add(developer);
            }
            return this;
        }

        public Builder developers(Collection<String> developers) {
            for (String dev : developers) {
                developer(dev);
            }
            return this;
        }

        public Builder developers(String... developers) {
            for (String dev : developers) {
                developer(dev);
            }
            return this;
        }

        public Builder sourceProgram(String srcPrgm) {
            this.sourceProgram = new Option<>(srcPrgm);
            return this;
        }

        public Builder sourceVersion(String version) {
            this.sourceVersion = new Option<>(version);
            return this;
        }

        public Builder extraMetadata(String key, String value) {
            this.extraMetadata.put(key, value);
            return this;
        }

        public WIFHeader build() {
            return new WIFHeader(wifVersion, date, developers, sourceProgram, sourceVersion, extraMetadata);
        }

    }
}
