/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.wif.model.warp;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFContents;
import com.gitlab.ccook.jweave.wif.model.abst.AbstractWIFTable;

import java.util.*;

/**
 * Mapping between the warp number and the symbol table index
 */
public class WIFWarpSymbols extends AbstractWIFTable<Integer> {
    private Option<WIFWarpSymbolTable> symbolTable = Option.NONE;

    public WIFWarpSymbols(List<String> lines) {
        super(lines, 0);
    }

    public WIFWarpSymbols(Map<Integer, Integer> table) {
        super(table, 0);
    }

    @Override
    public Integer parseOrDefault(String value) {
        return parseIntOrDefault(value, getDefaultValue());
    }

    public Option<Integer> getSymbolTableIndex(int warp) {
        Option<Integer> symbolIndex = new Option<>(table.get(warp));
        if (symbolIndex.isDefined()) {
            return symbolIndex;
        }
        if (getWarp().isDefined()) {
            return getWarp().get().getSymbolNumber();
        }
        return Option.NONE;
    }

    public Option<String> getSymbol(int warp) {
        if (symbolTable.isDefined() && getSymbolTableIndex(warp).isDefined()) {
            if (getSymbolTableIndex(warp).isDefined()) {
                return symbolTable.get().getSymbolForIndex(getSymbolTableIndex(warp).get());
            }
            if (getWarp().isDefined()) {
                Option<String> defaultSymbol = getWarp().get().getSymbol();
                if (defaultSymbol.isDefined()) {
                    return defaultSymbol;
                }
            }
        }

        return Option.NONE;
    }

    public void setSymbolTable(WIFWarpSymbolTable symbolTable) {
        this.symbolTable = new Option<>(symbolTable);
    }

    public Option<WIFWarpSymbolTable> getSymbolTable() {
        return symbolTable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WIFWarpSymbols that = (WIFWarpSymbols) o;
        return Objects.equals(symbolTable, that.symbolTable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), symbolTable);
    }

    /**
     * Removes unused threads
     *
     * @param r reader to optimize from
     */
    @Override
    public void optimize(WIFReader r) {
        if (r.getWarp().isDefined()) {
            setWarp(r.getWarp());
            int threads = r.getWarp().get().getThreads();
            List<Integer> toRemove = new ArrayList<>();
            for (Map.Entry<Integer, Integer> entry : table.entrySet()) {
                if (entry.getKey() > threads) {
                    toRemove.add(entry.getKey());
                }
            }
            for (Integer i : toRemove) {
                table.remove(i);
            }
        }
        if (r.getWarpSymbolTable().isDefined()) {
            this.symbolTable = r.getWarpSymbolTable();
        }
    }


    @Override
    public String toWIFSection() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(getWIFSectionName()).append("]").append("\n");
        for (Map.Entry<Integer, Integer> ent : table.entrySet()) {
            addKeyValue(sb, ent.getKey(), ent.getValue());
        }

        return sb.toString().trim();
    }


    @Override
    public WIFContents.WIFContentsKey getWIFSectionName() {
        return WIFContents.WIFContentsKey.WARP_SYMBOLS;
    }

    public class Builder {
        private Map<Integer, Integer> table = new TreeMap<>();

        public Builder(int warp, int symbolIndex) {
            table.put(warp, symbolIndex);
        }

        public WIFWarpSymbols build() {
            return new WIFWarpSymbols(table);
        }
    }
}

