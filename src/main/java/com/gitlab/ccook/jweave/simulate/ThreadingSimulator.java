/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate;

import com.gitlab.ccook.jweave.simulate.image.ThreadingImage;
import com.gitlab.ccook.jweave.wif.WIFReader;

import java.awt.image.BufferedImage;
import java.util.Objects;

public class ThreadingSimulator {
    private SimulatedWarp warp;
    private WIFReader r;

    public ThreadingSimulator(WIFReader r) {
        this.r = r;
        this.warp = new SimulatedWarp(r);
    }

    public boolean isThreaded(int shaft, int warpThread) {
        return warp.getThreadedShaftsForWarp(warpThread).contains(shaft);
    }

    public SimulatedWarp getWarp() {
        return warp;
    }

    public int getTotalShafts() {
        return warp.getTotalShafts().isDefined() ? warp.getTotalShafts().get() : 1;
    }

    public BufferedImage makeImage(int width, int height) {
        ThreadingImage im = new ThreadingImage(this);
        return im.getThreadingImage(width, height);
    }

    public void optimize(WIFReader r) {
        warp.optimize(r);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThreadingSimulator that = (ThreadingSimulator) o;
        return Objects.equals(warp, that.warp) &&
                Objects.equals(r, that.r);
    }

    @Override
    public int hashCode() {
        return Objects.hash(warp, r);
    }
}
