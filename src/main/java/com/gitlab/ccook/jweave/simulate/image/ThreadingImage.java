/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate.image;

import com.gitlab.ccook.jweave.simulate.ThreadingSimulator;
import com.gitlab.ccook.jweave.simulate.helper.SimulatorImageUtils;
import com.gitlab.ccook.jweave.util.Option;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Objects;

public class ThreadingImage {
    private ThreadingSimulator ts;
    private BufferedImage bufimage;

    public ThreadingImage(ThreadingSimulator ts) {
        this.ts = ts;
    }


    public BufferedImage getThreadingImage(int width, int height) {
        bufimage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bufimage.createGraphics();

        drawGrid(g2d, 0, 0, width, height);
        g2d.dispose();
        bufimage = SimulatorImageUtils.flipH(bufimage);
        bufimage = SimulatorImageUtils.flipV(bufimage);
        return bufimage;
    }

    private void drawGrid(Graphics2D g2d, int startX, int startY, int width, int height) {
        int shafts = ts.getTotalShafts();
        int threads = ts.getWarp().getThreadCount();
        int sectionWidth = (int) Math.ceil((double) width / threads);
        int sectionHeight = (int) Math.ceil((double) height / shafts);

        for (int x = 0; x < threads; x++) {
            Option<Color> colorOption = ts.getWarp().getColor(x + 1);
            for (int y = 0; y < shafts; y++) {
                boolean threaded = ts.isThreaded(y + 1, x + 1);
                Color color = Color.black;
                if (colorOption.isDefined()) {
                    color = colorOption.get();
                }
                if (threaded) {
                    g2d.setColor(color);
                } else {
                    g2d.setColor(Color.LIGHT_GRAY);
                }
                g2d.fillRect((startX + (x * sectionWidth)),
                        (startY + (y * sectionHeight)),
                        sectionWidth, sectionHeight);
                SimulatorImageUtils.addBorder(g2d,
                        (startX + (x * sectionWidth)),
                        (startY + (y * sectionHeight)),
                        sectionWidth, sectionHeight,
                        1, Color.BLACK);
            }
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThreadingImage that = (ThreadingImage) o;
        return Objects.equals(ts, that.ts) &&
                Objects.equals(bufimage, that.bufimage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ts, bufimage);
    }
}
