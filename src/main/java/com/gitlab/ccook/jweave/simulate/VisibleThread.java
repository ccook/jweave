/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate;

import com.gitlab.ccook.jweave.simulate.abst.AbstractWarpOrWeft;

import java.util.Objects;

public class VisibleThread {
    private int threadNum;
    private AbstractWarpOrWeft thread;
    private VisibleThreadType typ;

    public VisibleThread(int threadNum, AbstractWarpOrWeft thread) {
        this.threadNum = threadNum;
        this.thread = thread;
        typ = thread.getClass().equals(SimulatedWarp.class) ? VisibleThreadType.WARP : VisibleThreadType.WEFT;
    }

    public int getThreadNum() {
        return threadNum;
    }

    public AbstractWarpOrWeft getThread() {
        return thread;
    }

    public VisibleThreadType getType() {
        return typ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisibleThread that = (VisibleThread) o;
        return threadNum == that.threadNum &&
                Objects.equals(thread, that.thread) &&
                typ == that.typ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(threadNum, thread, typ);
    }
}
