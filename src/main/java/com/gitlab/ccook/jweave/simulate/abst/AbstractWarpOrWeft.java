/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate.abst;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;

import java.awt.*;

public abstract class AbstractWarpOrWeft {

    protected AbstractWarpOrWeft(WIFReader r) {
    }

    public abstract Option<Color> getColor(int threadNumb);
}
