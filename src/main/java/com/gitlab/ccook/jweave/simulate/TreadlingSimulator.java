/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate;

import com.gitlab.ccook.jweave.simulate.image.TreadlingImage;
import com.gitlab.ccook.jweave.wif.WIFReader;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Objects;

public class TreadlingSimulator {

    private SimulatedWeft weft;
    private WIFReader r;

    public TreadlingSimulator(WIFReader r) {
        this.r = r;
        this.weft = new SimulatedWeft(r);
    }

    public List<Integer> getTreadlingForPick(int pick) {
        return weft.getTreadlingForPick(pick);
    }

    public int getTotalTredles() {
        return weft.getTotalTreadles();
    }

    public int getPickCount() {
        return weft.getPickCount();
    }

    public SimulatedWeft getWeft() {
        return weft;
    }

    public BufferedImage makeImage(int width, int height) {
        return new TreadlingImage(this).getTreadlingImage(width, height);
    }

    public void optimize(WIFReader r) {
        weft.optimize(r);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreadlingSimulator that = (TreadlingSimulator) o;
        return Objects.equals(weft, that.weft) &&
                Objects.equals(r, that.r);
    }

    @Override
    public int hashCode() {
        return Objects.hash(weft, r);
    }
}
