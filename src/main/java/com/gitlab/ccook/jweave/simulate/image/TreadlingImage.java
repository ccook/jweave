/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate.image;

import com.gitlab.ccook.jweave.simulate.TreadlingSimulator;
import com.gitlab.ccook.jweave.simulate.helper.SimulatorImageUtils;
import com.gitlab.ccook.jweave.util.Option;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Objects;

public class TreadlingImage {
    private BufferedImage bufimage;
    private TreadlingSimulator ts;

    public TreadlingImage(TreadlingSimulator ts) {
        this.ts = ts;
    }


    public BufferedImage getTreadlingImage(int width, int height) {
        bufimage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bufimage.createGraphics();

        drawGrid(g2d, 0, 0, width, height);
        g2d.dispose();
        return bufimage;
    }

    private void drawGrid(Graphics2D g2d, int startX, int startY, int width, int height) {
        int treadles = ts.getTotalTredles();
        int picks = ts.getPickCount();
        int sectionWidth = (int) Math.ceil((double) width / treadles);
        int sectionHeight = (int) Math.ceil((double) height / picks);
        for (int x = 0; x < treadles; x++) {
            for (int y = 0; y < picks; y++) {
                List<Integer> treadlingForPick = ts.getTreadlingForPick(y + 1);
                Color color = Color.black;
                Option<Color> colorOption = ts.getWeft().getColor(y + 1);
                if (colorOption.isDefined()) {
                    color = colorOption.get();
                }
                if (treadlingForPick.contains(x + 1)) {
                    g2d.setColor(color);
                } else {
                    g2d.setColor(Color.LIGHT_GRAY);
                }
                g2d.fillRect((startX + (x * sectionWidth)),
                        (startY + (y * sectionHeight)),
                        sectionWidth, sectionHeight);
                SimulatorImageUtils.addBorder(g2d,
                        (startX + (x * sectionWidth)),
                        (startY + (y * sectionHeight)),
                        sectionWidth, sectionHeight,
                        1, Color.BLACK);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreadlingImage that = (TreadlingImage) o;
        return Objects.equals(bufimage, that.bufimage) &&
                Objects.equals(ts, that.ts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bufimage, ts);
    }
}
