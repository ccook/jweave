/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate;

import com.gitlab.ccook.jweave.simulate.abst.AbstractWarpOrWeft;
import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFColorTable;
import com.gitlab.ccook.jweave.wif.model.helper.WIFUnit;
import com.gitlab.ccook.jweave.wif.model.warp.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SuppressWarnings("unchecked")
public class SimulatedWarp extends AbstractWarpOrWeft {
    private Option<WIFThreading> threading;
    private Option<WIFWarpColors> colors;
    private Option<WIFColorTable> colorTable;
    private Option<WIFWarp> warp;
    private Option<WIFWarpSpacing> spacing;
    private Option<WIFWarpSpacingZoom> spacingZoom;
    private Option<WIFWarpThickness> thickness;
    private Option<WIFWarpThicknessZoom> thicknessZoom;
    private Option<WIFWarpSymbols> symbols;
    private Option<Integer> totalShafts = Option.NONE;

    public SimulatedWarp(WIFReader r) {
        super(r);
        setValues(r);
    }

    private void setValues(WIFReader r) {
        this.threading = r.getThreading();
        this.colors = r.getWarpColors();
        this.warp = r.getWarp();
        this.colorTable = r.getColorTable();
        this.spacing = r.getWarpSpacing();
        this.spacingZoom = r.getWarpSpacingZoom();
        this.thickness = r.getWarpThickness();
        this.thicknessZoom = r.getWarpThicknessZoom();
        this.symbols = r.getWarpSymbols();
        if (r.getWeaving().isDefined()) {
            totalShafts = new Option<>(r.getWeaving().get().getShafts());
        } else if (r.getThreading().isDefined()) {
            totalShafts = new Option<>(threading.get().getThreading().keySet().size());
        }
    }

    public List<Integer> getThreadedShaftsForWarp(int warpThread) {
        List<Integer> threadedShafts = new ArrayList<>();
        if (threading.isDefined()) {
            threadedShafts = threading.get().getShaftsForWarp(warpThread);
        }

        return threadedShafts;
    }

    public Option<Integer> getTotalShafts() {
        return totalShafts;
    }

    @Override
    public Option<Color> getColor(int warpThread) {
        if (colors.isDefined()) {
            return colors.get().getWarpColor(warpThread);
        }
        if (warp.isDefined() && colorTable.isDefined()) {
            return colorTable.get().lookupColor(warp.get().getColorPaletteIndex().get());
        }
        return Option.NONE;
    }

    public Pair<Double, WIFUnit> getTotalSpacing(int warpThread) {
        int spacingZ = 1;
        double space = 0;
        WIFUnit unit = WIFUnit.UNKNOWN;
        if (spacingZoom.isDefined() && spacingZoom.get().getZoom(warpThread).isDefined()) {
            spacingZ = spacingZoom.get().getZoom(warpThread).get();

        } else if (warp.isDefined() && warp.get().getSpacingZoom().isDefined()) {
            spacingZ = warp.get().getSpacingZoom().get();
        }

        if (spacing.isDefined() && spacing.get().getSpacing(warpThread).isDefined()) {
            space = spacing.get().getSpacing(warpThread).get().getFirst();
            unit = spacing.get().getSpacing(warpThread).get().getSecond();

        } else if (warp.isDefined() && warp.get().getSpacing().isDefined()) {
            space = warp.get().getSpacing().get();
        }

        if (unit == WIFUnit.UNKNOWN && warp.isDefined() && warp.get().getUnits().isDefined()) {
            unit = warp.get().getUnits().get();
        }

        return new Pair<>(space * spacingZ, unit);
    }


    public Pair<Double, WIFUnit> getTotalThickness(int warpThread) {
        int thickZ = 1;
        double thick = 1;
        WIFUnit unit = WIFUnit.UNKNOWN;
        if (thicknessZoom.isDefined() && thicknessZoom.get().getZoom(warpThread).isDefined()) {
            thickZ = thicknessZoom.get().getZoom(warpThread).get();
        } else if (warp.isDefined() && warp.get().getThicknessZoom().isDefined()) {
            thickZ = warp.get().getThicknessZoom().get();
        }

        if (thickness.isDefined() && thickness.get().getThickness(warpThread).isDefined()) {
            thick = thickness.get().getThickness(warpThread).get().getFirst();
            unit = thickness.get().getThickness(warpThread).get().getSecond();

        } else if (warp.isDefined() && warp.get().getThickness().isDefined()) {
            thick = warp.get().getThickness().get();
        }

        if (unit == WIFUnit.UNKNOWN && warp.isDefined() && warp.get().getUnits().isDefined()) {
            unit = warp.get().getUnits().get();
        }

        return new Pair<>(thick * thickZ, unit);
    }

    public Option<String> getSymbol(int warpThread) {
        Option<String> sym = Option.NONE;
        if (symbols.isDefined()) {
            if (symbols.get().getSymbol(warpThread).isDefined()) {
                return symbols.get().getSymbol(warpThread);
            }
        }
        if (warp.isDefined()) {
            return warp.get().getSymbol();
        }
        return sym;
    }

    public int getThreadCount() {

        //TODO consider other ways
        if (warp.isDefined()) {
            return warp.get().getThreads();
        }
        return 1;
    }

    public void optimize(WIFReader r) {
        warp.get().optimize(r);
        setValues(r);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimulatedWarp that = (SimulatedWarp) o;
        return Objects.equals(threading, that.threading) &&
                Objects.equals(colors, that.colors) &&
                Objects.equals(colorTable, that.colorTable) &&
                Objects.equals(warp, that.warp) &&
                Objects.equals(spacing, that.spacing) &&
                Objects.equals(spacingZoom, that.spacingZoom) &&
                Objects.equals(thickness, that.thickness) &&
                Objects.equals(thicknessZoom, that.thicknessZoom) &&
                Objects.equals(symbols, that.symbols) &&
                Objects.equals(totalShafts, that.totalShafts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(threading, colors, colorTable, warp, spacing, spacingZoom, thickness, thicknessZoom, symbols, totalShafts);
    }
}
