/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate;

import com.gitlab.ccook.jweave.wif.WIFReader;

import java.util.Objects;

public class LoomSimulator {
    private WIFReader r;
    private DrawDownSimulator dd;
    private ThreadingSimulator ts;
    private TreadlingSimulator tts;
    private TieUpSimulator tus;

    public LoomSimulator(WIFReader r) {
        this.r = r;
        this.dd = new DrawDownSimulator(r);
        this.ts = new ThreadingSimulator(r);
        this.tts = new TreadlingSimulator(r);
        this.tus = new TieUpSimulator(r);
    }

    public DrawDownSimulator getDrawDown() {
        return dd;
    }

    public ThreadingSimulator getThreading() {
        return ts;
    }

    public TreadlingSimulator getTreadling() {
        return tts;
    }

    public TieUpSimulator getTieUp() {
        return tus;
    }

    public void optimize(WIFReader r) {
        r.optimize();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoomSimulator that = (LoomSimulator) o;
        return Objects.equals(r, that.r) &&
                Objects.equals(dd, that.dd) &&
                Objects.equals(ts, that.ts) &&
                Objects.equals(tts, that.tts) &&
                Objects.equals(tus, that.tus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(r, dd, ts, tts, tus);
    }
}
