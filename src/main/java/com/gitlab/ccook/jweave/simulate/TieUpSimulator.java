/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate;

import com.gitlab.ccook.jweave.simulate.image.TieUpImage;
import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFTieup;
import com.gitlab.ccook.jweave.wif.model.WIFWeaving;
import com.gitlab.ccook.jweave.wif.model.weft.WIFLiftPlan;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Objects;

public class TieUpSimulator {
    private WIFReader r;
    private Option<WIFTieup> tieup;
    private Option<WIFLiftPlan> liftPlan;
    private Option<WIFWeaving> weaving;

    public TieUpSimulator(WIFReader r) {
        this.r = r;
        setValues(r);
    }

    private void setValues(WIFReader r) {
        this.tieup = r.getTieup();
        this.liftPlan = r.getLiftPlan();
        this.weaving = r.getWeaving();
    }

    public boolean isConnected(int treadle, int shaft) {
        if (r.getTieup().isDefined()) {
            List<Integer> shaftsForTreadle = r.getTieup().get().getShaftsForTreadle(treadle);
            return shaftsForTreadle.contains(shaft);
        }
        return shaft == treadle;
    }

    public int getTotalTreadles() {
        if (weaving.isDefined()) {
            return weaving.get().getTreadles();
        }
        return 1;
    }

    public int getTotalShafts() {
        if (weaving.isDefined()) {
            return weaving.get().getTreadles();
        }
        return 1;
    }

    public BufferedImage makeImage(int width, int height) {
        TieUpImage im = new TieUpImage(this);
        return im.getTieUpImage(width, height);
    }

    public Option<WIFTieup> getTieup() {
        return tieup;
    }

    public Option<WIFLiftPlan> getLiftPlan() {
        return liftPlan;
    }

    public Option<WIFWeaving> getWeaving() {
        return weaving;
    }

    public void optimize(WIFReader r) {
        if (weaving.isDefined()) {
            weaving.get().optimize(r);
        }
        if (tieup.isDefined()) {
            tieup.get().optimize(r);
        }

        if (liftPlan.isDefined()) {
            liftPlan.get().optimize(r);
        }
        setValues(r);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TieUpSimulator that = (TieUpSimulator) o;
        return Objects.equals(r, that.r) &&
                Objects.equals(tieup, that.tieup) &&
                Objects.equals(liftPlan, that.liftPlan) &&
                Objects.equals(weaving, that.weaving);
    }

    @Override
    public int hashCode() {
        return Objects.hash(r, tieup, liftPlan, weaving);
    }
}

