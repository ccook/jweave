/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate.image;

import com.gitlab.ccook.jweave.simulate.TieUpSimulator;
import com.gitlab.ccook.jweave.simulate.helper.SimulatorImageUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Objects;

public class TieUpImage {

    private BufferedImage bufimage;
    private TieUpSimulator ts;

    public TieUpImage(TieUpSimulator ts) {
        this.ts = ts;
    }

    public BufferedImage getTieUpImage(int width, int height) {
        bufimage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bufimage.createGraphics();

        drawGrid(g2d, 0, 0, width, height);
        g2d.dispose();
        bufimage = SimulatorImageUtils.flipH(bufimage);
        return bufimage;
    }

    private void drawGrid(Graphics2D g2d, int startX, int startY, int width, int height) {
        int treadles = ts.getTotalTreadles();
        int shafts = ts.getTotalShafts();
        int sectionWidth = (int) Math.floor((double) width / treadles);
        int sectionHeight = (int) Math.floor((double) height / shafts);
        boolean risingShed = this.ts.getWeaving().isDefined() &&
                this.ts.getWeaving().get().getRisingShed().isDefined() ? ts.getWeaving().get().getRisingShed().get() : true;
        for (int x = 0; x < treadles; x++) {
            for (int y = 0; y < shafts; y++) {
                if (ts.isConnected(x + 1, y + 1)) {
                    g2d.setColor(Color.BLACK);
                    g2d.fillRect((startX + (x * sectionWidth)),
                            (startY + (y * sectionHeight)),
                            sectionWidth, sectionHeight);
                    if (risingShed) {
                        SimulatorImageUtils.overlayO(g2d,
                                (startX + (x * sectionWidth)),
                                (startY + (y * sectionHeight)),
                                sectionWidth, sectionHeight,
                                Color.WHITE);
                    } else {
                        SimulatorImageUtils.overlayX(g2d,
                                (startX + (x * sectionWidth)),
                                (startY + (y * sectionHeight)),
                                sectionWidth, sectionHeight,
                                Color.WHITE);

                    }
                } else {
                    g2d.setColor(Color.WHITE);
                    g2d.fillRect((startX + (x * sectionWidth)),
                            (startY + (y * sectionHeight)),
                            sectionWidth, sectionHeight);
                }
                SimulatorImageUtils.addBorder(g2d,
                        (startX + (x * sectionWidth)),
                        (startY + (y * sectionHeight)),
                        sectionWidth, sectionHeight,
                        1, Color.BLACK);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TieUpImage that = (TieUpImage) o;
        return Objects.equals(bufimage, that.bufimage) &&
                Objects.equals(ts, that.ts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bufimage, ts);
    }
}
