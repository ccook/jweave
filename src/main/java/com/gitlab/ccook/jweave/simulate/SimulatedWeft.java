/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate;

import com.gitlab.ccook.jweave.simulate.abst.AbstractWarpOrWeft;
import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.WIFColorTable;
import com.gitlab.ccook.jweave.wif.model.WIFTieup;
import com.gitlab.ccook.jweave.wif.model.helper.WIFUnit;
import com.gitlab.ccook.jweave.wif.model.weft.*;

import java.awt.*;
import java.util.List;
import java.util.*;

@SuppressWarnings("unchecked")
public class SimulatedWeft extends AbstractWarpOrWeft {
    private Option<WIFTreadling> treadling;
    private Option<WIFTieup> tieup;
    private Option<WIFWeft> weft;
    private Option<WIFWeftColors> colors;
    private Option<WIFColorTable> colorTable;
    private Option<WIFWeftSpacing> spacing;
    private Option<WIFWeftSpacingZoom> spacingZoom;
    private Option<WIFWeftThickness> thickness;
    private Option<WIFWeftThicknessZoom> thicknessZoom;
    private Option<WIFWeftSymbols> symbols;
    private Option<WIFLiftPlan> liftPlan;

    public SimulatedWeft(WIFReader r) {
        super(r);
        setValues(r);
    }

    private void setValues(WIFReader r) {
        this.treadling = r.getTreadling();
        this.tieup = r.getTieup();
        this.weft = r.getWeft();
        this.colors = r.getWeftColors();
        this.colorTable = r.getColorTable();
        this.spacing = r.getWeftSpacing();
        this.spacingZoom = r.getWeftSpacingZoom();
        this.thickness = r.getWeftThickness();
        this.thicknessZoom = r.getWeftThicknessZoom();
        this.symbols = r.getWeftSymbols();
        this.liftPlan = r.getLiftPlan();
    }

    public List<Integer> getTreadlingForPick(int pick) {
        if (treadling.isDefined()) {
            return this.treadling.get().getTreadling(pick);
        } else if (liftPlan.isDefined()) {
            return this.liftPlan.get().getShaftsForPick(pick);
        }

        return new ArrayList<>();
    }

    public List<Integer> getUpShaftsForPick(int pick) {
        List<Integer> upShafts = new ArrayList<>();

        // TODO FIXME on sinking looms, this is reversed!
        if (treadling.isDefined()) {
            List<Integer> treadling = this.treadling.get().getTreadling(pick);
            if (tieup.isDefined()) {
                if (weft.get().getWeaving().isDefined()) {
                    Option<Boolean> risingShed = weft.get().getWeaving().get().getRisingShed();
                    if (risingShed.isDefined() && !risingShed.get()) {
                        List<Integer> downShafts = new ArrayList<>();
                        for (Integer treadle : treadling) {
                            List<Integer> shafts = tieup.get().getShaftsForTreadle(treadle);
                            downShafts.addAll(shafts);
                        }
                        int shafts = weft.get().getWeaving().get().getShafts();
                        for (int i = 1; i <= shafts; i++) {
                            if (!downShafts.contains(i)) {
                                upShafts.add(i);
                            }
                        }
                    } else {
                        for (Integer treadle : treadling) {
                            List<Integer> shafts = tieup.get().getShaftsForTreadle(treadle);
                            upShafts.addAll(shafts);
                        }
                    }
                } else {
                    System.out.println("Weaving not defined");
                }
            } else {
                System.out.println("No tieup defined");
            }
        } else {
            if (liftPlan.isDefined()) {
                List<Integer> treadling = this.liftPlan.get().getShaftsForPick(pick);
                if (weft.get().getWeaving().isDefined()) {
                    Option<Boolean> risingShed = weft.get().getWeaving().get().getRisingShed();
                    if (risingShed.isDefined() && !risingShed.get()) {
                        //compare treadling vs total number of shafts
                        int totalShafts = weft.get().getWeaving().get().getShafts();
                        for (int i = 1; i <= totalShafts; i++) {
                            if (!treadling.contains(i)) {
                                upShafts.add(i);
                            }
                        }
                    } else {
                        upShafts.addAll(treadling);
                    }
                }
            }
        }

        return upShafts;
    }

    @Override
    public Option<Color> getColor(int weftThread) {
        if (colors.isDefined()) {
            return colors.get().getWeftColor(weftThread);
        }
        if (weft.isDefined() && colorTable.isDefined()) {
            return colorTable.get().lookupColor(weft.get().getColorPaletteIndex().get());
        }
        return Option.NONE;
    }

    public Pair<Double, WIFUnit> getTotalSpacing(int warpThread) {
        int spacingZ = 1;
        double space = 0;
        WIFUnit unit = WIFUnit.UNKNOWN;
        if (spacingZoom.isDefined() && spacingZoom.get().getZoom(warpThread).isDefined()) {
            spacingZ = spacingZoom.get().getZoom(warpThread).get();

        } else if (weft.isDefined() && weft.get().getSpacingZoom().isDefined()) {
            spacingZ = weft.get().getSpacingZoom().get();
        }

        if (spacing.isDefined() && spacing.get().getSpacing(warpThread).isDefined()) {
            space = spacing.get().getSpacing(warpThread).get().getFirst();
            unit = spacing.get().getSpacing(warpThread).get().getSecond();

        } else if (weft.isDefined() && weft.get().getSpacing().isDefined()) {
            space = weft.get().getSpacing().get();
        }

        if (unit == WIFUnit.UNKNOWN && weft.isDefined() && weft.get().getUnits().isDefined()) {
            unit = weft.get().getUnits().get();
        }

        return new Pair<>(space * spacingZ, unit);
    }


    public Pair<Double, WIFUnit> getTotalThickness(int warpThread) {
        int thickZ = 1;
        double thick = 1;
        WIFUnit unit = WIFUnit.UNKNOWN;
        if (thicknessZoom.isDefined() && thicknessZoom.get().getZoom(warpThread).isDefined()) {
            thickZ = thicknessZoom.get().getZoom(warpThread).get();
        } else if (weft.isDefined() && weft.get().getThicknessZoom().isDefined()) {
            thickZ = weft.get().getThicknessZoom().get();
        }

        if (thickness.isDefined() && thickness.get().getThickness(warpThread).isDefined()) {
            thick = thickness.get().getThickness(warpThread).get().getFirst();
            unit = thickness.get().getThickness(warpThread).get().getSecond();

        } else if (weft.isDefined() && weft.get().getThickness().isDefined()) {
            thick = weft.get().getThickness().get();
        }

        if (unit == WIFUnit.UNKNOWN && weft.isDefined() && weft.get().getUnits().isDefined()) {
            unit = weft.get().getUnits().get();
        }

        return new Pair<>(thick * thickZ, unit);
    }

    public Option<String> getSymbol(int warpThread) {
        Option<String> sym = Option.NONE;
        if (symbols.isDefined()) {
            if (symbols.get().getSymbol(warpThread).isDefined()) {
                return symbols.get().getSymbol(warpThread);
            }
        }
        if (weft.isDefined()) {
            return weft.get().getSymbol();
        }
        return sym;
    }


    public int getPickCount() {
        //TODO consider other ways
        if (weft.isDefined()) {
            return weft.get().getThreads();
        }
        return 1;
    }

    public int getTotalTreadles() {
        if (treadling.isDefined()) {
            Set<Integer> treadles = new HashSet<>();
            for (Map.Entry<Integer, List<Integer>> k : treadling.get().getTable().entrySet()) {
                treadles.addAll(k.getValue());
            }
            return treadles.size();
        }
        if (weft.isDefined() && weft.get().getWeaving().isDefined()) {
            return weft.get().getWeaving().get().getTreadles();
        }
        return 1;
    }

    public void optimize(WIFReader r) {
        weft.get().optimize(r);
        setValues(r);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimulatedWeft that = (SimulatedWeft) o;
        return Objects.equals(treadling, that.treadling) &&
                Objects.equals(tieup, that.tieup) &&
                Objects.equals(weft, that.weft) &&
                Objects.equals(colors, that.colors) &&
                Objects.equals(colorTable, that.colorTable) &&
                Objects.equals(spacing, that.spacing) &&
                Objects.equals(spacingZoom, that.spacingZoom) &&
                Objects.equals(thickness, that.thickness) &&
                Objects.equals(thicknessZoom, that.thicknessZoom) &&
                Objects.equals(symbols, that.symbols) &&
                Objects.equals(liftPlan, that.liftPlan);
    }

    @Override
    public int hashCode() {
        return Objects.hash(treadling, tieup, weft, colors, colorTable, spacing, spacingZoom, thickness, thicknessZoom, symbols, liftPlan);
    }
}
