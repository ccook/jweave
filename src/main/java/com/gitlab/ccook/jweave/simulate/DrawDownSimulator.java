/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate;

import com.gitlab.ccook.jweave.simulate.image.DrawDownImage;
import com.gitlab.ccook.jweave.wif.WIFReader;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Objects;

public class DrawDownSimulator {
    private SimulatedWarp warp;
    private SimulatedWeft weft;
    private WIFReader r;

    public DrawDownSimulator(WIFReader r) {
        this.r = r;
        this.warp = new SimulatedWarp(r);
        this.weft = new SimulatedWeft(r);
    }

    public SimulatedWarp getWarp() {
        return warp;
    }

    public SimulatedWeft getWeft() {
        return weft;
    }

    public VisibleThread getVisible(int weftPick, int warpThread) {
        List<Integer> upShafts = weft.getUpShaftsForPick(weftPick);

        List<Integer> threadedShafts = warp.getThreadedShaftsForWarp(warpThread);

        for (Integer upShaft : upShafts) {
            if (threadedShafts.contains(upShaft)) {
                // Got a warp
                VisibleThread t = new VisibleThread(warpThread, warp);
                return t;
            }
        }
        VisibleThread t = new VisibleThread(weftPick, weft);
        return t;
    }

    public BufferedImage makeImage(int width, int height) {
        DrawDownImage img = new DrawDownImage(this);
        return img.getDrawDownImage(width, height);
    }

    public void optimize(WIFReader r) {
        warp.optimize(r);
        weft.optimize(r);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrawDownSimulator that = (DrawDownSimulator) o;
        return Objects.equals(warp, that.warp) &&
                Objects.equals(weft, that.weft) &&
                Objects.equals(r, that.r);
    }

    @Override
    public int hashCode() {
        return Objects.hash(warp, weft, r);
    }
}
