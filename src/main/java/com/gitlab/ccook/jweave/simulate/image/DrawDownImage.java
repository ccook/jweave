/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate.image;

import com.gitlab.ccook.jweave.simulate.DrawDownSimulator;
import com.gitlab.ccook.jweave.simulate.VisibleThread;
import com.gitlab.ccook.jweave.simulate.helper.SimulatorImageUtils;
import com.gitlab.ccook.jweave.util.Option;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Objects;

public class DrawDownImage {
    private DrawDownSimulator dd;
    private BufferedImage bufimage;

    public DrawDownImage(DrawDownSimulator dd) {
        this.dd = dd;
    }


    public BufferedImage getDrawDownImage(int width, int height) {
        bufimage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bufimage.createGraphics();

        drawGrid(g2d, 0, 0, width, height);
        g2d.dispose();
        bufimage = SimulatorImageUtils.flipH(bufimage);
        return bufimage;

    }


    private void drawGrid(Graphics2D g2d, int startX, int startY, int width, int height) {
        int threads = dd.getWarp().getThreadCount();
        int picks = dd.getWeft().getPickCount();

        int sectionWidth = (int) Math.ceil((double) width / threads);
        int sectionHeight = (int) Math.ceil((double) height / picks);
        for (int x = 0; x < threads; x++) {
            for (int y = 0; y < picks; y++) {
                VisibleThread visable = dd.getVisible(y + 1, x + 1);
                switch (visable.getType()) {
                    case WARP:
                        Color color = Color.black;
                        Option<Color> colorOption = visable.getThread().getColor(x + 1);
                        if (colorOption.isDefined()) {
                            color = colorOption.get();
                        }

                        g2d.setColor(color);
                        g2d.fillRect((startX + (x * sectionWidth)),
                                (startY + (y * sectionHeight)),
                                sectionWidth, sectionHeight);
                        break;
                    case WEFT:
                        color = Color.white;
                        colorOption = visable.getThread().getColor(y + 1);
                        if (colorOption.isDefined()) {
                            color = colorOption.get();
                        }
                        g2d.setColor(color);
                        g2d.fillRect((startX + (x * sectionWidth)),
                                (startY + (y * sectionHeight)),
                                sectionWidth, sectionHeight);
                        break;
                }

            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrawDownImage that = (DrawDownImage) o;
        return Objects.equals(dd, that.dd) &&
                Objects.equals(bufimage, that.bufimage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dd, bufimage);
    }
}
