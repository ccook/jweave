/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.simulate.helper;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

public class SimulatorImageUtils {

    public static BufferedImage flipH(BufferedImage image) {
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight() / 2; j++) {
                int tmp = image.getRGB(i, j);
                image.setRGB(i, j, image.getRGB(i, image.getHeight() - j - 1));
                image.setRGB(i, image.getHeight() - j - 1, tmp);
            }
        }
        return image;
    }

    public static BufferedImage flipV(BufferedImage bufferedImage) {
        AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
        tx.translate(-bufferedImage.getWidth(null), 0);
        AffineTransformOp op = new AffineTransformOp(tx,
                AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        bufferedImage = op.filter(bufferedImage, null);
        return bufferedImage;
    }

    public static void addBorder(Graphics2D g2d, int x, int y, int width, int height, int thickness, Color c) {
        Stroke oldStroke = g2d.getStroke();
        g2d.setStroke(new BasicStroke(thickness));
        g2d.setColor(c);
        g2d.drawRect(x, y, width, height);
        g2d.setStroke(oldStroke);
    }


    public static double calculateScaling(Graphics2D g2, int imageWidth, int imageHeight, String text, Font font) {

        //These are final to avoid accidental mending, since they are
        //the base for our calculations.
        //Belive me, it took me a while to debug the
        //scale calculation when I made this for my games :P

        FontMetrics fm = g2.getFontMetrics(font);


        /*
         * requestedStringWidthSize e requestedStringHeightSize are the measures needed
         * to draw the text WITHOUT resizing.
         */
        final int requestedStringWidthSize = fm.stringWidth(text);
        final int requestedStringHeightSize = fm.getHeight();


        double stringHeightSizeToUse = imageHeight;
        double stringWidthSizeToUse;

        double scale = stringHeightSizeToUse / requestedStringHeightSize;
        stringWidthSizeToUse = scale * requestedStringWidthSize;

        /*
         * Checking if fill in height makes the text go out of bound in width,
         * if it does, it rescalates it to size it to maximum width.
         */
        if (imageWidth < ((int) (Math.rint(stringWidthSizeToUse)))) {
            stringWidthSizeToUse = imageWidth;

            scale = stringWidthSizeToUse / requestedStringWidthSize;
            //stringHeightSizeToUse = scale*requestedStringHeightSize;
        }

        g2.dispose(); //we created this to use fontmetrics, now we don't need it.

        return scale;
    }

    public static void overlayO(Graphics2D g2d, int startX, int startY, int sectionWidth, int sectionHeight, Color color) {
        Color oldColor = g2d.getColor();
        g2d.setColor(color);
        g2d.fillOval(
                startX + (int) Math.floor(sectionWidth * .02),
                startY + (int) Math.floor(sectionHeight * .02),
                (int) Math.floor(sectionWidth * .9),
                (int) Math.floor(sectionHeight * .9));
        g2d.setColor(oldColor);
        g2d.fillOval(
                startX + (int) Math.floor(sectionWidth * .05),
                startY + (int) Math.floor(sectionHeight * .05),
                (int) Math.floor(sectionWidth * .8),
                (int) Math.floor(sectionHeight * .8));
    }

    public static void overlayX(Graphics2D g2d, int startX, int startY, int sectionWidth, int sectionHeight, Color color) {
        g2d.setColor(color);
        // \
        g2d.drawLine(startX, startY, (startX + sectionWidth), (startY + sectionHeight));
        // /
        g2d.drawLine(startX, (startY + sectionHeight), (startX + sectionWidth), startY);
    }
}
