/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.util;

import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.warp.WIFThreading;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarp;
import com.gitlab.ccook.jweave.wif.model.weft.WIFLiftPlan;
import com.gitlab.ccook.jweave.wif.model.weft.WIFWeft;

import java.util.List;

public class SettCalculator {

    public static int calculateEPI(WIFReader r, int yardsPerPound) {
        //   r.optimize();
        double diameterNumber = .9 * Math.sqrt(yardsPerPound);  //N
        WIFWeft weft = r.getWeft().get();
        //  weft.optimize(r, true);
        int numberOfPicksInOneRepeat = weft.getThreads();
        WIFLiftPlan p = r.getLiftPlan().get();
        double tiesTotal = 0;
        WIFWarp wifWarp = r.getWarp().get();
        // wifWarp.optimize(r, true);
        int numberOfEndsInOneRepeat = wifWarp.getThreads();
        for (int pick = 1; pick <= numberOfPicksInOneRepeat; pick++) {
            List<Integer> shaftsEngaged = p.getShaftsForPick(pick);
            WIFThreading threading = r.getThreading().get();
            for (int warpN = 1; warpN < wifWarp.getThreads(); warpN++) {
                List<Integer> shaftsForWarpN = threading.getShaftsForWarp(warpN);
                List<Integer> shaftsForWarpNPlus1 = threading.getShaftsForWarp(warpN + 1);
                if (shaftsEngaged.contains(shaftsForWarpN.get(0)) && !shaftsEngaged.contains(shaftsForWarpNPlus1.get(0))) {
                    tiesTotal++;
                } else if (!shaftsEngaged.contains(shaftsForWarpN.get(0)) && shaftsEngaged.contains(shaftsForWarpNPlus1.get(0))) {
                    tiesTotal++;
                }

            }
            tiesTotal++;
        }
        double ties = tiesTotal / numberOfEndsInOneRepeat; //T
        double perfectSett = (diameterNumber * numberOfEndsInOneRepeat) / (numberOfPicksInOneRepeat + ties);
        return (int) perfectSett;

    }
}
