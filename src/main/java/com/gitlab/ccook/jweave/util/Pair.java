/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.util;

import java.util.Objects;

/**
 * Helper object that holds two objects together
 *
 * @param <A> The type of the first element of the pair
 * @param <B> The type of the second element of the pair
 */
public class Pair<A, B> {

    private final A a;
    private final B b;

    public Pair(A thingA, B thingB) {
        this.a = thingA;
        this.b = thingB;
    }

    /**
     * @return the first object of the pair
     */
    public A getFirst() {
        return a;
    }

    /**
     * @return the second object of the pair
     */
    public B getSecond() {
        return b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(a, pair.a) &&
                Objects.equals(b, pair.b);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b);
    }

    @Override
    public String toString() {
        return "Pair{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }

}
