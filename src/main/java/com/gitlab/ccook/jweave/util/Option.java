/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave.util;

import java.util.Objects;

/**
 * Helper object that holds an object that may be undefined,
 * Implementers that get an Option returned to them should check
 * isDefined() before calling get()
 *
 * @param <T> The container; what's inside the option, if defined
 */
public class Option<T> {

    private final T thing;
    public static final Option NONE = new Option<>(null);

    public Option(T thing) {
        this.thing = thing;
    }

    /**
     * @return if the object held by the option is not null
     */
    public boolean isDefined() {
        return thing != null;
    }

    /**
     * @return the object held by the option (check isDefined() first)
     */
    public T get() {
        return thing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Option<?> option = (Option<?>) o;
        return Objects.equals(thing, option.thing);
    }

    @Override
    public int hashCode() {
        return Objects.hash(thing);
    }

    @Override
    public String toString() {
        if (thing != null) {
            return thing.toString();
        }
        return "None()";
    }
}
