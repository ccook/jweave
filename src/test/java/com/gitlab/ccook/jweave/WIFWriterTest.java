/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFBuilder;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.*;
import com.gitlab.ccook.jweave.wif.model.helper.WIFColorTripplet;
import com.gitlab.ccook.jweave.wif.model.helper.WIFUnit;
import com.gitlab.ccook.jweave.wif.model.warp.WIFThreading;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarp;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarpColors;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarpSpacing;
import com.gitlab.ccook.jweave.wif.model.weft.WIFLiftPlan;
import com.gitlab.ccook.jweave.wif.model.weft.WIFTreadling;
import com.gitlab.ccook.jweave.wif.model.weft.WIFWeft;
import org.junit.Test;

import java.io.InputStream;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class WIFWriterTest {

    @Test
    public void writeFromWIFTest1() throws Exception {
        InputStream is = getInputStream("test1.wif");
        WIFReader r = new WIFReader(is);
        // Full unoptimized WIF
        String wifFromReader = r.toString();

        // HEADER
        WIFHeader header = r.getHeader();
        String headerAsWIF = "[WIF]\n" +
                "Version=1.1\n" +
                "Date=April 20, 1997\n" +
                "Developers=wif@mhsoft.com\n" +
                "Source Program=Fiberworks PCW\n" +
                "Source Version=4.2";
        assertEquals(header.toString(), headerAsWIF);
        // CONTENTS
        WIFContents contents = r.getContents();
        String contentsAsWIF = "[CONTENTS]\n" +
                "COLOR PALETTE=true\n" +
                "TEXT=true\n" +
                "WEAVING=true\n" +
                "WARP=true\n" +
                "WEFT=true\n" +
                "COLOR TABLE=true\n" +
                "THREADING=true\n" +
                "TIEUP=true\n" +
                "TREADLING=true\n" +
                "WARP COLORS=true\n" +
                "WARP SPACING=true\n" +
                "WEFT COLORS=true\n" +
                "WEFT SPACING=true";

        assertEquals(contents.toString(), contentsAsWIF);
        // COLOR PALETTE
        Option<WIFColorPalette> colorPaletteOption = r.getColorPalette();
        assertTrue("Color Palette expected but not found", colorPaletteOption.isDefined());
        WIFColorPalette colorPalette = colorPaletteOption.get();
        String colorPaletteAsWIF = "[COLOR PALETTE]\n" +
                "Entries=82\n" +
                "Range=0,999";
        assertEquals(colorPalette.toString(), colorPaletteAsWIF);
        // TEXT
        Option<WIFText> textOption = r.getText();
        assertTrue(textOption.isDefined());
        WIFText text = textOption.get();
        String textAsWIF = "[TEXT]\n" +
                "Title=#1 Shadow weave thick and thin based on honeysuckle long runsl";
        assertEquals(text.toString(), textAsWIF);
        // WEAVING
        Option<WIFWeaving> weavingOption = r.getWeaving();
        assertTrue(weavingOption.isDefined());
        WIFWeaving weaving = weavingOption.get();
        String weavingAsWIF = "[WEAVING]\n" +
                "Shafts=8\n" +
                "Treadles=8\n" +
                "Rising Shed=true";
        assertEquals(weaving.toString(), weavingAsWIF);
        // WARP
        Option<WIFWarp> warpOption = r.getWarp();
        assertTrue(warpOption.isDefined());
        WIFWarp warp = warpOption.get();
        String warpAsWIF = "[WARP]\n" +
                "Threads=459\n" +
                "Color=3\n" +
                "Units=Centimeters\n" +
                "Spacing=0.212\n" +
                "Thickness=0.212";
        assertEquals(warp.toString(), warpAsWIF);
        Option<WIFWeft> weftOption = r.getWeft();
        assertTrue(weftOption.isDefined());
        // WEFT
        WIFWeft weft = weftOption.get();
        String weftAsWIF = "[WEFT]\n" +
                "Threads=552\n" +
                "Color=6\n" +
                "Units=Centimeters\n" +
                "Spacing=0.212\n" +
                "Thickness=0.212";
        assertEquals(weft.toString(), weftAsWIF);
        // TIEUP
        Option<WIFTieup> tieupOption = r.getTieup();
        assertTrue(tieupOption.isDefined());
        WIFTieup tieup = tieupOption.get();
        String tieupAsWIF = "[TIEUP]\n" +
                "1=1,2,3,4\n" +
                "2=2,3,4,5\n" +
                "3=3,4,5,6\n" +
                "4=4,5,6,7\n" +
                "5=5,6,7,8\n" +
                "6=1,6,7,8\n" +
                "7=1,2,7,8\n" +
                "8=1,2,3,8";
        assertEquals(tieup.toString(), tieupAsWIF);
        // COLOR TABLE
        Option<WIFColorTable> colorTableOption = r.getColorTable();
        assertTrue(colorTableOption.isDefined());
        WIFColorTable colorTable = colorTableOption.get();
        String colorTableAsWIF = "[COLOR TABLE]\n" +
                "1=999,999,999\n" +
                "2=0,0,0\n" +
                "3=121,0,607\n" +
                "4=0,266,999\n" +
                "5=0,999,999\n" +
                "6=0,587,0\n" +
                "7=466,999,0\n" +
                "8=999,999,0\n" +
                "9=999,532,0\n" +
                "10=999,0,0\n" +
                "11=999,0,599\n" +
                "12=799,0,999\n" +
                "13=481,305,767\n" +
                "14=399,607,999\n" +
                "15=399,999,999\n" +
                "16=399,999,399\n" +
                "17=787,999,399\n" +
                "18=999,999,399\n" +
                "19=999,677,399\n" +
                "20=999,399,399\n" +
                "21=999,399,760\n" +
                "22=877,399,999\n" +
                "23=121,0,599\n" +
                "24=0,160,599\n" +
                "25=0,599,599\n" +
                "26=0,599,0\n" +
                "27=278,599,0\n" +
                "28=599,599,0\n" +
                "29=599,321,0\n" +
                "30=599,0,0\n" +
                "31=599,0,360\n" +
                "32=477,0,599\n" +
                "33=368,266,799\n" +
                "34=266,399,799\n" +
                "35=266,799,799\n" +
                "36=266,799,266\n" +
                "37=599,799,266\n" +
                "38=799,799,266\n" +
                "39=799,532,266\n" +
                "40=799,266,266\n" +
                "41=799,266,599\n" +
                "42=666,266,799\n" +
                "43=148,0,799\n" +
                "44=0,199,799\n" +
                "45=0,799,799\n" +
                "46=0,799,0\n" +
                "47=348,799,0\n" +
                "48=799,799,0\n" +
                "49=799,399,0\n" +
                "50=799,0,0\n" +
                "51=799,0,450\n" +
                "52=599,0,799\n" +
                "53=658,532,999\n" +
                "54=532,666,999\n" +
                "55=532,999,999\n" +
                "56=532,999,532\n" +
                "57=901,999,532\n" +
                "58=999,999,532\n" +
                "59=999,697,532\n" +
                "60=999,532,532\n" +
                "61=999,532,799\n" +
                "62=940,532,999\n" +
                "63=195,133,932\n" +
                "64=133,266,932\n" +
                "65=133,932,932\n" +
                "66=133,932,133\n" +
                "67=278,932,133\n" +
                "68=932,932,133\n" +
                "69=932,321,133\n" +
                "70=932,133,133\n" +
                "71=932,133,360\n" +
                "72=477,133,932\n" +
                "73=501,399,932\n" +
                "74=399,532,932\n" +
                "75=399,932,932\n" +
                "76=399,932,399\n" +
                "77=732,932,399\n" +
                "78=932,932,399\n" +
                "79=932,666,399\n" +
                "80=932,399,399\n" +
                "81=932,399,732\n" +
                "82=799,399,932";
        assertEquals(colorTable.toString(), colorTableAsWIF);

        // THREADING
        Option<WIFThreading> threadingOption = r.getThreading();
        assertTrue(threadingOption.isDefined());
        WIFThreading threading = threadingOption.get();
        String threadingAsWIF = "[THREADING]\n" +
                "1=5\n" +
                "2=2\n" +
                "3=6\n" +
                "4=3\n" +
                "5=7\n" +
                "6=2\n" +
                "7=6\n" +
                "8=3\n" +
                "9=7\n" +
                "10=4\n" +
                "11=8\n" +
                "12=3\n" +
                "13=8\n" +
                "14=4\n" +
                "15=1\n" +
                "16=5\n" +
                "17=2\n" +
                "18=6\n" +
                "19=3\n" +
                "20=7\n" +
                "21=4\n" +
                "22=8\n" +
                "23=5\n" +
                "24=2\n" +
                "25=6\n" +
                "26=3\n" +
                "27=7\n" +
                "28=4\n" +
                "29=8\n" +
                "30=4\n" +
                "31=1\n" +
                "32=5\n" +
                "33=2\n" +
                "34=6\n" +
                "35=3\n" +
                "36=7\n" +
                "37=4\n" +
                "38=8\n" +
                "39=5\n" +
                "40=1\n" +
                "41=6\n" +
                "42=2\n" +
                "43=5\n" +
                "44=1\n" +
                "45=6\n" +
                "46=2\n" +
                "47=7\n" +
                "48=3\n" +
                "49=6\n" +
                "50=2\n" +
                "51=7\n" +
                "52=3\n" +
                "53=8\n" +
                "54=4\n" +
                "55=7\n" +
                "56=3\n" +
                "57=7\n" +
                "58=4\n" +
                "59=8\n" +
                "60=3\n" +
                "61=7\n" +
                "62=2\n" +
                "63=6\n" +
                "64=3\n" +
                "65=7\n" +
                "66=2\n" +
                "67=6\n" +
                "68=1\n" +
                "69=5\n" +
                "70=2\n" +
                "71=6\n" +
                "72=1\n" +
                "73=5\n" +
                "74=8\n" +
                "75=4\n" +
                "76=7\n" +
                "77=3\n" +
                "78=6\n" +
                "79=2\n" +
                "80=5\n" +
                "81=1\n" +
                "82=4\n" +
                "83=8\n" +
                "84=4\n" +
                "85=7\n" +
                "86=3\n" +
                "87=6\n" +
                "88=2\n" +
                "89=5\n" +
                "90=8\n" +
                "91=4\n" +
                "92=7\n" +
                "93=3\n" +
                "94=6\n" +
                "95=2\n" +
                "96=5\n" +
                "97=1\n" +
                "98=4\n" +
                "99=8\n" +
                "100=3\n" +
                "101=8\n" +
                "102=4\n" +
                "103=7\n" +
                "104=3\n" +
                "105=6\n" +
                "106=2\n" +
                "107=7\n" +
                "108=3\n" +
                "109=6\n" +
                "110=2\n" +
                "111=5\n" +
                "112=1\n" +
                "113=5\n" +
                "114=2\n" +
                "115=6\n" +
                "116=1\n" +
                "117=5\n" +
                "118=2\n" +
                "119=6\n" +
                "120=3\n" +
                "121=7\n" +
                "122=2\n" +
                "123=6\n" +
                "124=3\n" +
                "125=7\n" +
                "126=4\n" +
                "127=8\n" +
                "128=3\n" +
                "129=8\n" +
                "130=4\n" +
                "131=1\n" +
                "132=5\n" +
                "133=2\n" +
                "134=6\n" +
                "135=3\n" +
                "136=7\n" +
                "137=4\n" +
                "138=8\n" +
                "139=5\n" +
                "140=2\n" +
                "141=6\n" +
                "142=3\n" +
                "143=7\n" +
                "144=4\n" +
                "145=8\n" +
                "146=4\n" +
                "147=1\n" +
                "148=5\n" +
                "149=2\n" +
                "150=6\n" +
                "151=3\n" +
                "152=7\n" +
                "153=4\n" +
                "154=8\n" +
                "155=5\n" +
                "156=1\n" +
                "157=6\n" +
                "158=2\n" +
                "159=5\n" +
                "160=1\n" +
                "161=6\n" +
                "162=2\n" +
                "163=7\n" +
                "164=3\n" +
                "165=6\n" +
                "166=2\n" +
                "167=7\n" +
                "168=3\n" +
                "169=8\n" +
                "170=4\n" +
                "171=7\n" +
                "172=3\n" +
                "173=7\n" +
                "174=4\n" +
                "175=8\n" +
                "176=3\n" +
                "177=7\n" +
                "178=2\n" +
                "179=6\n" +
                "180=3\n" +
                "181=7\n" +
                "182=2\n" +
                "183=6\n" +
                "184=1\n" +
                "185=5\n" +
                "186=2\n" +
                "187=6\n" +
                "188=1\n" +
                "189=5\n" +
                "190=8\n" +
                "191=4\n" +
                "192=7\n" +
                "193=3\n" +
                "194=6\n" +
                "195=2\n" +
                "196=5\n" +
                "197=1\n" +
                "198=4\n" +
                "199=8\n" +
                "200=4\n" +
                "201=7\n" +
                "202=3\n" +
                "203=6\n" +
                "204=2\n" +
                "205=5\n" +
                "206=8\n" +
                "207=4\n" +
                "208=7\n" +
                "209=3\n" +
                "210=6\n" +
                "211=2\n" +
                "212=5\n" +
                "213=1\n" +
                "214=4\n" +
                "215=8\n" +
                "216=3\n" +
                "217=8\n" +
                "218=4\n" +
                "219=7\n" +
                "220=3\n" +
                "221=6\n" +
                "222=2\n" +
                "223=7\n" +
                "224=3\n" +
                "225=6\n" +
                "226=2\n" +
                "227=5\n" +
                "228=1\n" +
                "229=5\n" +
                "230=2\n" +
                "231=6\n" +
                "232=1\n" +
                "233=5\n" +
                "234=2\n" +
                "235=6\n" +
                "236=3\n" +
                "237=7\n" +
                "238=2\n" +
                "239=6\n" +
                "240=3\n" +
                "241=7\n" +
                "242=4\n" +
                "243=8\n" +
                "244=3\n" +
                "245=8\n" +
                "246=4\n" +
                "247=1\n" +
                "248=5\n" +
                "249=2\n" +
                "250=6\n" +
                "251=3\n" +
                "252=7\n" +
                "253=4\n" +
                "254=8\n" +
                "255=5\n" +
                "256=2\n" +
                "257=6\n" +
                "258=3\n" +
                "259=7\n" +
                "260=4\n" +
                "261=8\n" +
                "262=4\n" +
                "263=1\n" +
                "264=5\n" +
                "265=2\n" +
                "266=6\n" +
                "267=3\n" +
                "268=7\n" +
                "269=4\n" +
                "270=8\n" +
                "271=5\n" +
                "272=1\n" +
                "273=6\n" +
                "274=2\n" +
                "275=5\n" +
                "276=1\n" +
                "277=6\n" +
                "278=2\n" +
                "279=7\n" +
                "280=3\n" +
                "281=6\n" +
                "282=2\n" +
                "283=7\n" +
                "284=3\n" +
                "285=8\n" +
                "286=4\n" +
                "287=7\n" +
                "288=3\n" +
                "289=7\n" +
                "290=4\n" +
                "291=8\n" +
                "292=3\n" +
                "293=7\n" +
                "294=2\n" +
                "295=6\n" +
                "296=3\n" +
                "297=7\n" +
                "298=2\n" +
                "299=6\n" +
                "300=1\n" +
                "301=5\n" +
                "302=2\n" +
                "303=6\n" +
                "304=1\n" +
                "305=5\n" +
                "306=8\n" +
                "307=4\n" +
                "308=7\n" +
                "309=3\n" +
                "310=6\n" +
                "311=2\n" +
                "312=5\n" +
                "313=1\n" +
                "314=4\n" +
                "315=8\n" +
                "316=4\n" +
                "317=7\n" +
                "318=3\n" +
                "319=6\n" +
                "320=2\n" +
                "321=5\n" +
                "322=8\n" +
                "323=4\n" +
                "324=7\n" +
                "325=3\n" +
                "326=6\n" +
                "327=2\n" +
                "328=5\n" +
                "329=1\n" +
                "330=4\n" +
                "331=8\n" +
                "332=3\n" +
                "333=8\n" +
                "334=4\n" +
                "335=7\n" +
                "336=3\n" +
                "337=6\n" +
                "338=2\n" +
                "339=7\n" +
                "340=3\n" +
                "341=6\n" +
                "342=2\n" +
                "343=5\n" +
                "344=1\n" +
                "345=5\n" +
                "346=2\n" +
                "347=6\n" +
                "348=1\n" +
                "349=5\n" +
                "350=2\n" +
                "351=6\n" +
                "352=3\n" +
                "353=7\n" +
                "354=2\n" +
                "355=6\n" +
                "356=3\n" +
                "357=7\n" +
                "358=4\n" +
                "359=8\n" +
                "360=3\n" +
                "361=8\n" +
                "362=4\n" +
                "363=1\n" +
                "364=5\n" +
                "365=2\n" +
                "366=6\n" +
                "367=3\n" +
                "368=7\n" +
                "369=4\n" +
                "370=8\n" +
                "371=5\n" +
                "372=2\n" +
                "373=6\n" +
                "374=3\n" +
                "375=7\n" +
                "376=4\n" +
                "377=8\n" +
                "378=4\n" +
                "379=1\n" +
                "380=5\n" +
                "381=2\n" +
                "382=6\n" +
                "383=3\n" +
                "384=7\n" +
                "385=4\n" +
                "386=8\n" +
                "387=5\n" +
                "388=1\n" +
                "389=6\n" +
                "390=2\n" +
                "391=5\n" +
                "392=1\n" +
                "393=6\n" +
                "394=2\n" +
                "395=7\n" +
                "396=3\n" +
                "397=6\n" +
                "398=2\n" +
                "399=7\n" +
                "400=3\n" +
                "401=8\n" +
                "402=4\n" +
                "403=7\n" +
                "404=3\n" +
                "405=7\n" +
                "406=4\n" +
                "407=8\n" +
                "408=3\n" +
                "409=7\n" +
                "410=2\n" +
                "411=6\n" +
                "412=3\n" +
                "413=7\n" +
                "414=2\n" +
                "415=6\n" +
                "416=1\n" +
                "417=5\n" +
                "418=2\n" +
                "419=6\n" +
                "420=1\n" +
                "421=5\n" +
                "422=8\n" +
                "423=4\n" +
                "424=7\n" +
                "425=3\n" +
                "426=6\n" +
                "427=2\n" +
                "428=5\n" +
                "429=1\n" +
                "430=4\n" +
                "431=8\n" +
                "432=4\n" +
                "433=7\n" +
                "434=3\n" +
                "435=6\n" +
                "436=2\n" +
                "437=5\n" +
                "438=8\n" +
                "439=4\n" +
                "440=7\n" +
                "441=3\n" +
                "442=6\n" +
                "443=2\n" +
                "444=5\n" +
                "445=1\n" +
                "446=4\n" +
                "447=8\n" +
                "448=3\n" +
                "449=8\n" +
                "450=4\n" +
                "451=7\n" +
                "452=3\n" +
                "453=6\n" +
                "454=2\n" +
                "455=7\n" +
                "456=3\n" +
                "457=6\n" +
                "458=2\n" +
                "459=5";
        assertEquals(threadingAsWIF, threading.toString());

        // WARP SPACING
        Option<WIFWarpSpacing> warpSpacingOption = r.getWarpSpacing();
        assertTrue(warpSpacingOption.isDefined());
        WIFWarpSpacing warpSpacing = warpSpacingOption.get();
        StringBuilder sb = new StringBuilder("[WARP SPACING]\n");
        for (int i = 2; i <= 458; i = i + 2) {
            sb.append(i).append("=0.424\n");
        }
        String warpSpacingAsWIF = sb.toString().trim();
        assertEquals(warpSpacingAsWIF, warpSpacing.toString());

        // WARP COLORS
        Option<WIFWarpColors> warpColorsOption = r.getWarpColors();
        assertTrue(warpColorsOption.isDefined());
        WIFWarpColors warpColors = warpColorsOption.get();
        sb = new StringBuilder("[WARP COLORS]\n");
        for (int i = 2; i <= 458; i = i + 2) {
            sb.append(i).append("=1\n");
        }
        String warpColorsAsWIF = sb.toString().trim();
        assertEquals(warpColorsAsWIF, warpColors.toString());
        r.close();
    }

    @Test
    public void writeFromBuilder1() throws Exception {
        WIFBuilder wifBuilder = new WIFBuilder();

        // HEADER
        WIFHeader.Builder header = new WIFHeader.Builder()
                .version("1.1")
                .developer("wif@mhsoft.com")
                .date("April 20, 1997")
                .sourceProgram("Fiberworks PCW")
                .sourceVersion("4.2");
        wifBuilder.header(header);

        // COLOR PALETTE
        WIFColorPalette.Builder colorPalette = new WIFColorPalette.Builder(82, new Pair<>(0, 999));
        wifBuilder.colorPalette(colorPalette);

        // TEXT
        WIFText.Builder text = new WIFText.Builder()
                .title("#1 Shadow weave thick and thin based on honeysuckle long runsl");
        wifBuilder.text(text);

        // WEAVING
        WIFWeaving.Builder weaving = new WIFWeaving.Builder(8, 8).risingShed(true);
        wifBuilder.weaving(weaving);

        // WARP
        WIFWarp.Builder warp = new WIFWarp.Builder(459)
                .units(WIFUnit.CENTIMETERS)
                .colorPaletteIndex(3)
                .spacing(0.212)
                .thickness(0.212);
        wifBuilder.warp(warp);

        // WEFT
        WIFWeft.Builder weft = new WIFWeft.Builder(552)
                .colorPaletteIndex(6)
                .units(WIFUnit.CENTIMETERS)
                .spacing(0.212)
                .thickness(0.212);
        wifBuilder.weft(weft);


        // TIEUP
        WIFTieup.Builder tieUp = new WIFTieup.Builder()
                .tieup(1, 1, 2, 3, 4)
                .tieup(2, 2, 3, 4, 5)
                .tieup(3, 3, 4, 5, 6)
                .tieup(4, 4, 5, 6, 7)
                .tieup(5, 5, 6, 7, 8)
                .tieup(6, 1, 6, 7, 8)
                .tieup(7, 1, 2, 7, 8)
                .tieup(8, 1, 2, 3, 8);
        wifBuilder.tieup(tieUp);

        // COLOR TABLE
        WIFColorTable.Builder colorTable = test182Colors(new WIFColorTable.Builder().palette(colorPalette));
        wifBuilder.colorTable(colorTable);

        // THREADING
        WIFThreading.Builder threading = new WIFThreading.Builder();
        for (int i = 1; i <= warp.build().getThreads(); i++) {
            threading.threading(i, (i % 8) + 1);
        }
        wifBuilder.threading(threading);

        // WARP SPACING
        WIFWarpSpacing.Builder warpSpacing = new WIFWarpSpacing.Builder();
        for (int i = 2; i <= 458; i += 2) {
            warpSpacing.spacing(i, 0.424);
        }
        wifBuilder.warpSpacing(warpSpacing);

        // WARP COLORS
        WIFWarpColors.Builder warpColors = new WIFWarpColors.Builder();
        for (int i = 2; i <= 458; i += 2) {
            warpColors.warpColor(i, 1);
        }
        wifBuilder.warpColors(warpColors);

        // LIFTPLAN
        WIFLiftPlan.Builder plan = new WIFLiftPlan.Builder();
        for (int i = 1; i <= weft.build().getThreads(); i++) {
            if (i % 2 == 0) {
                plan.liftPlan(i, 5, 6, 7, 8);
            } else {
                plan.liftPlan(i, 1, 2, 3, 4);
            }
        }
        wifBuilder.liftPlan(plan);

        //Build
        WIFReader build = wifBuilder.build();
        assertNotNull(build);
    }

    @Test
    public void writeFromWIFTest2() throws Exception {
        InputStream is = getInputStream("test2.wif");
        WIFReader r = new WIFReader(is);
        // Full unoptimized WIF
        String wifFromReader = r.toString();

        // HEADER
        WIFHeader header = r.getHeader();
        String headerAsWIF = "[WIF]\n" +
                "Version=1.1\n" +
                "Date=April 20, 1997\n" +
                "Developers=wif@mhsoft.com\n" +
                "Source Program=SwiftWeave\n" +
                "Source Version=5.14";
        assertEquals(header.toString(), headerAsWIF);

        // CONTENTS
        WIFContents contents = r.getContents();
        String contentsAsWIF = "[CONTENTS]\n" +
                "NOTES=true\n" +
                "WEAVING=true\n" +
                "COLOR PALETTE=true\n" +
                "COLOR TABLE=true\n" +
                "WARP=true\n" +
                "THREADING=true\n" +
                "WARP THICKNESS=false\n" +
                "WARP SPACING=false\n" +
                "WARP COLORS=true\n" +
                "WEFT=true\n" +
                "TREADLING=true\n" +
                "LIFTPLAN=false\n" +
                "WEFT THICKNESS=false\n" +
                "WEFT SPACING=false\n" +
                "TIEUP=true";
        assertEquals(contentsAsWIF, contents.toString());

        // NOTES
        Option<WIFNotes> notesOptions = r.getNotes();
        assertTrue(notesOptions.isDefined());
        WIFNotes notes = notesOptions.get();
        String notesAsWIF = "[NOTES]\n" +
                "1=Comment:";
        assertEquals(notesAsWIF, notes.toString());

        // WEAVING
        Option<WIFWeaving> weavingOption = r.getWeaving();
        assertTrue(weavingOption.isDefined());
        WIFWeaving weaving = weavingOption.get();
        String weavingAsWIF = "[WEAVING]\n" +
                "Shafts=8\n" +
                "Treadles=8\n" +
                "Rising Shed=true";
        assertEquals(weavingAsWIF, weaving.toString());

        // COLOR PALETTE
        Option<WIFColorPalette> colorPaletteOption = r.getColorPalette();
        assertTrue(colorPaletteOption.isDefined());
        WIFColorPalette colorPalette = colorPaletteOption.get();
        String colorPaletteAsWIF = "[COLOR PALETTE]\n" +
                "Entries=41\n" +
                "Range=0,65535";
        assertEquals(colorPaletteAsWIF, colorPalette.toString());

        // COLOR TABLE
        Option<WIFColorTable> colorTableOption = r.getColorTable();
        assertTrue(colorTableOption.isDefined());
        WIFColorTable colorTable = colorTableOption.get();
        String colorTableAsWIF = "[COLOR TABLE]\n" +
                "1=0,0,0\n" +
                "2=1,65535,19252\n" +
                "3=1,65535,21178\n" +
                "4=1,65535,21178\n" +
                "5=1,65535,23104\n" +
                "6=1,65535,25030\n" +
                "7=1,65535,26956\n" +
                "8=1,65535,26956\n" +
                "9=1,65535,28882\n" +
                "10=1,65535,30808\n" +
                "11=1,65535,32734\n" +
                "12=1,65535,32734\n" +
                "13=1,65535,34660\n" +
                "14=1,65535,36586\n" +
                "15=1,65535,38512\n" +
                "16=1,65535,38512\n" +
                "17=1,65535,40438\n" +
                "18=1,65535,42364\n" +
                "19=1,65535,44290\n" +
                "20=1,65535,44290\n" +
                "21=1,65535,46216\n" +
                "22=1,65535,48142\n" +
                "23=1,65535,50067\n" +
                "24=1,65535,50067\n" +
                "25=1,65535,51993\n" +
                "26=1,65535,53919\n" +
                "27=1,65535,55845\n" +
                "28=1,65535,55845\n" +
                "29=1,65535,57771\n" +
                "30=1,65535,59697\n" +
                "31=1,65535,65523\n" +
                "32=1,63621,65535\n" +
                "33=1,61695,65535\n" +
                "34=1,61695,65535\n" +
                "35=1,59769,65535\n" +
                "36=1,57843,65535\n" +
                "37=1,55917,65535\n" +
                "38=1,55917,65535\n" +
                "39=1,53991,65535\n" +
                "40=1,52065,65535\n" +
                "41=1,50139,65535";
        assertEquals(colorTableAsWIF.trim(), colorTable.toString());

        // WARP
        Option<WIFWarp> warpOption = r.getWarp();
        assertTrue(warpOption.isDefined());
        WIFWarp warp = warpOption.get();
        String warpAsWIF = "[WARP]\n" +
                "Threads=157\n" +
                "Color=3\n" +
                "Symbol=X\n" +
                "Units=Inches\n" +
                "Spacing=0.625\n" +
                "Thickness=0.5";
        assertEquals(warpAsWIF, warp.toString());

        // THREADING
        Option<WIFThreading> threadingOption = r.getThreading();
        assertTrue(threadingOption.isDefined());
        WIFThreading threading = threadingOption.get();
        String threadingAsWIF = "[THREADING]\n" +
                "1=1\n" +
                "2=2\n" +
                "3=3\n" +
                "4=4\n" +
                "5=5\n" +
                "6=6\n" +
                "7=7\n" +
                "8=8\n" +
                "9=1\n" +
                "10=2\n" +
                "11=3\n" +
                "12=4\n" +
                "13=5\n" +
                "14=6\n" +
                "15=7\n" +
                "16=8\n" +
                "17=1\n" +
                "18=2\n" +
                "19=3\n" +
                "20=4\n" +
                "21=5\n" +
                "22=6\n" +
                "23=7\n" +
                "24=8\n" +
                "25=1\n" +
                "26=2\n" +
                "27=3\n" +
                "28=4\n" +
                "29=5\n" +
                "30=6\n" +
                "31=7\n" +
                "32=8\n" +
                "33=1\n" +
                "34=2\n" +
                "35=3\n" +
                "36=4\n" +
                "37=5\n" +
                "38=6\n" +
                "39=7\n" +
                "40=8\n" +
                "41=7\n" +
                "42=6\n" +
                "43=5\n" +
                "44=4\n" +
                "45=3\n" +
                "46=2\n" +
                "47=1\n" +
                "48=8\n" +
                "49=7\n" +
                "50=6\n" +
                "51=5\n" +
                "52=4\n" +
                "53=3\n" +
                "54=2\n" +
                "55=1\n" +
                "56=8\n" +
                "57=7\n" +
                "58=6\n" +
                "59=5\n" +
                "60=4\n" +
                "61=3\n" +
                "62=2\n" +
                "63=1\n" +
                "64=8\n" +
                "65=7\n" +
                "66=6\n" +
                "67=5\n" +
                "68=4\n" +
                "69=3\n" +
                "70=2\n" +
                "71=1\n" +
                "72=8\n" +
                "73=7\n" +
                "74=6\n" +
                "75=5\n" +
                "76=4\n" +
                "77=3\n" +
                "78=2\n" +
                "79=1\n" +
                "80=2\n" +
                "81=3\n" +
                "82=4\n" +
                "83=5\n" +
                "84=6\n" +
                "85=7\n" +
                "86=8\n" +
                "87=1\n" +
                "88=2\n" +
                "89=3\n" +
                "90=4\n" +
                "91=5\n" +
                "92=6\n" +
                "93=7\n" +
                "94=8\n" +
                "95=1\n" +
                "96=2\n" +
                "97=3\n" +
                "98=4\n" +
                "99=5\n" +
                "100=6\n" +
                "101=7\n" +
                "102=8\n" +
                "103=1\n" +
                "104=2\n" +
                "105=3\n" +
                "106=4\n" +
                "107=5\n" +
                "108=6\n" +
                "109=7\n" +
                "110=8\n" +
                "111=1\n" +
                "112=2\n" +
                "113=3\n" +
                "114=4\n" +
                "115=5\n" +
                "116=6\n" +
                "117=7\n" +
                "118=8\n" +
                "119=7\n" +
                "120=6\n" +
                "121=5\n" +
                "122=4\n" +
                "123=3\n" +
                "124=2\n" +
                "125=1\n" +
                "126=8\n" +
                "127=7\n" +
                "128=6\n" +
                "129=5\n" +
                "130=4\n" +
                "131=3\n" +
                "132=2\n" +
                "133=1\n" +
                "134=8\n" +
                "135=7\n" +
                "136=6\n" +
                "137=5\n" +
                "138=4\n" +
                "139=3\n" +
                "140=2\n" +
                "141=1\n" +
                "142=8\n" +
                "143=7\n" +
                "144=6\n" +
                "145=5\n" +
                "146=4\n" +
                "147=3\n" +
                "148=2\n" +
                "149=1\n" +
                "150=8\n" +
                "151=7\n" +
                "152=6\n" +
                "153=5\n" +
                "154=4\n" +
                "155=3\n" +
                "156=2\n" +
                "157=1";
        assertEquals(threadingAsWIF, threading.toString());

        // WARP COLORS=true
        Option<WIFWarpColors> warpColorsOption = r.getWarpColors();
        assertTrue(warpColorsOption.isDefined());
        WIFWarpColors warpColors = warpColorsOption.get();
        String warpColorsAsWIF = "[WARP COLORS]\n" +
                "1=2\n" +
                "3=4\n" +
                "4=5\n" +
                "5=6\n" +
                "6=7\n" +
                "7=8\n" +
                "8=9\n" +
                "9=10\n" +
                "10=11\n" +
                "11=12\n" +
                "12=13\n" +
                "13=14\n" +
                "14=15\n" +
                "15=16\n" +
                "16=17\n" +
                "17=18\n" +
                "18=19\n" +
                "19=20\n" +
                "20=21\n" +
                "21=22\n" +
                "22=23\n" +
                "23=24\n" +
                "24=25\n" +
                "25=26\n" +
                "26=27\n" +
                "27=28\n" +
                "28=29\n" +
                "29=30\n" +
                "30=31\n" +
                "31=32\n" +
                "32=33\n" +
                "33=34\n" +
                "34=35\n" +
                "35=36\n" +
                "36=37\n" +
                "37=38\n" +
                "38=39\n" +
                "39=40\n" +
                "40=41\n" +
                "41=40\n" +
                "42=39\n" +
                "43=38\n" +
                "44=37\n" +
                "45=36\n" +
                "46=35\n" +
                "47=34\n" +
                "48=33\n" +
                "49=32\n" +
                "50=31\n" +
                "51=30\n" +
                "52=29\n" +
                "53=28\n" +
                "54=27\n" +
                "55=26\n" +
                "56=25\n" +
                "57=24\n" +
                "58=23\n" +
                "59=22\n" +
                "60=21\n" +
                "61=20\n" +
                "62=19\n" +
                "63=18\n" +
                "64=17\n" +
                "65=16\n" +
                "66=15\n" +
                "67=14\n" +
                "68=13\n" +
                "69=12\n" +
                "70=11\n" +
                "71=10\n" +
                "72=9\n" +
                "73=8\n" +
                "74=7\n" +
                "75=6\n" +
                "76=5\n" +
                "77=4\n" +
                "79=2\n" +
                "81=4\n" +
                "82=5\n" +
                "83=6\n" +
                "84=7\n" +
                "85=8\n" +
                "86=9\n" +
                "87=10\n" +
                "88=11\n" +
                "89=12\n" +
                "90=13\n" +
                "91=14\n" +
                "92=15\n" +
                "93=16\n" +
                "94=17\n" +
                "95=18\n" +
                "96=19\n" +
                "97=20\n" +
                "98=21\n" +
                "99=22\n" +
                "100=23\n" +
                "101=24\n" +
                "102=25\n" +
                "103=26\n" +
                "104=27\n" +
                "105=28\n" +
                "106=29\n" +
                "107=30\n" +
                "108=31\n" +
                "109=32\n" +
                "110=33\n" +
                "111=34\n" +
                "112=35\n" +
                "113=36\n" +
                "114=37\n" +
                "115=38\n" +
                "116=39\n" +
                "117=40\n" +
                "118=41\n" +
                "119=40\n" +
                "120=39\n" +
                "121=38\n" +
                "122=37\n" +
                "123=36\n" +
                "124=35\n" +
                "125=34\n" +
                "126=33\n" +
                "127=32\n" +
                "128=31\n" +
                "129=30\n" +
                "130=29\n" +
                "131=28\n" +
                "132=27\n" +
                "133=26\n" +
                "134=25\n" +
                "135=24\n" +
                "136=23\n" +
                "137=22\n" +
                "138=21\n" +
                "139=20\n" +
                "140=19\n" +
                "141=18\n" +
                "142=17\n" +
                "143=16\n" +
                "144=15\n" +
                "145=14\n" +
                "146=13\n" +
                "147=12\n" +
                "148=11\n" +
                "149=10\n" +
                "150=9\n" +
                "151=8\n" +
                "152=7\n" +
                "153=6\n" +
                "154=5\n" +
                "155=4\n" +
                "157=2";
        assertEquals(warpColorsAsWIF, warpColors.toString());

        // WEFT
        Option<WIFWeft> weftOption = r.getWeft();
        assertTrue(weftOption.isDefined());
        WIFWeft weft = weftOption.get();
        String weftAsWIF = "[WEFT]\n" +
                "Threads=80\n" +
                "Color=1\n" +
                "Symbol=X\n" +
                "Units=Inches\n" +
                "Spacing=0.25\n" +
                "Thickness=0.125";
        assertEquals(weftAsWIF, weft.toString());

        // TREADLING
        Option<WIFTreadling> treadlingOption = r.getTreadling();
        assertTrue(treadlingOption.isDefined());
        WIFTreadling treadling = treadlingOption.get();
        String treadlingAsWIF = "[TREADLING]\n" +
                "1=1\n" +
                "2=2\n" +
                "3=3\n" +
                "4=4\n" +
                "5=5\n" +
                "6=6\n" +
                "7=7\n" +
                "8=8\n" +
                "9=1\n" +
                "10=2\n" +
                "11=3\n" +
                "12=4\n" +
                "13=5\n" +
                "14=6\n" +
                "15=7\n" +
                "16=8\n" +
                "17=1\n" +
                "18=2\n" +
                "19=3\n" +
                "20=4\n" +
                "21=5\n" +
                "22=6\n" +
                "23=7\n" +
                "24=8\n" +
                "25=1\n" +
                "26=2\n" +
                "27=3\n" +
                "28=4\n" +
                "29=5\n" +
                "30=6\n" +
                "31=7\n" +
                "32=8\n" +
                "33=1\n" +
                "34=2\n" +
                "35=3\n" +
                "36=4\n" +
                "37=5\n" +
                "38=6\n" +
                "39=7\n" +
                "40=8\n" +
                "41=1\n" +
                "42=2\n" +
                "43=3\n" +
                "44=4\n" +
                "45=5\n" +
                "46=6\n" +
                "47=7\n" +
                "48=8\n" +
                "49=1\n" +
                "50=2\n" +
                "51=3\n" +
                "52=4\n" +
                "53=5\n" +
                "54=6\n" +
                "55=7\n" +
                "56=8\n" +
                "57=1\n" +
                "58=2\n" +
                "59=3\n" +
                "60=4\n" +
                "61=5\n" +
                "62=6\n" +
                "63=7\n" +
                "64=8\n" +
                "65=1\n" +
                "66=2\n" +
                "67=3\n" +
                "68=4\n" +
                "69=5\n" +
                "70=6\n" +
                "71=7\n" +
                "72=8\n" +
                "73=1\n" +
                "74=2\n" +
                "75=3\n" +
                "76=4\n" +
                "77=5\n" +
                "78=6\n" +
                "79=7\n" +
                "80=8";
        assertEquals(treadlingAsWIF, treadling.toString());

        // TIEUP
        Option<WIFTieup> tieupOption = r.getTieup();
        assertTrue(tieupOption.isDefined());
        String tieupAsWIF = "[TIEUP]\n" +
                "1=1,4,7,8\n" +
                "2=1,2,8\n" +
                "3=1,2,3\n" +
                "4=2,3,4\n" +
                "5=3,4,5,8\n" +
                "6=1,4,6\n" +
                "7=2,5,7\n" +
                "8=3,6,8";
        WIFTieup tieup = tieupOption.get();
        assertEquals(tieupAsWIF, tieup.toString());
        r.close();

    }

    @Test
    public void writeFromBuilder2() throws Exception {
        WIFBuilder wifBuilder = new WIFBuilder();

        // HEADER
        WIFHeader.Builder header = new WIFHeader.Builder()
                .version("1.1")
                .developer("wif@mhsoft.com")
                .date("April 20, 1997")
                .sourceProgram("SwiftWeave")
                .sourceVersion("5.14");
        wifBuilder.header(header);

        // NOTES
        WIFNotes.Builder notes = new WIFNotes.Builder().notes("Comment:");
        wifBuilder.notes(notes);

        // WEAVING
        WIFWeaving.Builder weaving = new WIFWeaving.Builder(8, 8).risingShed(true);
        wifBuilder.weaving(weaving);

        // WEFT
        WIFWeft.Builder weft = new WIFWeft.Builder(80)
                .spacing(0.25)
                .thickness(0.125)
                .units(WIFUnit.INCHES)
                .symbol("X")
                .colorPaletteIndex(1);
        wifBuilder.weft(weft);

        // WARP
        WIFWarp.Builder warp = new WIFWarp.Builder(157)
                .colorPaletteIndex(3)
                .units(WIFUnit.INCHES)
                .symbol("X")
                .thickness(0.5)
                .spacing(0.625);
        wifBuilder.warp(warp);


        //COLOR PALETTE
        WIFColorPalette.Builder palette = new WIFColorPalette.Builder(41, new Pair<>(0, 65535));
        wifBuilder.colorPalette(palette);

        //COLOR TABLE
        WIFColorTable.Builder table = test41Colors(new WIFColorTable.Builder());
        wifBuilder.colorTable(table);

        // THREADING
        WIFThreading.Builder threading = new WIFThreading.Builder();
        for (int i = 1; i <= warp.build().getThreads(); i++) {
            if (i <= 40 || i > 80 && i < 118) {
                threading.threading(i, (i % 8) + 1);
            } else {
                threading.threading(i, 8 - (i % 8));
            }
        }
        wifBuilder.threading(threading);

        //TREADLING
        WIFTreadling.Builder treadling = new WIFTreadling.Builder();
        for (int i = 1; i <= weft.build().getThreads(); i++) {
            treadling.treadling(i, (i % 8) + 1);
        }
        wifBuilder.treadling(treadling);

        // TIEUP
        WIFTieup.Builder tieUp = new WIFTieup.Builder();
        tieUp.tieup(1, 1, 4, 7, 8)
                .tieup(2, 1, 2, 8)
                .tieup(3, 1, 2, 3)
                .tieup(4, 2, 3, 4)
                .tieup(5, 3, 4, 6, 8)
                .tieup(6, 1, 4, 8)
                .tieup(7, 2, 5, 7)
                .tieup(8, 3, 6, 8);
        wifBuilder.tieup(tieUp);

        WIFReader build = wifBuilder.build();
        assertNotNull(build);


    }

    private WIFColorTable.Builder test41Colors(WIFColorTable.Builder b) {
        b.color(1, new WIFColorTripplet(0, 0, 0))
                .color(2, new WIFColorTripplet(1, 65535, 19252))
                .color(3, new WIFColorTripplet(1, 65535, 21178))
                .color(4, new WIFColorTripplet(1, 65535, 21178))
                .color(5, new WIFColorTripplet(1, 65535, 23104))
                .color(6, new WIFColorTripplet(1, 65535, 25030))
                .color(7, new WIFColorTripplet(1, 65535, 26956))
                .color(8, new WIFColorTripplet(1, 65535, 26956))
                .color(9, new WIFColorTripplet(1, 65535, 28882))
                .color(10, new WIFColorTripplet(1, 65535, 30808))
                .color(11, new WIFColorTripplet(1, 65535, 32734))
                .color(12, new WIFColorTripplet(1, 65535, 32734))
                .color(13, new WIFColorTripplet(1, 65535, 34660))
                .color(14, new WIFColorTripplet(1, 65535, 36586))
                .color(15, new WIFColorTripplet(1, 65535, 38512))
                .color(16, new WIFColorTripplet(1, 65535, 38512))
                .color(17, new WIFColorTripplet(1, 65535, 40438))
                .color(18, new WIFColorTripplet(1, 65535, 42364))
                .color(19, new WIFColorTripplet(1, 65535, 44290))
                .color(20, new WIFColorTripplet(1, 65535, 44290))
                .color(21, new WIFColorTripplet(1, 65535, 46216))
                .color(22, new WIFColorTripplet(1, 65535, 48142))
                .color(23, new WIFColorTripplet(1, 65535, 50067))
                .color(24, new WIFColorTripplet(1, 65535, 50067))
                .color(25, new WIFColorTripplet(1, 65535, 51993))
                .color(26, new WIFColorTripplet(1, 65535, 53919))
                .color(27, new WIFColorTripplet(1, 65535, 55845))
                .color(28, new WIFColorTripplet(1, 65535, 55845))
                .color(29, new WIFColorTripplet(1, 65535, 57771))
                .color(30, new WIFColorTripplet(1, 65535, 59697))
                .color(31, new WIFColorTripplet(1, 65535, 65523))
                .color(32, new WIFColorTripplet(1, 63621, 65535))
                .color(33, new WIFColorTripplet(1, 61695, 65535))
                .color(34, new WIFColorTripplet(1, 61695, 65535))
                .color(35, new WIFColorTripplet(1, 59769, 65535))
                .color(36, new WIFColorTripplet(1, 57843, 65535))
                .color(37, new WIFColorTripplet(1, 55917, 65535))
                .color(38, new WIFColorTripplet(1, 55917, 65535))
                .color(39, new WIFColorTripplet(1, 53991, 65535))
                .color(40, new WIFColorTripplet(1, 52065, 65535))
                .color(41, new WIFColorTripplet(1, 50139, 65535));
        return b;
    }

    private WIFColorTable.Builder test182Colors(WIFColorTable.Builder b) {
        b.color(1, new WIFColorTripplet(999, 999, 999))
                .color(2, new WIFColorTripplet(0, 0, 0))
                .color(3, new WIFColorTripplet(121, 0, 607))
                .color(4, new WIFColorTripplet(0, 266, 999))
                .color(5, new WIFColorTripplet(0, 999, 999))
                .color(6, new WIFColorTripplet(0, 587, 0))
                .color(7, new WIFColorTripplet(466, 999, 0))
                .color(8, new WIFColorTripplet(999, 999, 0))
                .color(9, new WIFColorTripplet(999, 532, 0))
                .color(10, new WIFColorTripplet(999, 0, 0))
                .color(11, new WIFColorTripplet(999, 0, 599))
                .color(12, new WIFColorTripplet(799, 0, 999))
                .color(13, new WIFColorTripplet(481, 305, 767))
                .color(14, new WIFColorTripplet(399, 607, 999))
                .color(15, new WIFColorTripplet(399, 999, 999))
                .color(16, new WIFColorTripplet(399, 999, 399))
                .color(17, new WIFColorTripplet(787, 999, 399))
                .color(18, new WIFColorTripplet(999, 999, 399))
                .color(19, new WIFColorTripplet(999, 677, 399))
                .color(20, new WIFColorTripplet(999, 399, 399))
                .color(21, new WIFColorTripplet(999, 399, 760))
                .color(22, new WIFColorTripplet(877, 399, 999))
                .color(23, new WIFColorTripplet(121, 0, 599))
                .color(24, new WIFColorTripplet(0, 160, 599))
                .color(25, new WIFColorTripplet(0, 599, 599))
                .color(26, new WIFColorTripplet(0, 599, 0))
                .color(27, new WIFColorTripplet(278, 599, 0))
                .color(28, new WIFColorTripplet(599, 599, 0))
                .color(29, new WIFColorTripplet(599, 321, 0))
                .color(30, new WIFColorTripplet(599, 0, 0))
                .color(31, new WIFColorTripplet(599, 0, 360))
                .color(32, new WIFColorTripplet(477, 0, 599))
                .color(33, new WIFColorTripplet(368, 266, 799))
                .color(34, new WIFColorTripplet(266, 399, 799))
                .color(35, new WIFColorTripplet(266, 799, 799))
                .color(36, new WIFColorTripplet(266, 799, 266))
                .color(37, new WIFColorTripplet(599, 799, 266))
                .color(38, new WIFColorTripplet(799, 799, 266))
                .color(39, new WIFColorTripplet(799, 532, 266))
                .color(40, new WIFColorTripplet(799, 266, 266))
                .color(41, new WIFColorTripplet(799, 266, 599))
                .color(42, new WIFColorTripplet(666, 266, 799))
                .color(43, new WIFColorTripplet(148, 0, 799))
                .color(44, new WIFColorTripplet(0, 199, 799))
                .color(45, new WIFColorTripplet(0, 799, 799))
                .color(46, new WIFColorTripplet(0, 799, 0))
                .color(47, new WIFColorTripplet(348, 799, 0))
                .color(48, new WIFColorTripplet(799, 799, 0))
                .color(49, new WIFColorTripplet(799, 399, 0))
                .color(50, new WIFColorTripplet(799, 0, 0))
                .color(51, new WIFColorTripplet(799, 0, 450))
                .color(52, new WIFColorTripplet(599, 0, 799))
                .color(53, new WIFColorTripplet(658, 532, 999))
                .color(54, new WIFColorTripplet(532, 666, 999))
                .color(55, new WIFColorTripplet(532, 999, 999))
                .color(56, new WIFColorTripplet(532, 999, 532))
                .color(57, new WIFColorTripplet(901, 999, 532))
                .color(58, new WIFColorTripplet(999, 999, 532))
                .color(59, new WIFColorTripplet(999, 697, 532))
                .color(60, new WIFColorTripplet(999, 532, 532))
                .color(61, new WIFColorTripplet(999, 532, 799))
                .color(62, new WIFColorTripplet(940, 532, 999))
                .color(63, new WIFColorTripplet(195, 133, 932))
                .color(64, new WIFColorTripplet(133, 266, 932))
                .color(65, new WIFColorTripplet(133, 932, 932))
                .color(66, new WIFColorTripplet(133, 932, 133))
                .color(67, new WIFColorTripplet(278, 932, 133))
                .color(68, new WIFColorTripplet(932, 932, 133))
                .color(69, new WIFColorTripplet(932, 321, 133))
                .color(70, new WIFColorTripplet(932, 133, 133))
                .color(71, new WIFColorTripplet(932, 133, 360))
                .color(72, new WIFColorTripplet(477, 133, 932))
                .color(73, new WIFColorTripplet(501, 399, 932))
                .color(74, new WIFColorTripplet(399, 532, 932))
                .color(75, new WIFColorTripplet(399, 932, 932))
                .color(76, new WIFColorTripplet(399, 932, 399))
                .color(77, new WIFColorTripplet(732, 932, 399))
                .color(78, new WIFColorTripplet(932, 932, 399))
                .color(79, new WIFColorTripplet(932, 666, 399))
                .color(80, new WIFColorTripplet(932, 399, 399))
                .color(81, new WIFColorTripplet(932, 399, 732))
                .color(82, new WIFColorTripplet(799, 399, 932));
        return b;
    }

    private InputStream getInputStream(String file) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        return classloader.getResourceAsStream(file);
    }

}
