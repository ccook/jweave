/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave;

import com.gitlab.ccook.jweave.simulate.DrawDownSimulator;
import com.gitlab.ccook.jweave.simulate.LoomSimulator;
import com.gitlab.ccook.jweave.wif.WIFReader;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.net.URL;

public class LoomSimulatorTest {


    private static File getTestImagesDirectory() {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        URL resource = classloader.getResource("test1.png");
        File f = new File(resource.getPath()).getParentFile();
        return f;
    }

    @Test
    public void test1Simulation() throws Exception {
        File test1Directory = new File(getTestImagesDirectory(), "test1");
        test1Directory.mkdirs();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("test1.wif");
        WIFReader r = new WIFReader(is);
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(test1Directory, "test1.png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(test1Directory, "test1_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(test1Directory, "test1_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(test1Directory, "test1_tieup.png"));

        sim.optimize(r);

        width = 3508;
        height = 2480;
        bufferedImage = sim.getDrawDown().makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(test1Directory, "test1_optimized.png"));

        width = 3508;
        height = 500;
        im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(test1Directory, "test1_threading_optimized.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(test1Directory, "test1_treadling_optimized.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(test1Directory, "test1_tieup_optimized.png"));
    }

    @Test
    public void test2Simulation() throws Exception {
        File testDirectory = new File(getTestImagesDirectory(), "test2");
        testDirectory.mkdirs();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("test2.wif");
        WIFReader r = new WIFReader(is);
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, "test2.png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test2_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test2_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test2_tieup.png"));
    }

    @Test
    public void test3Simulation() throws Exception {
        File testDirectory = new File(getTestImagesDirectory(), "test3");
        testDirectory.mkdirs();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("test3.wif");
        WIFReader r = new WIFReader(is);
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, "test3.png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test3_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test3_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test3_tieup.png"));
    }

    @Test
    public void test4Simulation() throws Exception {
        File testDirectory = new File(getTestImagesDirectory(), "test4");
        testDirectory.mkdirs();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("test4.wif");
        WIFReader r = new WIFReader(is);
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, "test4.png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test4_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test4_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test4_tieup.png"));
    }

    @Test
    public void test5Simulation() throws Exception {
        File testDirectory = new File(getTestImagesDirectory(), "test5");
        testDirectory.mkdirs();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("test5.wif");
        WIFReader r = new WIFReader(is);
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, "test5.png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test5_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test5_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test5_tieup.png"));
    }

    @Test
    public void test6Simulation() throws Exception {
        File testDirectory = new File(getTestImagesDirectory(), "test6");
        testDirectory.mkdirs();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("test6.wif");
        WIFReader r = new WIFReader(is);
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, "test6.png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test6_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test6_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test6_tieup.png"));
    }

    @Test
    public void test7Simulation() throws Exception {
        File testDirectory = new File(getTestImagesDirectory(), "test7");
        testDirectory.mkdirs();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("test7.wif");
        WIFReader r = new WIFReader(is);
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, "test7.png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test7_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test7_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test7_tieup.png"));
    }

    @Test
    public void test8Simulation() throws Exception {
        File testDirectory = new File(getTestImagesDirectory(), "test8");
        testDirectory.mkdirs();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("test8.wif");
        WIFReader r = new WIFReader(is);
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, "test8.png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test8_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test8_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test8_tieup.png"));
    }

    @Test
    public void test9Simulation() throws Exception {
        File testDirectory = new File(getTestImagesDirectory(), "test9");
        testDirectory.mkdirs();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("test9.wif");
        WIFReader r = new WIFReader(is);
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, "test9.png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test9_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test9_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test9_tieup.png"));
    }

    @Test
    public void test10Simulation() throws Exception {
        File testDirectory = new File(getTestImagesDirectory(), "test10");
        testDirectory.mkdirs();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("test10.wif");
        WIFReader r = new WIFReader(is);
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, "test10.png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test10_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test10_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, "test10_tieup.png"));
    }
}
