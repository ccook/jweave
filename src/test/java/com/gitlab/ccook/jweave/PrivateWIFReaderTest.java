/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave;

import com.gitlab.ccook.jweave.simulate.DrawDownSimulator;
import com.gitlab.ccook.jweave.simulate.LoomSimulator;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.util.SettCalculator;
import com.gitlab.ccook.jweave.wif.WIFReader;
import org.junit.Before;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;

public class PrivateWIFReaderTest {

    private File privateWIFDir = new File("/home/cam/wifs/");
    private File privateTestDirectory = new File(privateWIFDir, "/test/");

    @Before
    public void before() throws Exception {
        if (privateWIFDir.exists()) {
            privateTestDirectory.mkdirs();
        }
    }

    @Test
    public void privateTest1() throws Exception {
        if (privateWIFDir.exists()) {
            File privateWIF1 = new File(privateWIFDir, "Williams_JF20_Twill.wif");
            if (privateWIF1.exists()) {
                File testDirectory = new File(privateTestDirectory, privateWIF1.getName() + "_results/");
                testDirectory.mkdirs();
                writeResults(privateWIF1, testDirectory);
            }
        }
    }

    @Test
    public void krokbragTest() throws Exception {
        if (privateWIFDir.exists()) {

            Scanner in = new Scanner(new File("/home/cam/Documents/krokbragd_xs.csv"));
            int lineNum = 0;
            Map<Integer, String> map = new TreeMap<>();
            while (in.hasNextLine()) {
                String line = in.nextLine();
                lineNum++;
                map.put(lineNum, line);
            }
            in.close();
            for (Map.Entry<Integer, String> ent : map.entrySet()) {
                String[] parts = ent.getValue().split(",");
                //System.out.println(ent.getKey());
                List<Pair<Integer, Integer>> colorX = new ArrayList<>();
                int counter = 0;
                for (int i = 0; i < parts.length - 1; i++) {
                    String colorAtI = parts[i];
                    String nextColor = parts[i + 1];
                    if (colorAtI.equalsIgnoreCase(nextColor)) {
                        counter++;
                    } else {

                    }
                }
            }
        }
    }

    @Test
    public void settTest() throws Exception {
        int yardsPerPound = 3300;
        File privateWIF1 = new File(privateWIFDir, "Earthly_ND19_plain_weave.wif");
        if (privateWIF1.exists()) {
            WIFReader r = new WIFReader(new FileInputStream(privateWIF1));
            int epi = SettCalculator.calculateEPI(r, yardsPerPound);
            System.out.println("Perfect EPI:" + epi);
        }

    }

    private void writeResults(File privateWIF, File testDirectory) throws Exception {
        WIFReader r = new WIFReader(new FileInputStream(privateWIF));
        LoomSimulator sim = new LoomSimulator(r);
        DrawDownSimulator drawDown = sim.getDrawDown();
        //2480 pixels x 3508 pixels
        int width = 3508;
        int height = 2480;
        BufferedImage bufferedImage = drawDown.makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, privateWIF.getName() + ".png"));

        width = 3508;
        height = 500;
        BufferedImage im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, privateWIF.getName() + "_threading.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, privateWIF.getName() + "_treadling.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, privateWIF.getName() + "_tieup.png"));
        sim.optimize(r);
        System.out.println(r.getLiftPlan());

        width = 3508;
        height = 2480;
        bufferedImage = sim.getDrawDown().makeImage(width, height);
        ImageIO.write(bufferedImage, "png", new File(testDirectory, privateWIF.getName() + "_optimized.png"));

        width = 3508;
        height = 500;
        im = sim.getThreading().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, privateWIF.getName() + "_threading_optimized.png"));

        width = 200;
        height = 2480;

        im = sim.getTreadling().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, privateWIF.getName() + "_treadling_optimized.png"));

        width = 500;
        height = 500;
        im = sim.getTieUp().makeImage(width, height);
        ImageIO.write(im, "png", new File(testDirectory, privateWIF.getName() + "test1_tieup_optimized.png"));

    }
}
