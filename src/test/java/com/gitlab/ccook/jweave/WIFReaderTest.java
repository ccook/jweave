/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.gitlab.ccook.jweave;

import com.gitlab.ccook.jweave.util.Option;
import com.gitlab.ccook.jweave.util.Pair;
import com.gitlab.ccook.jweave.wif.WIFReader;
import com.gitlab.ccook.jweave.wif.model.*;
import com.gitlab.ccook.jweave.wif.model.helper.WIFColorTripplet;
import com.gitlab.ccook.jweave.wif.model.helper.WIFUnit;
import com.gitlab.ccook.jweave.wif.model.weft.*;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarp;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarpColors;
import com.gitlab.ccook.jweave.wif.model.warp.WIFWarpSpacing;
import com.gitlab.ccook.jweave.wif.model.warp.WIFThreading;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

import static junit.framework.TestCase.*;

public class WIFReaderTest {

    @Test
    public void test1Load() throws Exception {
        InputStream is = getInputStream("test1.wif");
        WIFReader r = new WIFReader(is);

        // HEADER
        WIFHeader header = r.getHeader();
        WIFHeader compareHeader = new WIFHeader.Builder()
                .version("1.1")
                .developer("wif@mhsoft.com")
                .date("April 20, 1997")
                .sourceProgram("Fiberworks PCW")
                .sourceVersion("4.2")
                .build();
        assertEquals("WIFHeader did not match what was expected", compareHeader, header);


        // CONTENTS
        WIFContents contents = r.getContents();
        WIFContents compareContents = new WIFContents.Builder()
                .contents(WIFContents.WIFContentsKey.THREADING, true)
                .contents(WIFContents.WIFContentsKey.WEFT, true)
                .contents(WIFContents.WIFContentsKey.TIEUP, true)
                .contents(WIFContents.WIFContentsKey.WARP_SPACING, true)
                .contents(WIFContents.WIFContentsKey.TEXT, true)
                .contents(WIFContents.WIFContentsKey.COLOR_TABLE, true)
                .contents(WIFContents.WIFContentsKey.TREADLING, true)
                .contents(WIFContents.WIFContentsKey.WEAVING, true)
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, true)
                .contents(WIFContents.WIFContentsKey.WARP_COLORS, true)
                .contents(WIFContents.WIFContentsKey.WEFT_COLORS, true)
                .contents(WIFContents.WIFContentsKey.WARP, true)
                .contents(WIFContents.WIFContentsKey.WEFT_SPACING, true).build();
        assertEquals("WIFContents did not match what was expected", compareContents, contents);

        // COLOR PALETTE
        Option<WIFColorPalette> colorPaletteOption = r.getColorPalette();
        assertTrue("Color Palette expected but not found", colorPaletteOption.isDefined());
        WIFColorPalette colorPalette = colorPaletteOption.get();
        WIFColorPalette compareColorPalette = new WIFColorPalette.Builder(82, new Pair<>(0, 999)).build();
        assertEquals(colorPalette, compareColorPalette);

        // TEXT
        Option<WIFText> textOption = r.getText();
        assertTrue(textOption.isDefined());
        WIFText text = textOption.get();
        WIFText compareText = new WIFText.Builder()
                .title("#1 Shadow weave thick and thin based on honeysuckle long runsl").build();
        assertEquals(text, compareText);

        // WEAVING
        Option<WIFWeaving> weavingOption = r.getWeaving();
        assertTrue(weavingOption.isDefined());
        WIFWeaving weaving = weavingOption.get();
        WIFWeaving compareWeaving = new WIFWeaving.Builder(8, 8).risingShed(true).build();
        assertEquals(weaving, compareWeaving);

        // WARP
        Option<WIFWarp> warpOption = r.getWarp();
        assertTrue(warpOption.isDefined());
        WIFWarp warp = warpOption.get();
        WIFWarp compareWarp = new WIFWarp.Builder(459)
                .units(WIFUnit.CENTIMETERS)
                .colorPaletteIndex(3)
                .spacing(0.212)
                .thickness(0.212).build();
        int unoptimizedThreads = warp.getThreads();
        assertEquals(warp, compareWarp);


        // WEFT
        Option<WIFWeft> weftOption = r.getWeft();
        assertTrue(weftOption.isDefined());
        WIFWeft weft = weftOption.get();
        WIFWeft compareWeft = new WIFWeft.Builder(552)
                .colorPaletteIndex(6)
                .units(WIFUnit.CENTIMETERS)
                .spacing(0.212)
                .thickness(0.212).build();
        assertEquals(weft, compareWeft);
        int unOptimizedPicks = weft.getThreads();

        // WEFT
        weft.optimize(r);
        compareWeft.optimize(r);
        assertEquals(weft, compareWeft);
        int picks = weft.getThreads();
        assertEquals(268, picks);

        // TIEUP
        Option<WIFTieup> tieupOption = r.getTieup();
        assertTrue(tieupOption.isDefined());
        WIFTieup tieup = tieupOption.get();
        WIFTieup compareTieUp = new WIFTieup.Builder()
                .tieup(1, 1, 2, 3, 4)
                .tieup(2, 2, 3, 4, 5)
                .tieup(3, 3, 4, 5, 6)
                .tieup(4, 4, 5, 6, 7)
                .tieup(5, 5, 6, 7, 8)
                .tieup(6, 1, 6, 7, 8)
                .tieup(7, 1, 2, 7, 8)
                .tieup(8, 1, 2, 3, 8).build();
        assertEquals(tieup, compareTieUp);

        // COLOR TABLE
        Option<WIFColorTable> colorTableOption = r.getColorTable();
        assertTrue(colorTableOption.isDefined());
        WIFColorTable colorTable = colorTableOption.get();
        WIFColorTable compareColorTable = test182Colors(new WIFColorTable.Builder().palette(colorPaletteOption.get())).build();
        assertEquals(colorTable, compareColorTable);
        int numOfColors = colorTable.getLookupIndices().size();
        assertEquals(82, numOfColors);

        // THREADING
        Option<WIFThreading> threadingOption = r.getThreading();
        assertTrue(threadingOption.isDefined());
        WIFThreading threading = threadingOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // WARP SPACING
        Option<WIFWarpSpacing> warpSpacingOption = r.getWarpSpacing();
        assertTrue(warpSpacingOption.isDefined());
        WIFWarpSpacing warpSpacing = warpSpacingOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            assertTrue(warpSpacing.getSpacing(i).isDefined());
        }

        // WARP COLORS
        Option<WIFWarpColors> warpColorsOption = r.getWarpColors();
        assertTrue(warpColorsOption.isDefined());
        WIFWarpColors warpColors = warpColorsOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            assertTrue(warpColors.getWarpColor(i).isDefined());
        }

        // TREADLING
        Option<WIFTreadling> treadlingOption = r.getTreadling();
        assertTrue(treadlingOption.isDefined());
        WIFTreadling treadling = treadlingOption.get();
        for (int i = 1; i <= unOptimizedPicks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }


        // WEFT SPACING
        Option<WIFWeftSpacing> weftSpacingOption = r.getWeftSpacing();
        assertTrue(weftSpacingOption.isDefined());
        WIFWeftSpacing weftSpacing = weftSpacingOption.get();
        for (int i = 1; i <= unOptimizedPicks; i += 1) {
            assertTrue(weftSpacing.getSpacing(i).isDefined());
        }


        // WEFT COLORS
        Option<WIFWeftColors> weftColorsOption = r.getWeftColors();
        assertTrue(weftColorsOption.isDefined());
        WIFWeftColors weftColors = weftColorsOption.get();
        for (int i = 1; i <= unOptimizedPicks; i += 2) {
            assertTrue(weftColors.getWeftColor(i).isDefined());
        }

        // CONTENTS (optimized)
        contents.optimize(r);
        compareContents.optimize(r);
        assertEquals("Optimized WIFContents did not match what was expected", compareContents, contents);

        // COLOR PALETTE (optimized)
        colorPalette.optimize(r);
        compareColorPalette.optimize(r);
        assertEquals(colorPalette, compareColorPalette);
        assertEquals(colorPalette.entries, 4);

        // WEAVING (optimized)
        weaving.optimize(r);
        compareWeaving.optimize(r);
        assertEquals(weaving, compareWeaving);

        // WARP (optimized)
        warp.optimize(r);
        compareWarp.optimize(r);
        assertEquals(warp, compareWarp);
        int threads = warp.getThreads();


        // COLOR TABLE (optimized)
        colorTable.optimize(r);
        compareColorTable.optimize(r);
        assertEquals(colorTable, compareColorTable);


        // THREADING (optimized)
        threading.optimize(r);
        for (int i = 1; i <= threads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // WARP SPACING (optimized)
        warpSpacing.optimize(r);
        for (int i = 1; i <= threads; i++) {
            assertTrue(warpSpacing.getSpacing(i).isDefined());
        }

        // WARP COLORS (optimized)
        warpColors.optimize(r);
        for (int i = 1; i <= threads; i++) {
            assertTrue(warpColors.getWarpColor(i).isDefined());
        }

        // TREADLING (optimized)
        treadling.optimize(r);
        for (int i = 1; i <= picks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }

        // WEFT SPACING (optimized)
        weftSpacing.optimize(r);
        for (int i = 1; i <= picks; i += 1) {
            assertTrue(weftSpacing.getSpacing(i).isDefined());
        }

        // WEFT COLORS (optimized)
        weftColors.optimize(r);
        for (int i = 1; i <= picks; i += 2) {
            assertTrue(weftColors.getWeftColor(i).isDefined());
        }

        // LIFTPLAN (optimized)
        r.optimize();
        Option<WIFLiftPlan> liftPlanOption = r.getLiftPlan();
        assertTrue(liftPlanOption.isDefined());
        WIFLiftPlan plan = liftPlanOption.get();
        /*        int picks = weft.getThreads();*/
        for (int i = 1; i <= picks; i++) {
            List<Integer> shaftsForPick = plan.getShaftsForPick(i);
            assertTrue(!shaftsForPick.isEmpty());
        }
        r.close();
        is.close();
    }

    @Test
    public void test2Load() throws Exception {
        InputStream is = getInputStream("test2.wif");
        WIFReader r = new WIFReader(is);

        // HEADER
        WIFHeader header = r.getHeader();
        WIFHeader compareHeader = new WIFHeader.Builder()
                .version("1.1")
                .developer("wif@mhsoft.com")
                .date("April 20, 1997")
                .sourceProgram("SwiftWeave")
                .sourceVersion("5.14")
                .build();
        assertEquals("WIFHeader did not match what was expected", compareHeader, header);


        // CONTENTS
        WIFContents contents = r.getContents();
        WIFContents compareContents = new WIFContents.Builder()
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, true)
                .contents(WIFContents.WIFContentsKey.WEAVING, true)
                .contents(WIFContents.WIFContentsKey.WARP, true)
                .contents(WIFContents.WIFContentsKey.WEFT, true)
                .contents(WIFContents.WIFContentsKey.NOTES, true)
                .contents(WIFContents.WIFContentsKey.TIEUP, true)
                .contents(WIFContents.WIFContentsKey.COLOR_TABLE, true)
                .contents(WIFContents.WIFContentsKey.THREADING, true)
                .contents(WIFContents.WIFContentsKey.WARP_THICKNESS, false)
                .contents(WIFContents.WIFContentsKey.WARP_SPACING, false)
                .contents(WIFContents.WIFContentsKey.WARP_COLORS, true)
                .contents(WIFContents.WIFContentsKey.TREADLING, true)
                .contents(WIFContents.WIFContentsKey.LIFTPLAN, false)
                .contents(WIFContents.WIFContentsKey.WEFT_THICKNESS, false)
                .contents(WIFContents.WIFContentsKey.WEFT_SPACING, false)
                .build();
        assertEquals("WIFContents did not match what was expected", compareContents, contents);

        // COLOR PALETTE
        Option<WIFColorPalette> colorPaletteOption = r.getColorPalette();
        assertTrue("Color Palette expected but not found", colorPaletteOption.isDefined());
        WIFColorPalette colorPalette = colorPaletteOption.get();
        WIFColorPalette compareColorPalette = new WIFColorPalette.Builder(41, new Pair<>(0, 65535)).build();
        assertEquals(colorPalette, compareColorPalette);


        // WEAVING
        Option<WIFWeaving> weavingOption = r.getWeaving();
        assertTrue(weavingOption.isDefined());
        WIFWeaving weaving = weavingOption.get();
        WIFWeaving compareWeaving = new WIFWeaving.Builder(8, 8).risingShed(true).build();
        assertEquals(weaving, compareWeaving);

        // WARP
        Option<WIFWarp> warpOption = r.getWarp();
        assertTrue(warpOption.isDefined());
        WIFWarp warp = warpOption.get();
        WIFWarp compareWarp = new WIFWarp.Builder(157)
                .colorPaletteIndex(3)
                .symbol("X")
                .units(WIFUnit.INCHES)
                .spacing(0.625)
                .thickness(0.5).build();
        int unoptimizedThreads = warp.getThreads();
        assertEquals(warp, compareWarp);

        // WEFT
        Option<WIFWeft> weftOption = r.getWeft();
        assertTrue(weftOption.isDefined());
        WIFWeft weft = weftOption.get();
        WIFWeft compareWeft = new WIFWeft.Builder(80)
                .colorPaletteIndex(1)
                .symbol("X")
                .units(WIFUnit.INCHES)
                .spacing(0.25)
                .thickness(0.125).build();
        int unoptimizedPicks = weft.getThreads();
        assertEquals(weft, compareWeft);


        // NOTES
        Option<WIFNotes> notesOption = r.getNotes();
        assertTrue(notesOption.isDefined());
        WIFNotes notes = notesOption.get();
        assertEquals("Comment:", notes.getNotes().trim());

        // TIEUP
        Option<WIFTieup> tieupOption = r.getTieup();
        assertTrue(tieupOption.isDefined());
        WIFTieup tieup = tieupOption.get();
        WIFTieup compareTieup = new WIFTieup.Builder()
                .tieup(1, 1, 4, 7, 8)
                .tieup(2, 1, 2, 8)
                .tieup(3, 1, 2, 3)
                .tieup(4, 2, 3, 4)
                .tieup(5, 3, 4, 5, 8)
                .tieup(6, 1, 4, 6)
                .tieup(7, 2, 5, 7)
                .tieup(8, 3, 6, 8).build();
        assertEquals(tieup, compareTieup);

        // COLOR TABLE
        Option<WIFColorTable> colorTableOption = r.getColorTable();
        assertTrue(colorTableOption.isDefined());
        WIFColorTable colorTable = colorTableOption.get();
        WIFColorTable compareColorTable = test241Colors(new WIFColorTable.Builder()
                .palette(colorPalette))
                .build();
        assertEquals(colorTable, compareColorTable);
        int numOfColors = colorTable.getLookupIndices().size();
        assertEquals(41, numOfColors);


        // TREADLING
        Option<WIFTreadling> treadlingOption = r.getTreadling();
        assertTrue(treadlingOption.isDefined());
        WIFTreadling treadling = treadlingOption.get();
        for (int i = 1; i <= unoptimizedPicks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }

        // WARP COLORS
        Option<WIFWarpColors> warpColorsOption = r.getWarpColors();
        assertTrue(warpColorsOption.isDefined());
        WIFWarpColors warpColors = warpColorsOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            assertTrue(warpColors.getWarpColor(i).isDefined());
        }

        // THREADING
        Option<WIFThreading> threadingOption = r.getThreading();
        assertTrue(threadingOption.isDefined());
        WIFThreading threading = threadingOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }


        // CONTENTS (optimized)
        contents.optimize(r);
        compareContents.optimize(r);
        assertEquals("Optimized WIFContents did not match what was expected", compareContents, contents);

        // COLOR PALETTE (optimized)
        colorPalette.optimize(r);
        compareColorPalette.optimize(r);
        assertEquals(colorPalette, compareColorPalette);
        assertEquals(colorPalette.entries, 41);

        // WEAVING (optimized)
        weaving.optimize(r);
        compareWeaving.optimize(r);
        assertEquals(weaving, compareWeaving);

        // WARP (optimized)
        warp.optimize(r);
        compareWarp.optimize(r);
        int threads = warp.getThreads();
        assertEquals(warp, compareWarp);

        // WEFT (optimized)
        weft.optimize(r);
        compareWeft.optimize(r);
        assertEquals(weft, compareWeft);
        int picks = weft.getThreads();

        // COLOR TABLE (optimized)
        colorTable.optimize(r);
        compareColorTable.optimize(r);
        assertEquals(colorTable, compareColorTable);
        numOfColors = colorTable.getLookupIndices().size();
        assertEquals(41, numOfColors);

        // TREADLING (optimized)
        treadling.optimize(r);
        for (int i = 1; i <= picks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }

        // WARP COLORS (optimized)
        warpColors.optimize(r);
        for (int i = 1; i <= threads; i++) {
            assertTrue(warpColors.getWarpColor(i).isDefined());
        }

        // THREADING (optimized)
        threading.optimize(r);
        for (int i = 1; i <= threads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        r.close();
        is.close();
    }

    @Test
    public void test3Load() throws Exception {
        InputStream is = getInputStream("test3.wif");
        WIFReader r = new WIFReader(is);

        // HEADER
        WIFHeader header = r.getHeader();
        WIFHeader compareHeader = new WIFHeader.Builder()
                .version("1.1")
                .developer("wif@handweaving.net")
                .date("2/12/2004")
                .sourceProgram("Handweaving.net Pattern Maker")
                .sourceVersion("1.0")
                .build();
        assertEquals("WIFHeader did not match what was expected", compareHeader, header);


        // CONTENTS
        WIFContents contents = r.getContents();
        WIFContents compareContents = new WIFContents.Builder()
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, true)
                .contents(WIFContents.WIFContentsKey.WEAVING, true)
                .contents(WIFContents.WIFContentsKey.WARP, true)
                .contents(WIFContents.WIFContentsKey.WEFT, true)
                .contents(WIFContents.WIFContentsKey.TIEUP, true)
                .contents(WIFContents.WIFContentsKey.COLOR_TABLE, true)
                .contents(WIFContents.WIFContentsKey.THREADING, true)
                .contents(WIFContents.WIFContentsKey.WARP_COLORS, true)
                .contents(WIFContents.WIFContentsKey.TREADLING, true)
                .contents(WIFContents.WIFContentsKey.WEFT_COLORS, true)
                .build();
        assertEquals("WIFContents did not match what was expected", compareContents, contents);


        // WEAVING
        Option<WIFWeaving> weavingOption = r.getWeaving();
        WIFWeaving weaving = weavingOption.get();
        WIFWeaving compareWeaving = new WIFWeaving.Builder(4, 4).risingShed(true).build();
        assertEquals(weaving, compareWeaving);


        // COLOR PALETTE
        Option<WIFColorPalette> colorPaletteOption = r.getColorPalette();
        assertTrue("Color Palette expected but not found", colorPaletteOption.isDefined());
        WIFColorPalette colorPalette = colorPaletteOption.get();
        WIFColorPalette compareColorPalette = new WIFColorPalette.Builder(2, new Pair<>(0, 255)).build();
        assertEquals(colorPalette, compareColorPalette);

        // WARP
        Option<WIFWarp> warpOption = r.getWarp();
        assertTrue(warpOption.isDefined());
        WIFWarp warp = warpOption.get();
        WIFWarp compareWarp = new WIFWarp.Builder(32)
                .units(WIFUnit.DECIPOINTS)
                .spacing(80.0)
                .thickness(80.0).build();
        int unoptimizedThreads = warp.getThreads();
        assertEquals(warp, compareWarp);

        // WEFT
        Option<WIFWeft> weftOption = r.getWeft();
        assertTrue(weftOption.isDefined());
        WIFWeft weft = weftOption.get();
        WIFWeft compareWeft = new WIFWeft.Builder(32)
                .units(WIFUnit.DECIPOINTS)
                .spacing(80.0)
                .thickness(80.0).build();
        int unoptimizedPicks = weft.getThreads();
        assertEquals(weft, compareWeft);

        // TIEUP
        Option<WIFTieup> tieupOption = r.getTieup();
        assertTrue(tieupOption.isDefined());
        WIFTieup tieup = tieupOption.get();
        WIFTieup compareTieup = new WIFTieup.Builder()
                .tieup(1, 1, 4)
                .tieup(2, 3, 2)
                .tieup(3, 2, 3)
                .tieup(4, 1, 4).build();
        assertEquals(tieup, compareTieup);

        // COLOR TABLE
        Option<WIFColorTable> colorTableOption = r.getColorTable();
        assertTrue(colorTableOption.isDefined());
        WIFColorTable colorTable = colorTableOption.get();
        WIFColorTable compareColorTable = new WIFColorTable.Builder().palette(colorPalette)
                .color(1, new WIFColorTripplet(102, 204, 255))
                .color(2, new WIFColorTripplet(204, 103, 79)).build();
        assertEquals(colorTable, compareColorTable);

        // TREADLING
        Option<WIFTreadling> treadlingOption = r.getTreadling();
        assertTrue(treadlingOption.isDefined());
        WIFTreadling treadling = treadlingOption.get();
        for (int i = 1; i <= unoptimizedPicks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }

        // WARP COLORS
        Option<WIFWarpColors> warpColorsOption = r.getWarpColors();
        assertTrue(warpColorsOption.isDefined());
        WIFWarpColors warpColors = warpColorsOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            assertTrue(warpColors.getWarpColor(i).isDefined());
        }

        // THREADING
        Option<WIFThreading> threadingOption = r.getThreading();
        assertTrue(threadingOption.isDefined());
        WIFThreading threading = threadingOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // WEFT COLORS
        Option<WIFWeftColors> weftColorsOption = r.getWeftColors();
        assertTrue(weftColorsOption.isDefined());
        WIFWeftColors weftColors = weftColorsOption.get();
        for (int i = 1; i <= unoptimizedPicks; i += 1) {
            assertTrue(weftColors.getWeftColor(i).isDefined());
        }

        // CONTENTS (optimized)
        contents.optimize(r);
        compareContents.optimize(r);
        assertEquals("Optimized WIFContents did not match what was expected", compareContents, contents);

        // WEAVING (optimized)
        weaving.optimize(r);
        compareWeaving.optimize(r);
        assertEquals(weaving, compareWeaving);

        // COLOR PALETTE (optimized)
        colorPalette.optimize(r);
        compareColorPalette.optimize(r);
        assertEquals(colorPalette, compareColorPalette);

        // WARP (optimized)
        warp.optimize(r);
        compareWarp.optimize(r);
        assertEquals(warp, compareWarp);
        int threads = warp.getThreads();

        // WEFT (optmized)
        weft.optimize(r);
        compareWeft.optimize(r);
        assertEquals(weft, compareWeft);
        int picks = weft.getThreads();

        // COLOR TABLE (optimized)
        colorTable.optimize(r);
        compareColorTable.optimize(r);
        assertEquals(colorTable, compareColorTable);

        // TREADLING (optimized)
        treadling.optimize(r);
        for (int i = 1; i <= picks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }

        // WARP COLORS (optimized)
        warpColors.optimize(r);
        for (int i = 1; i <= threads; i++) {
            assertTrue(warpColors.getWarpColor(i).isDefined());
        }

        // THREADING (optimized)
        threading.optimize(r);
        for (int i = 1; i <= threads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // WEFT COLORS (optimized)
        weftColors.optimize(r);
        for (int i = 1; i <= picks; i += 1) {
            assertTrue(weftColors.getWeftColor(i).isDefined());
        }


        is.close();
        r.close();
    }

    @Test
    public void test4Load() throws Exception {
        InputStream is = getInputStream("test4.wif");
        WIFReader r = new WIFReader(is);

        // HEADER
        WIFHeader header = r.getHeader();
        WIFHeader compareHeader = new WIFHeader.Builder()
                .version("1.1")
                .developer("arahne@arahne.si")
                .date("April 09, 2004")
                .sourceProgram("ArahWeave 4.0g")
                .extraMetadata("Title", "16 zu 16, 010")
                .extraMetadata("Source", "Old German Pattern Book, Untitled and Unbound")
                .extraMetadata("Place", "Germany")
                .extraMetadata("Date", "Unknown")
                .extraMetadata("Categories", "")
                .extraMetadata("WarpThreads", "16")
                .extraMetadata("WeftThreads", "16")
                .extraMetadata("WarpRepeats", "3")
                .extraMetadata("WeftRepeats", "3")
                .build();
        assertEquals("WIFHeader did not match what was expected", compareHeader, header);

        // CONTENTS
        WIFContents contents = r.getContents();
        WIFContents compareContents = new WIFContents.Builder()
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, false)
                .contents(WIFContents.WIFContentsKey.WEAVING, true)
                .contents(WIFContents.WIFContentsKey.WARP, true)
                .contents(WIFContents.WIFContentsKey.WEFT, true)
                .contents(WIFContents.WIFContentsKey.TIEUP, true)
                .contents(WIFContents.WIFContentsKey.COLOR_TABLE, false)
                .contents(WIFContents.WIFContentsKey.THREADING, true)
                .contents(WIFContents.WIFContentsKey.WARP_COLORS, false)
                .contents(WIFContents.WIFContentsKey.WEFT_COLORS, false)
                .contents(WIFContents.WIFContentsKey.LIFTPLAN, true)
                .build();
        assertEquals("WIFContents did not match what was expected", compareContents, contents);


        // WEAVING
        Option<WIFWeaving> weavingOption = r.getWeaving();
        WIFWeaving weaving = weavingOption.get();
        WIFWeaving compareWeaving = new WIFWeaving.Builder(16, 16).risingShed(true).build();
        assertEquals(weaving, compareWeaving);

        // WARP
        Option<WIFWarp> warpOption = r.getWarp();
        assertTrue(warpOption.isDefined());
        WIFWarp warp = warpOption.get();
        WIFWarp compareWarp = new WIFWarp.Builder(48)
                .units(WIFUnit.CENTIMETERS)
                .spacing(0.0185)
                .thickness(0.0213).build();
        assertEquals(warp, compareWarp);
        int unoptimizedThreads = warp.getThreads();

        //TODO make treadling

        // WEFT
        Option<WIFWeft> weftOption = r.getWeft();
        assertTrue(weftOption.isDefined());
        WIFWeft weft = weftOption.get();
        WIFWeft compareWeft = new WIFWeft.Builder(48)
                .units(WIFUnit.CENTIMETERS)
                .spacing(0.0333)
                .thickness(0.0246).build();
        assertEquals(weft, compareWeft);
        int unoptimizedPicks = weft.getThreads();

        // TIEUP
        Option<WIFTieup> tieupOption = r.getTieup();
        assertTrue(tieupOption.isDefined());
        WIFTieup tieup = tieupOption.get();
        WIFTieup compareTieup = new WIFTieup.Builder()
                .tieup(1, 1, 3, 6, 11, 13, 14, 15)
                .tieup(2, 1, 2, 4, 5, 8, 12, 13, 14, 16)
                .tieup(3, 1, 3, 5, 6, 7, 10, 12, 13, 15, 16)
                .tieup(4, 3, 4, 6, 7, 8, 11, 12, 14, 15, 16)
                .tieup(5, 2, 4, 5, 7, 8, 9, 11, 13, 14, 15)
                .tieup(6, 4, 5, 6, 8, 9, 10, 12, 13, 16)
                .tieup(7, 3, 5, 6, 7, 9, 11, 14)
                .tieup(8, 1, 6, 7, 8, 10, 11, 12)
                .tieup(9, 4, 7, 9, 11, 12, 13, 15)
                .tieup(10, 2, 5, 6, 8, 9, 10, 12, 13, 14)
                .tieup(11, 3, 4, 5, 7, 9, 10, 11, 13, 14, 16)
                .tieup(12, 2, 3, 4, 6, 7, 10, 11, 12, 14, 15)
                .tieup(13, 1, 2, 3, 5, 6, 8, 11, 12, 13, 15)
                .tieup(14, 1, 2, 4, 5, 6, 10, 13, 14, 16)
                .tieup(15, 1, 3, 4, 5, 7, 12, 15)
                .tieup(16, 2, 3, 4, 9, 14, 15, 16).build();
        assertEquals(tieup, compareTieup);

        // THREADING
        Option<WIFThreading> threadingOption = r.getThreading();
        assertTrue(threadingOption.isDefined());
        WIFThreading threading = threadingOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // LIFTPLAN
        Option<WIFLiftPlan> liftPlanOption = r.getLiftPlan();
        assertTrue(liftPlanOption.isDefined());
        WIFLiftPlan liftPlan = liftPlanOption.get();
        for (int i = 1; i <= unoptimizedPicks; i++) {
            List<Integer> shaftsForPick = liftPlan.getShaftsForPick(i);
            assertTrue(!shaftsForPick.isEmpty());
        }

        // CONTENTS (optimized)
        contents.optimize(r);
        compareContents.optimize(r);
        assertEquals("Optimized WIFContents did not match what was expected", compareContents, contents);

        // WEAVING (optimized)
        weaving.optimize(r);
        compareWeaving.optimize(r);
        assertEquals(weaving, compareWeaving);

        // WARP (optimized)
        warp.optimize(r);
        compareWarp.optimize(r);
        assertEquals(warp, compareWarp);
        int threads = warp.getThreads();

        // WEFT (optimized)
        weft.optimize(r);
        compareWeft.optimize(r);
        assertEquals(weft, compareWeft);
        int picks = weft.getThreads();

        // THREADING (optimized)
        threading.optimize(r);
        for (int i = 1; i <= threads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // LIFTPLAN (optimized)
        liftPlan.optimize(r);
        for (int i = 1; i <= picks; i++) {
            List<Integer> shaftsForPick = liftPlan.getShaftsForPick(i);
            assertTrue(!shaftsForPick.isEmpty());
        }

        r.close();
        is.close();

    }

    @Test
    public void test5Load() throws Exception {
        InputStream is = getInputStream("test5.wif");
        WIFReader r = new WIFReader(is);

        // HEADER
        WIFHeader header = r.getHeader();
        assertNotNull("No Header found in test5", header);
        WIFHeader compareHeader = new WIFHeader.Builder()
                .version("1.1")
                .developer("wif@mhsoft.com")
                .date("April 20, 1997")
                .sourceProgram("Fiberworks PCW")
                .sourceVersion("4.0")
                .build();

        assertEquals("WIFHeader did not match what was expected", compareHeader, header);

        // CONTENTS (unoptimized)
        WIFContents contents = r.getContents();
        WIFContents compareContents = new WIFContents.Builder()
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, false)
                .contents(WIFContents.WIFContentsKey.TEXT, true)
                .contents(WIFContents.WIFContentsKey.WEAVING, true)
                .contents(WIFContents.WIFContentsKey.WARP, true)
                .contents(WIFContents.WIFContentsKey.WEFT, true)
                .contents(WIFContents.WIFContentsKey.NOTES, true)
                .contents(WIFContents.WIFContentsKey.WEFT_COLORS, true)
                .contents(WIFContents.WIFContentsKey.COLOR_TABLE, true)
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, true)
                .contents(WIFContents.WIFContentsKey.LIFTPLAN, true)
                .contents(WIFContents.WIFContentsKey.THREADING, true)
                .build();
        assertEquals("WIFContents did not match what was expected", compareContents, contents);

        // CONTENTS (optimized)
        contents.optimize(r);
        compareContents.optimize(r);
        assertEquals("Optimized WIFContents did not match what was expected", compareContents, contents);

        // COLOR PALETTE
        Option<WIFColorPalette> colorPaletteOption = r.getColorPalette();
        assertTrue("Color Palette expected but not found", colorPaletteOption.isDefined());
        WIFColorPalette colorPalette = colorPaletteOption.get();
        WIFColorPalette compareColorPalette = new WIFColorPalette.Builder(82, new Pair<>(0, 999)).build();
        assertEquals(colorPalette, compareColorPalette);

        // COLOR PALETTE (optimized)
        colorPalette.optimize(r);
        compareColorPalette.optimize(r);
        assertEquals(colorPalette, compareColorPalette);

        // TEXT
        Option<WIFText> textOption = r.getText();
        assertTrue(textOption.isDefined());
        WIFText text = textOption.get();
        WIFText compareText = new WIFText.Builder().title("oelsner833_4blattd.dt").build();
        assertEquals(text, compareText);

        // WEAVING (unoptimized)
        Option<WIFWeaving> weavingOption = r.getWeaving();
        WIFWeaving weaving = weavingOption.get();
        WIFWeaving compareWeaving = new WIFWeaving.Builder(32, 32).risingShed(true).build();
        assertEquals(weaving, compareWeaving);

        // WEAVING (optimized)
        weaving.optimize(r);
        compareWeaving.optimize(r);
        assertEquals(weaving, compareWeaving);

        // WARP
        Option<WIFWarp> warpOption = r.getWarp();
        assertTrue(warpOption.isDefined());
        WIFWarp warp = warpOption.get();
        WIFWarp compareWarp = new WIFWarp.Builder(128)
                .colorPaletteIndex(1)
                .units(WIFUnit.CENTIMETERS)
                .spacing(0.212)
                .thickness(0.212).build();
        int unoptimizedThreads = warp.getThreads();
        assertEquals(warp, compareWarp);

        // WARP (optimized)
        warp.optimize(r);
        compareWarp.optimize(r);
        int threads = warp.getThreads();

        // WEFT
        Option<WIFWeft> weftOption = r.getWeft();
        assertTrue(weftOption.isDefined());
        WIFWeft weft = weftOption.get();
        WIFWeft compareWeft = new WIFWeft.Builder(296)
                .colorPaletteIndex(69)
                .units(WIFUnit.CENTIMETERS)
                .spacing(0.212)
                .thickness(0.212).build();
        assertEquals(weft, compareWeft);
        int unoptimizedPicks = weft.getThreads();

        // WEFT (optimized)
        weft.optimize(r);
        compareWeft.optimize(r);
        assertEquals(weft, compareWeft);
        int picks = weft.getThreads();


        // NOTES
        Option<WIFNotes> notesOption = r.getNotes();
        assertTrue(notesOption.isDefined());
        WIFNotes notes = notesOption.get();
        assertEquals(("oelsner833_4blattd.wif\n" +
                "WIF Version 1.1\n" +
                "Source Program: Fiberworks PCW 4.0\n" +
                "\n" +
                "\n" +
                "oelsner833_4blatt.wif\n" +
                "WIF Version 1.1\n" +
                "Source Program: Fiberworks PCW 4.0").replace("\n", "").trim(), notes.getNotes().replace("\n", "").trim());

        // COLOR TABLE
        Option<WIFColorTable> colorTableOption = r.getColorTable();
        assertTrue(colorTableOption.isDefined());
        WIFColorTable colorTable = colorTableOption.get();
        WIFColorTable compareColorTable = test582Colors(new WIFColorTable.Builder().palette(colorPalette)).build();
        assertEquals(colorTable, compareColorTable);
        int numOfColors = colorTable.getLookupIndices().size();
        assertEquals(82, numOfColors);

        // COLOR TABLE (optimized)
        colorTable.optimize(r);
        compareColorTable.optimize(r);
        numOfColors = colorTable.getLookupIndices().size();
        assertEquals(2, numOfColors);
        assertEquals(colorTable, compareColorTable);

        // THREADING
        Option<WIFThreading> threadingOption = r.getThreading();
        assertTrue(threadingOption.isDefined());
        WIFThreading threading = threadingOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // THREADING (optimized)
        threading.optimize(r);
        for (int i = 1; i <= threads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // LIFTPLAN
        Option<WIFLiftPlan> liftPlanOption = r.getLiftPlan();
        assertTrue(liftPlanOption.isDefined());
        WIFLiftPlan liftPlan = liftPlanOption.get();
        for (int i = 1; i <= unoptimizedPicks; i++) {
            List<Integer> shaftsForPick = liftPlan.getShaftsForPick(i);
            assertTrue(!shaftsForPick.isEmpty());
        }

        // WEFT COLORS
        Option<WIFWeftColors> weftColorsOption = r.getWeftColors();
        assertTrue(weftColorsOption.isDefined());
        WIFWeftColors weftColors = weftColorsOption.get();
        for (int i = 1; i <= unoptimizedPicks; i++) {
            assertTrue(weftColors.getWeftColor(i).isDefined());
        }

        // WEFT COLORS (optimized)
        weftColors.optimize(r);
        for (int i = 1; i <= picks; i++) {
            assertTrue(weftColors.getWeftColor(i).isDefined());
        }
        is.close();
        r.close();
    }

    @Test
    public void test6Load() throws Exception {
        InputStream is = getInputStream("test6.wif");
        WIFReader r = new WIFReader(is);

        // HEADER
        WIFHeader header = r.getHeader();
        assertNotNull("No Header found in test6", header);
        WIFHeader compareHeader = new WIFHeader.Builder()
                .version("1.1")
                .developer("ralph@cs.arizona.edu")
                .date("Monday, April 17, 2000  9:06 am")
                .sourceProgram("gif2wif.icn")
                .build();
        assertEquals("WIFHeader did not match what was expected", compareHeader, header);

        // CONTENTS
        WIFContents contents = r.getContents();
        WIFContents compareContents = new WIFContents.Builder()
                .contents(WIFContents.WIFContentsKey.TEXT, true)
                .contents(WIFContents.WIFContentsKey.TIEUP, true)
                .contents(WIFContents.WIFContentsKey.WEAVING, true)
                .contents(WIFContents.WIFContentsKey.WARP, true)
                .contents(WIFContents.WIFContentsKey.WEFT, true)
                .contents(WIFContents.WIFContentsKey.WEFT_COLORS, false)
                .contents(WIFContents.WIFContentsKey.WARP_COLORS, false)
                .contents(WIFContents.WIFContentsKey.TREADLING, true)
                .contents(WIFContents.WIFContentsKey.COLOR_TABLE, true)
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, true)
                .contents(WIFContents.WIFContentsKey.THREADING, true)
                .build();
        assertEquals("WIFContents did not match what was expected", compareContents, contents);

        // COLOR PALETTE
        Option<WIFColorPalette> colorPaletteOption = r.getColorPalette();
        assertTrue("Color Palette expected but not found", colorPaletteOption.isDefined());
        WIFColorPalette colorPalette = colorPaletteOption.get();
        WIFColorPalette compareColorPalette = new WIFColorPalette.Builder(2, new Pair<>(0, 65535)).build();
        assertEquals("Color Palette incorrect", colorPalette, compareColorPalette);

        // TEXT
        Option<WIFText> textOption = r.getText();
        assertTrue(textOption.isDefined());
        WIFText text = textOption.get();
        WIFText compareText = new WIFText.Builder()
                .title("example")
                .author("Ralph E. Griswold")
                .address("5302 E. 4th St., Tucson, AZ 85711")
                .email("ralph@cs.arizona.edu")
                .telephone("520-881-1470")
                .fax("520-325-3948").build();
        assertEquals(text, compareText);

        // WEAVING
        Option<WIFWeaving> weavingOption = r.getWeaving();
        WIFWeaving weaving = weavingOption.get();
        WIFWeaving compareWeaving = new WIFWeaving.Builder(32, 32).risingShed(true).build();
        assertEquals(weaving, compareWeaving);

        // WARP
        Option<WIFWarp> warpOption = r.getWarp();
        assertTrue(warpOption.isDefined());
        WIFWarp warp = warpOption.get();
        WIFWarp compareWarp = new WIFWarp.Builder(65)
                .colorPaletteIndex(1)
                .units(WIFUnit.DECIPOINTS)
                .thickness(10.0).build();
        assertEquals(warp, compareWarp);
        int unoptimizedThreads = warp.getThreads();

        // WEFT
        Option<WIFWeft> weftOption = r.getWeft();
        assertTrue(weftOption.isDefined());
        WIFWeft weft = weftOption.get();
        WIFWeft compareWeft = new WIFWeft.Builder(65)
                .colorPaletteIndex(2)
                .units(WIFUnit.DECIPOINTS)
                .thickness(10.0).build();
        assertEquals(weft, compareWeft);
        int unoptimizedPicks = weft.getThreads();

        // TIEUP
        Option<WIFTieup> tieupOption = r.getTieup();
        assertTrue(tieupOption.isDefined());
        WIFTieup tieup = tieupOption.get();
        WIFTieup compareTieup = new WIFTieup.Builder()
                .tieup(1, 1, 2, 3, 4, 7, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 27, 30, 31, 32)
                .tieup(2, 1, 2, 3, 4, 5, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 26, 27, 28, 31, 32)
                .tieup(3, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 15, 16, 17, 18, 19, 20, 21, 22, 25, 26, 27, 28, 29, 32)
                .tieup(4, 1, 4, 5, 6, 7, 8, 9, 10, 13, 16, 17, 18, 19, 20, 21, 24, 25, 26, 27, 28, 29, 30, 31, 32)
                .tieup(5, 3, 4, 5, 6, 7, 8, 9, 12, 13, 14, 17, 18, 19, 20, 21, 22, 25, 26, 27, 28, 29, 30, 31, 32)
                .tieup(6, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 18, 19, 20, 21, 22, 23, 26, 27, 28, 29, 30, 31)
                .tieup(7, 1, 2, 3, 4, 5, 6, 7, 10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 27, 28, 29, 30)
                .tieup(8, 2, 3, 4, 5, 6, 9, 10, 11, 12, 13, 14, 15, 16, 17, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31)
                .tieup(9, 3, 4, 5, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 21, 22, 23, 24, 25, 26, 29, 30, 31, 32)
                .tieup(10, 1, 4, 7, 8, 9, 10, 11, 12, 16, 17, 18, 19, 22, 23, 24, 25, 26, 27, 30, 31, 32)
                .tieup(11, 1, 2, 6, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 26, 27, 28, 31, 32)
                .tieup(12, 1, 2, 3, 5, 6, 7, 8, 9, 10, 13, 16, 17, 18, 19, 21, 24, 25, 26, 27, 28, 29, 32)
                .tieup(13, 1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 15, 16, 17, 18, 22, 25, 26, 27, 28, 29, 30)
                .tieup(14, 1, 2, 3, 4, 5, 6, 7, 8, 11, 14, 15, 16, 17, 20, 23, 26, 27, 28, 29, 32)
                .tieup(15, 1, 2, 3, 4, 5, 6, 7, 10, 11, 12, 15, 19, 20, 21, 25, 26, 27, 28, 31, 32)
                .tieup(16, 1, 2, 3, 4, 5, 6, 9, 10, 11, 12, 13, 16, 18, 19, 20, 21, 22, 24, 25, 26, 27, 30, 31, 32)
                .tieup(17, 1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 14, 17, 20, 21, 22, 23, 24, 25, 26, 29, 30, 31, 32)
                .tieup(18, 1, 2, 3, 4, 7, 8, 9, 10, 12, 13, 14, 15, 16, 18, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32)
                .tieup(19, 1, 2, 3, 6, 7, 8, 9, 13, 14, 15, 19, 22, 23, 24, 27, 28, 29, 30, 31, 32)
                .tieup(20, 1, 2, 5, 6, 7, 8, 11, 14, 17, 18, 19, 20, 23, 26, 27, 28, 29, 30, 31, 32)
                .tieup(21, 1, 4, 5, 6, 7, 8, 9, 12, 16, 17, 18, 19, 22, 25, 26, 27, 28, 29, 30, 31, 32)
                .tieup(22, 1, 2, 5, 6, 7, 8, 9, 10, 13, 15, 16, 17, 18, 21, 24, 25, 26, 27, 28, 29, 31, 32)
                .tieup(23, 1, 2, 3, 6, 7, 8, 9, 10, 11, 14, 15, 16, 17, 20, 23, 24, 25, 26, 27, 28, 32)
                .tieup(24, 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 15, 16, 17, 18, 22, 23, 24, 25, 26, 27, 30)
                .tieup(25, 2, 3, 4, 5, 8, 9, 10, 11, 12, 13, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26, 29, 30, 31)
                .tieup(26, 3, 4, 5, 6, 9, 10, 11, 12, 13, 14, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32)
                .tieup(27, 1, 4, 5, 6, 7, 10, 11, 12, 13, 14, 15, 18, 19, 20, 21, 22, 23, 24, 27, 28, 29, 30, 31, 32)
                .tieup(28, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 26, 27, 28, 29, 30, 31, 32)
                .tieup(29, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 14, 15, 16, 17, 20, 21, 22, 25, 26, 27, 28, 29, 30, 31)
                .tieup(30, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 13, 14, 15, 16, 17, 18, 21, 24, 25, 26, 27, 28, 29, 30)
                .tieup(31, 1, 2, 3, 5, 6, 7, 8, 9, 12, 13, 14, 15, 16, 17, 18, 19, 23, 24, 25, 26, 28, 29, 30, 31, 32)
                .tieup(32, 1, 2, 3, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 29, 30, 31, 32).build();
        assertEquals(tieup, compareTieup);

        // COLOR TABLE
        Option<WIFColorTable> colorTableOption = r.getColorTable();
        assertTrue(colorTableOption.isDefined());
        WIFColorTable colorTable = colorTableOption.get();
        WIFColorTable compareColorTable = new WIFColorTable.Builder()
                .palette(colorPalette)
                .color(1, new WIFColorTripplet(0, 0, 0))
                .color(2, new WIFColorTripplet(65535, 65535, 65535)).build();
        assertEquals(colorTable, compareColorTable);

        // THREADING
        Option<WIFThreading> threadingOption = r.getThreading();
        assertTrue(threadingOption.isDefined());
        WIFThreading threading = threadingOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // TREADLING
        Option<WIFTreadling> treadlingOption = r.getTreadling();
        assertTrue(treadlingOption.isDefined());
        WIFTreadling treadling = treadlingOption.get();
        for (int i = 1; i <= unoptimizedPicks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }


        // CONTENTS (optimized)
        contents.optimize(r);
        compareContents.optimize(r);
        assertEquals("Optimized WIFContents did not match what was expected", compareContents, contents);


        // COLOR PALETTE (optimized)
        colorPalette.optimize(r);
        compareColorPalette.optimize(r);
        assertEquals("Color Palette incorrect", colorPalette, compareColorPalette);


        // WEAVING (optimized)
        weaving.optimize(r);
        compareWeaving.optimize(r);
        assertEquals(weaving, compareWeaving);

        // WARP (optimized)
        warp.optimize(r);
        compareWarp.optimize(r);
        int threads = warp.getThreads();
        assertEquals(warp, compareWarp);

        // WEFT (optimized)
        weft.optimize(r);
        compareWeft.optimize(r);
        int picks = weft.getThreads();
        assertEquals(weft, compareWeft);

        // COLOR TABLE (optimized)
        colorTable.optimize(r);
        compareColorTable.optimize(r);
        assertEquals(colorTable, compareColorTable);

        // THREADING (optimized)
        threading.optimize(r);
        for (int i = 1; i <= threads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // TREADLING (optimized)
        treadling.optimize(r);
        for (int i = 1; i <= picks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }

        r.close();
        is.close();
    }

    @Test
    public void test7Load() throws Exception {
        InputStream is = getInputStream("test7.wif");
        WIFReader r = new WIFReader(is);

        // HEADER
        WIFHeader header = r.getHeader();
        assertNotNull("No Header found in test7", header);
        WIFHeader compareHeader = new WIFHeader.Builder()
                .version("1.1")
                .developer("wif@mhsoft.com")
                .date("April 20, 1997")
                .sourceProgram("Fiberworks PCW")
                .sourceVersion("4.0")
                .build();
        assertEquals("WIFHeader did not match what was expected", compareHeader, header);

        // CONTENTS
        WIFContents contents = r.getContents();
        WIFContents compareContents = new WIFContents.Builder()
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, false)
                .contents(WIFContents.WIFContentsKey.TEXT, true)
                .contents(WIFContents.WIFContentsKey.WEAVING, true)
                .contents(WIFContents.WIFContentsKey.WARP, true)
                .contents(WIFContents.WIFContentsKey.WEFT, true)
                .contents(WIFContents.WIFContentsKey.WEFT_COLORS, true)
                .contents(WIFContents.WIFContentsKey.WARP_COLORS, true)
                .contents(WIFContents.WIFContentsKey.COLOR_TABLE, true)
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, true)
                .contents(WIFContents.WIFContentsKey.THREADING, true)
                .contents(WIFContents.WIFContentsKey.TREADLING, true)
                .contents(WIFContents.WIFContentsKey.TIEUP, true)
                .build();
        assertEquals("WIFContents did not match what was expected", compareContents, contents);


        // COLOR PALETTE
        Option<WIFColorPalette> colorPaletteOption = r.getColorPalette();
        assertTrue("Color Palette expected but not found", colorPaletteOption.isDefined());
        WIFColorPalette colorPalette = colorPaletteOption.get();
        WIFColorPalette compareColorPalette = new WIFColorPalette.Builder(82, new Pair<>(0, 999)).build();
        assertEquals("Color Palette incorrect", colorPalette, compareColorPalette);


        // TEXT
        Option<WIFText> textOption = r.getText();
        WIFText text = textOption.get();
        WIFText compareText = new WIFText.Builder().title("Huck01.dtx").build();
        assertEquals(text, compareText);


        // WEAVING
        Option<WIFWeaving> weavingOption = r.getWeaving();
        WIFWeaving weaving = weavingOption.get();
        WIFWeaving compareWeaving = new WIFWeaving.Builder(4, 6).risingShed(false).build();
        assertEquals(weaving, compareWeaving);

        // WARP
        Option<WIFWarp> warpOption = r.getWarp();
        assertTrue(warpOption.isDefined());
        WIFWarp warp = warpOption.get();
        WIFWarp compareWarp = new WIFWarp.Builder(84)
                .colorPaletteIndex(36)
                .units(WIFUnit.CENTIMETERS)
                .spacing(0.212)
                .thickness(0.212).build();
        assertEquals(warp, compareWarp);
        int unoptimizedThreads = warp.getThreads();

        // WEFT
        Option<WIFWeft> weftOption = r.getWeft();
        assertTrue(weftOption.isDefined());
        WIFWeft weft = weftOption.get();
        WIFWeft compareWeft = new WIFWeft.Builder(48)
                .colorPaletteIndex(1)
                .units(WIFUnit.CENTIMETERS)
                .spacing(0.212)
                .thickness(0.212).build();
        assertEquals(weft, compareWeft);
        int unoptimizedPicks = weft.getThreads();

        // TIEUP
        Option<WIFTieup> tieupOption = r.getTieup();
        assertTrue(tieupOption.isDefined());
        WIFTieup tieup = tieupOption.get();
        WIFTieup compareTieup = new WIFTieup.Builder()
                .tieup(1, 1, 2)
                .tieup(2, 2, 3)
                .tieup(3, 3, 4)
                .tieup(4, 1, 4)
                .tieup(5, 1, 3)
                .tieup(6, 2, 4).build();
        assertEquals(tieup, compareTieup);

        // COLOR TABLE
        Option<WIFColorTable> colorTableOption = r.getColorTable();
        assertTrue(colorTableOption.isDefined());
        WIFColorTable colorTable = colorTableOption.get();
        WIFColorTable compareColorTable = test782Colors(new WIFColorTable.Builder().palette(colorPalette)).build();
        assertEquals(colorTable, compareColorTable);
        int numOfColors = colorTable.getLookupIndices().size();
        assertEquals(82, numOfColors);


        // THREADING
        Option<WIFThreading> threadingOption = r.getThreading();
        assertTrue(threadingOption.isDefined());
        WIFThreading threading = threadingOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // TREADLING
        Option<WIFTreadling> treadlingOption = r.getTreadling();
        assertTrue(treadlingOption.isDefined());
        WIFTreadling treadling = treadlingOption.get();
        for (int i = 1; i <= unoptimizedPicks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }

        // WARP COLORS
        Option<WIFWarpColors> warpColorsOption = r.getWarpColors();
        assertTrue(warpColorsOption.isDefined());
        WIFWarpColors warpColors = warpColorsOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            assertTrue(warpColors.getWarpColor(i).isDefined());
        }

        // WEFT COLORS
        Option<WIFWeftColors> weftColorsOption = r.getWeftColors();
        assertTrue(weftColorsOption.isDefined());
        WIFWeftColors weftColors = weftColorsOption.get();
        for (int i = 1; i <= unoptimizedPicks; i++) {
            assertTrue(weftColors.getWeftColor(i).isDefined());
        }

        // WEFT (optimized)
        weft.optimize(r);
        compareWeft.optimize(r);
        assertEquals(weft, compareWeft);
        int picks = weft.getThreads();

        // WEFT COLORS (optimized)
        weftColors.optimize(r);
        for (int i = 1; i <= picks; i++) {
            assertTrue(weftColors.getWeftColor(i).isDefined());
        }

        // CONTENTS (optimized)
        contents.optimize(r);
        compareContents.optimize(r);
        assertEquals("Optimized WIFContents did not match what was expected", compareContents, contents);

        // COLOR PALETTE (optimized)
        colorPalette.optimize(r);
        compareColorPalette.optimize(r);
        assertEquals("Color Palette incorrect", colorPalette, compareColorPalette);
        assertEquals("Optimization failed", compareColorPalette.entries, 2);

        // WEAVING (optimized)
        weaving.optimize(r);
        compareWeaving.optimize(r);
        assertEquals(weaving, compareWeaving);

        // WARP (optimized)
        warp.optimize(r);
        compareWarp.optimize(r);
        int threads = warp.getThreads();
        assertEquals(warp, compareWarp);

        // COLOR TABLE (optimized)
        colorTable.optimize(r);
        compareColorTable.optimize(r);
        numOfColors = colorTable.getLookupIndices().size();
        assertEquals(2, numOfColors);
        assertEquals(colorTable, compareColorTable);

        // THREADING (optimized)
        threading.optimize(r);
        for (int i = 1; i <= threads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // TREADLING (optimized)
        treadling.optimize(r);
        for (int i = 1; i <= picks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }

        // WARP COLORS (optimized)
        warpColors.optimize(r);
        for (int i = 1; i <= threads; i++) {
            assertTrue(warpColors.getWarpColor(i).isDefined());
        }


        r.close();
        is.close();
    }

    @Test
    public void test8Load() throws Exception {
        InputStream is = getInputStream("test8.wif");
        WIFReader r = new WIFReader(is);

        // HEADER
        WIFHeader header = r.getHeader();
        assertNotNull("No Header found in test8", header);
        WIFHeader compareHeader = new WIFHeader.Builder()
                .version("1.1")
                .developer("wif@mhsoft.com")
                .date("April 20, 1997")
                .sourceProgram("Fiberworks PCW")
                .sourceVersion("4.0")
                .build();

        assertEquals("WIFHeader did not match what was expected", compareHeader, header);

        // CONTENTS
        WIFContents contents = r.getContents();
        WIFContents compareContents = new WIFContents.Builder()
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, false)
                .contents(WIFContents.WIFContentsKey.TEXT, true)
                .contents(WIFContents.WIFContentsKey.WEAVING, true)
                .contents(WIFContents.WIFContentsKey.WARP, true)
                .contents(WIFContents.WIFContentsKey.WEFT, true)
                .contents(WIFContents.WIFContentsKey.WEFT_COLORS, true)
                .contents(WIFContents.WIFContentsKey.WARP_COLORS, true)
                .contents(WIFContents.WIFContentsKey.COLOR_TABLE, true)
                .contents(WIFContents.WIFContentsKey.COLOR_PALETTE, true)
                .contents(WIFContents.WIFContentsKey.THREADING, true)
                .contents(WIFContents.WIFContentsKey.TREADLING, true)
                .contents(WIFContents.WIFContentsKey.TIEUP, true)
                .build();
        assertEquals("WIFContents did not match what was expected", compareContents, contents);

        // COLOR PALETTE
        Option<WIFColorPalette> colorPaletteOption = r.getColorPalette();
        assertTrue("Color Palette expected but not found", colorPaletteOption.isDefined());
        WIFColorPalette colorPalette = colorPaletteOption.get();
        WIFColorPalette compareColorPalette = new WIFColorPalette.Builder(82, new Pair<>(0, 999)).build();
        assertEquals(colorPalette, compareColorPalette);


        // TEXT
        Option<WIFText> textOption = r.getText();
        WIFText text = textOption.get();
        WIFText compareText = new WIFText.Builder().title("Star01.dtx").build();
        assertEquals(text, compareText);


        // WEAVING
        Option<WIFWeaving> weavingOption = r.getWeaving();
        WIFWeaving weaving = weavingOption.get();
        WIFWeaving compareWeaving = new WIFWeaving.Builder(4, 4).risingShed(false).build();
        assertEquals(weaving, compareWeaving);

        // WARP
        Option<WIFWarp> warpOption = r.getWarp();
        assertTrue(warpOption.isDefined());
        WIFWarp warp = warpOption.get();
        WIFWarp compareWarp = new WIFWarp.Builder(52)
                .colorPaletteIndex(48)
                .units(WIFUnit.CENTIMETERS)
                .spacing(0.212)
                .thickness(0.212).build();
        assertEquals(warp, compareWarp);
        int unoptimizedThreads = warp.getThreads();


        // WEFT
        Option<WIFWeft> weftOption = r.getWeft();
        assertTrue(weftOption.isDefined());
        WIFWeft weft = weftOption.get();
        WIFWeft compareWeft = new WIFWeft.Builder(52)
                .colorPaletteIndex(50)
                .units(WIFUnit.CENTIMETERS)
                .spacing(0.212)
                .thickness(0.212).build();
        assertEquals(weft, compareWeft);
        int unoptimizedPicks = weft.getThreads();


        // TIEUP
        Option<WIFTieup> tieupOption = r.getTieup();
        assertTrue(tieupOption.isDefined());
        WIFTieup tieup = tieupOption.get();
        WIFTieup compareTieup = new WIFTieup.Builder()
                .tieup(1, 1, 2)
                .tieup(2, 2, 3)
                .tieup(3, 3, 4)
                .tieup(4, 1, 4).build();
        assertEquals(tieup, compareTieup);

        // COLOR TABLE
        Option<WIFColorTable> colorTableOption = r.getColorTable();
        assertTrue(colorTableOption.isDefined());
        WIFColorTable colorTable = colorTableOption.get();
        WIFColorTable compareColorTable = test882Colors(new WIFColorTable.Builder().palette(colorPalette)).build();
        assertEquals(colorTable, compareColorTable);
        int numOfColors = colorTable.getLookupIndices().size();
        assertEquals(82, numOfColors);


        // THREADING
        Option<WIFThreading> threadingOption = r.getThreading();
        assertTrue(threadingOption.isDefined());
        WIFThreading threading = threadingOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // TREADLING
        Option<WIFTreadling> treadlingOption = r.getTreadling();
        assertTrue(treadlingOption.isDefined());
        WIFTreadling treadling = treadlingOption.get();
        for (int i = 1; i <= unoptimizedPicks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }

        // WARP COLORS
        Option<WIFWarpColors> warpColorsOption = r.getWarpColors();
        assertTrue(warpColorsOption.isDefined());
        WIFWarpColors warpColors = warpColorsOption.get();
        for (int i = 1; i <= unoptimizedThreads; i++) {
            assertTrue(warpColors.getWarpColor(i).isDefined());
        }


        // WEFT COLORS
        Option<WIFWeftColors> weftColorsOption = r.getWeftColors();
        assertTrue(weftColorsOption.isDefined());
        WIFWeftColors weftColors = weftColorsOption.get();
        for (int i = 1; i <= unoptimizedPicks; i++) {
            assertTrue(weftColors.getWeftColor(i).isDefined());
        }

        // CONTENTS (optimized)
        contents.optimize(r);
        compareContents.optimize(r);
        assertEquals("Optimized WIFContents did not match what was expected", compareContents, contents);

        // COLOR PALETTE (optimized)
        colorPalette.optimize(r);
        compareColorPalette.optimize(r);
        assertEquals(colorPalette, compareColorPalette);
        assertEquals(colorPalette.entries, 2);

        // WEAVING (optimized)
        weaving.optimize(r);
        compareWeaving.optimize(r);
        assertEquals(weaving, compareWeaving);

        //WARP (optimized)
        warp.optimize(r);
        compareWarp.optimize(r);
        assertEquals(warp, compareWarp);
        int threads = warp.getThreads();

        // WEFT (optimized)
        weft.optimize(r);
        compareWeft.optimize(r);
        assertEquals(weft, compareWeft);
        int picks = weft.getThreads();

        // COLOR TABLE (optimized)
        colorTable.optimize(r);
        compareColorTable.optimize(r);
        numOfColors = colorTable.getLookupIndices().size();
        assertEquals(2, numOfColors);
        assertEquals(colorTable, compareColorTable);

        // THREADING (optimized)
        threading.optimize(r);
        for (int i = 1; i <= threads; i++) {
            List<Integer> shaftsForWarp = threading.getShaftsForWarp(i);
            assertTrue(!shaftsForWarp.isEmpty());
        }

        // TREADLING (optimized)
        treadling.optimize(r);
        for (int i = 1; i <= picks; i++) {
            assertTrue(!treadling.getTreadling(i).isEmpty());
        }

        // WARP COLORS (optimized)
        warpColors.optimize(r);
        for (int i = 1; i <= threads; i++) {
            assertTrue(warpColors.getWarpColor(i).isDefined());
        }

        // WEFT COLORS (optimized)
        weftColors.optimize(r);
        for (int i = 1; i <= picks; i++) {
            assertTrue(weftColors.getWeftColor(i).isDefined());
        }


        r.close();
        is.close();
    }


    private WIFColorTable.Builder test882Colors(WIFColorTable.Builder b) {
        b.color(1, new WIFColorTripplet(999, 999, 999))
                .color(2, new WIFColorTripplet(0, 0, 999))
                .color(3, new WIFColorTripplet(199, 0, 999))
                .color(4, new WIFColorTripplet(0, 266, 999))
                .color(5, new WIFColorTripplet(0, 999, 999))
                .color(6, new WIFColorTripplet(0, 999, 0))
                .color(7, new WIFColorTripplet(466, 999, 0))
                .color(8, new WIFColorTripplet(999, 999, 0))
                .color(9, new WIFColorTripplet(999, 532, 0))
                .color(10, new WIFColorTripplet(999, 0, 0))
                .color(11, new WIFColorTripplet(999, 0, 599))
                .color(12, new WIFColorTripplet(799, 0, 999))
                .color(13, new WIFColorTripplet(517, 399, 999))
                .color(14, new WIFColorTripplet(399, 607, 999))
                .color(15, new WIFColorTripplet(399, 999, 999))
                .color(16, new WIFColorTripplet(399, 999, 399))
                .color(17, new WIFColorTripplet(787, 999, 399))
                .color(18, new WIFColorTripplet(999, 999, 399))
                .color(19, new WIFColorTripplet(999, 677, 399))
                .color(20, new WIFColorTripplet(999, 399, 399))
                .color(21, new WIFColorTripplet(999, 399, 760))
                .color(22, new WIFColorTripplet(877, 399, 999))
                .color(23, new WIFColorTripplet(121, 0, 599))
                .color(24, new WIFColorTripplet(0, 160, 599))
                .color(25, new WIFColorTripplet(0, 599, 599))
                .color(26, new WIFColorTripplet(0, 599, 0))
                .color(27, new WIFColorTripplet(278, 599, 0))
                .color(28, new WIFColorTripplet(599, 599, 0))
                .color(29, new WIFColorTripplet(599, 321, 0))
                .color(30, new WIFColorTripplet(599, 0, 0))
                .color(31, new WIFColorTripplet(599, 0, 360))
                .color(32, new WIFColorTripplet(477, 0, 599))
                .color(33, new WIFColorTripplet(368, 266, 799))
                .color(34, new WIFColorTripplet(266, 399, 799))
                .color(35, new WIFColorTripplet(266, 799, 799))
                .color(36, new WIFColorTripplet(266, 799, 266))
                .color(37, new WIFColorTripplet(599, 799, 266))
                .color(38, new WIFColorTripplet(799, 799, 266))
                .color(39, new WIFColorTripplet(799, 532, 266))
                .color(40, new WIFColorTripplet(799, 266, 266))
                .color(41, new WIFColorTripplet(799, 266, 599))
                .color(42, new WIFColorTripplet(666, 266, 799))
                .color(43, new WIFColorTripplet(148, 0, 799))
                .color(44, new WIFColorTripplet(0, 199, 799))
                .color(45, new WIFColorTripplet(0, 799, 799))
                .color(46, new WIFColorTripplet(0, 799, 0))
                .color(47, new WIFColorTripplet(348, 799, 0))
                .color(48, new WIFColorTripplet(799, 799, 0))
                .color(49, new WIFColorTripplet(799, 399, 0))
                .color(50, new WIFColorTripplet(799, 0, 0))
                .color(51, new WIFColorTripplet(799, 0, 450))
                .color(52, new WIFColorTripplet(599, 0, 799))
                .color(53, new WIFColorTripplet(658, 532, 999))
                .color(54, new WIFColorTripplet(532, 666, 999))
                .color(55, new WIFColorTripplet(532, 999, 999))
                .color(56, new WIFColorTripplet(532, 999, 532))
                .color(57, new WIFColorTripplet(901, 999, 532))
                .color(58, new WIFColorTripplet(999, 999, 532))
                .color(59, new WIFColorTripplet(999, 697, 532))
                .color(60, new WIFColorTripplet(999, 532, 532))
                .color(61, new WIFColorTripplet(999, 532, 799))
                .color(62, new WIFColorTripplet(940, 532, 999))
                .color(63, new WIFColorTripplet(195, 133, 932))
                .color(64, new WIFColorTripplet(133, 266, 932))
                .color(65, new WIFColorTripplet(133, 932, 932))
                .color(66, new WIFColorTripplet(133, 932, 133))
                .color(67, new WIFColorTripplet(278, 932, 133))
                .color(68, new WIFColorTripplet(932, 932, 133))
                .color(69, new WIFColorTripplet(932, 321, 133))
                .color(70, new WIFColorTripplet(932, 133, 133))
                .color(71, new WIFColorTripplet(932, 133, 360))
                .color(72, new WIFColorTripplet(477, 133, 932))
                .color(73, new WIFColorTripplet(501, 399, 932))
                .color(74, new WIFColorTripplet(399, 532, 932))
                .color(75, new WIFColorTripplet(399, 932, 932))
                .color(76, new WIFColorTripplet(399, 932, 399))
                .color(77, new WIFColorTripplet(732, 932, 399))
                .color(78, new WIFColorTripplet(932, 932, 399))
                .color(79, new WIFColorTripplet(932, 666, 399))
                .color(80, new WIFColorTripplet(932, 399, 399))
                .color(81, new WIFColorTripplet(932, 399, 732))
                .color(82, new WIFColorTripplet(799, 399, 932));
        return b;
    }

    private WIFColorTable.Builder test182Colors(WIFColorTable.Builder b) {
        b.color(1, new WIFColorTripplet(999, 999, 999))
                .color(2, new WIFColorTripplet(0, 0, 0))
                .color(3, new WIFColorTripplet(121, 0, 607))
                .color(4, new WIFColorTripplet(0, 266, 999))
                .color(5, new WIFColorTripplet(0, 999, 999))
                .color(6, new WIFColorTripplet(0, 587, 0))
                .color(7, new WIFColorTripplet(466, 999, 0))
                .color(8, new WIFColorTripplet(999, 999, 0))
                .color(9, new WIFColorTripplet(999, 532, 0))
                .color(10, new WIFColorTripplet(999, 0, 0))
                .color(11, new WIFColorTripplet(999, 0, 599))
                .color(12, new WIFColorTripplet(799, 0, 999))
                .color(13, new WIFColorTripplet(481, 305, 767))
                .color(14, new WIFColorTripplet(399, 607, 999))
                .color(15, new WIFColorTripplet(399, 999, 999))
                .color(16, new WIFColorTripplet(399, 999, 399))
                .color(17, new WIFColorTripplet(787, 999, 399))
                .color(18, new WIFColorTripplet(999, 999, 399))
                .color(19, new WIFColorTripplet(999, 677, 399))
                .color(20, new WIFColorTripplet(999, 399, 399))
                .color(21, new WIFColorTripplet(999, 399, 760))
                .color(22, new WIFColorTripplet(877, 399, 999))
                .color(23, new WIFColorTripplet(121, 0, 599))
                .color(24, new WIFColorTripplet(0, 160, 599))
                .color(25, new WIFColorTripplet(0, 599, 599))
                .color(26, new WIFColorTripplet(0, 599, 0))
                .color(27, new WIFColorTripplet(278, 599, 0))
                .color(28, new WIFColorTripplet(599, 599, 0))
                .color(29, new WIFColorTripplet(599, 321, 0))
                .color(30, new WIFColorTripplet(599, 0, 0))
                .color(31, new WIFColorTripplet(599, 0, 360))
                .color(32, new WIFColorTripplet(477, 0, 599))
                .color(33, new WIFColorTripplet(368, 266, 799))
                .color(34, new WIFColorTripplet(266, 399, 799))
                .color(35, new WIFColorTripplet(266, 799, 799))
                .color(36, new WIFColorTripplet(266, 799, 266))
                .color(37, new WIFColorTripplet(599, 799, 266))
                .color(38, new WIFColorTripplet(799, 799, 266))
                .color(39, new WIFColorTripplet(799, 532, 266))
                .color(40, new WIFColorTripplet(799, 266, 266))
                .color(41, new WIFColorTripplet(799, 266, 599))
                .color(42, new WIFColorTripplet(666, 266, 799))
                .color(43, new WIFColorTripplet(148, 0, 799))
                .color(44, new WIFColorTripplet(0, 199, 799))
                .color(45, new WIFColorTripplet(0, 799, 799))
                .color(46, new WIFColorTripplet(0, 799, 0))
                .color(47, new WIFColorTripplet(348, 799, 0))
                .color(48, new WIFColorTripplet(799, 799, 0))
                .color(49, new WIFColorTripplet(799, 399, 0))
                .color(50, new WIFColorTripplet(799, 0, 0))
                .color(51, new WIFColorTripplet(799, 0, 450))
                .color(52, new WIFColorTripplet(599, 0, 799))
                .color(53, new WIFColorTripplet(658, 532, 999))
                .color(54, new WIFColorTripplet(532, 666, 999))
                .color(55, new WIFColorTripplet(532, 999, 999))
                .color(56, new WIFColorTripplet(532, 999, 532))
                .color(57, new WIFColorTripplet(901, 999, 532))
                .color(58, new WIFColorTripplet(999, 999, 532))
                .color(59, new WIFColorTripplet(999, 697, 532))
                .color(60, new WIFColorTripplet(999, 532, 532))
                .color(61, new WIFColorTripplet(999, 532, 799))
                .color(62, new WIFColorTripplet(940, 532, 999))
                .color(63, new WIFColorTripplet(195, 133, 932))
                .color(64, new WIFColorTripplet(133, 266, 932))
                .color(65, new WIFColorTripplet(133, 932, 932))
                .color(66, new WIFColorTripplet(133, 932, 133))
                .color(67, new WIFColorTripplet(278, 932, 133))
                .color(68, new WIFColorTripplet(932, 932, 133))
                .color(69, new WIFColorTripplet(932, 321, 133))
                .color(70, new WIFColorTripplet(932, 133, 133))
                .color(71, new WIFColorTripplet(932, 133, 360))
                .color(72, new WIFColorTripplet(477, 133, 932))
                .color(73, new WIFColorTripplet(501, 399, 932))
                .color(74, new WIFColorTripplet(399, 532, 932))
                .color(75, new WIFColorTripplet(399, 932, 932))
                .color(76, new WIFColorTripplet(399, 932, 399))
                .color(77, new WIFColorTripplet(732, 932, 399))
                .color(78, new WIFColorTripplet(932, 932, 399))
                .color(79, new WIFColorTripplet(932, 666, 399))
                .color(80, new WIFColorTripplet(932, 399, 399))
                .color(81, new WIFColorTripplet(932, 399, 732))
                .color(82, new WIFColorTripplet(799, 399, 932));
        return b;
    }

    private WIFColorTable.Builder test241Colors(WIFColorTable.Builder b) {
        b.color(1, new WIFColorTripplet(0, 0, 0))
                .color(2, new WIFColorTripplet(1, 65535, 19252))
                .color(3, new WIFColorTripplet(1, 65535, 21178))
                .color(4, new WIFColorTripplet(1, 65535, 21178))
                .color(5, new WIFColorTripplet(1, 65535, 23104))
                .color(6, new WIFColorTripplet(1, 65535, 25030))
                .color(7, new WIFColorTripplet(1, 65535, 26956))
                .color(8, new WIFColorTripplet(1, 65535, 26956))
                .color(9, new WIFColorTripplet(1, 65535, 28882))
                .color(10, new WIFColorTripplet(1, 65535, 30808))
                .color(11, new WIFColorTripplet(1, 65535, 32734))
                .color(12, new WIFColorTripplet(1, 65535, 32734))
                .color(13, new WIFColorTripplet(1, 65535, 34660))
                .color(14, new WIFColorTripplet(1, 65535, 36586))
                .color(15, new WIFColorTripplet(1, 65535, 38512))
                .color(16, new WIFColorTripplet(1, 65535, 38512))
                .color(17, new WIFColorTripplet(1, 65535, 40438))
                .color(18, new WIFColorTripplet(1, 65535, 42364))
                .color(19, new WIFColorTripplet(1, 65535, 44290))
                .color(20, new WIFColorTripplet(1, 65535, 44290))
                .color(21, new WIFColorTripplet(1, 65535, 46216))
                .color(22, new WIFColorTripplet(1, 65535, 48142))
                .color(23, new WIFColorTripplet(1, 65535, 50067))
                .color(24, new WIFColorTripplet(1, 65535, 50067))
                .color(25, new WIFColorTripplet(1, 65535, 51993))
                .color(26, new WIFColorTripplet(1, 65535, 53919))
                .color(27, new WIFColorTripplet(1, 65535, 55845))
                .color(28, new WIFColorTripplet(1, 65535, 55845))
                .color(29, new WIFColorTripplet(1, 65535, 57771))
                .color(30, new WIFColorTripplet(1, 65535, 59697))
                .color(31, new WIFColorTripplet(1, 65535, 65523))
                .color(32, new WIFColorTripplet(1, 63621, 65535))
                .color(33, new WIFColorTripplet(1, 61695, 65535))
                .color(34, new WIFColorTripplet(1, 61695, 65535))
                .color(35, new WIFColorTripplet(1, 59769, 65535))
                .color(36, new WIFColorTripplet(1, 57843, 65535))
                .color(37, new WIFColorTripplet(1, 55917, 65535))
                .color(38, new WIFColorTripplet(1, 55917, 65535))
                .color(39, new WIFColorTripplet(1, 53991, 65535))
                .color(40, new WIFColorTripplet(1, 52065, 65535))
                .color(41, new WIFColorTripplet(1, 50139, 65535));
        return b;
    }

    private WIFColorTable.Builder test582Colors(WIFColorTable.Builder b) {
        b.color(1, new WIFColorTripplet(999, 999, 999))
                .color(2, new WIFColorTripplet(603, 603, 603))
                .color(3, new WIFColorTripplet(0, 0, 0))
                .color(4, new WIFColorTripplet(697, 587, 587))
                .color(5, new WIFColorTripplet(348, 348, 521))
                .color(6, new WIFColorTripplet(603, 395, 505))
                .color(7, new WIFColorTripplet(728, 270, 411))
                .color(8, new WIFColorTripplet(760, 458, 489))
                .color(9, new WIFColorTripplet(822, 618, 556))
                .color(10, new WIFColorTripplet(869, 760, 603))
                .color(11, new WIFColorTripplet(936, 920, 666))
                .color(12, new WIFColorTripplet(666, 775, 587))
                .color(13, new WIFColorTripplet(129, 669, 289))
                .color(14, new WIFColorTripplet(301, 571, 536))
                .color(15, new WIFColorTripplet(505, 634, 697))
                .color(16, new WIFColorTripplet(999, 999, 999))
                .color(17, new WIFColorTripplet(787, 999, 399))
                .color(18, new WIFColorTripplet(999, 999, 399))
                .color(19, new WIFColorTripplet(999, 677, 399))
                .color(20, new WIFColorTripplet(999, 399, 399))
                .color(21, new WIFColorTripplet(999, 399, 760))
                .color(22, new WIFColorTripplet(877, 399, 999))
                .color(23, new WIFColorTripplet(121, 0, 599))
                .color(24, new WIFColorTripplet(0, 160, 599))
                .color(25, new WIFColorTripplet(0, 599, 599))
                .color(26, new WIFColorTripplet(0, 599, 0))
                .color(27, new WIFColorTripplet(278, 599, 0))
                .color(28, new WIFColorTripplet(599, 599, 0))
                .color(29, new WIFColorTripplet(599, 321, 0))
                .color(30, new WIFColorTripplet(599, 0, 0))
                .color(31, new WIFColorTripplet(599, 0, 360))
                .color(32, new WIFColorTripplet(477, 0, 599))
                .color(33, new WIFColorTripplet(368, 266, 799))
                .color(34, new WIFColorTripplet(266, 399, 799))
                .color(35, new WIFColorTripplet(266, 799, 799))
                .color(36, new WIFColorTripplet(266, 799, 266))
                .color(37, new WIFColorTripplet(599, 799, 266))
                .color(38, new WIFColorTripplet(799, 799, 266))
                .color(39, new WIFColorTripplet(799, 532, 266))
                .color(40, new WIFColorTripplet(799, 266, 266))
                .color(41, new WIFColorTripplet(799, 266, 599))
                .color(42, new WIFColorTripplet(666, 266, 799))
                .color(43, new WIFColorTripplet(148, 0, 799))
                .color(44, new WIFColorTripplet(0, 199, 799))
                .color(45, new WIFColorTripplet(0, 799, 799))
                .color(46, new WIFColorTripplet(0, 799, 0))
                .color(47, new WIFColorTripplet(348, 799, 0))
                .color(48, new WIFColorTripplet(799, 799, 0))
                .color(49, new WIFColorTripplet(799, 399, 0))
                .color(50, new WIFColorTripplet(799, 0, 0))
                .color(51, new WIFColorTripplet(799, 0, 450))
                .color(52, new WIFColorTripplet(599, 0, 799))
                .color(53, new WIFColorTripplet(658, 532, 999))
                .color(54, new WIFColorTripplet(532, 666, 999))
                .color(55, new WIFColorTripplet(532, 999, 999))
                .color(56, new WIFColorTripplet(532, 999, 532))
                .color(57, new WIFColorTripplet(901, 999, 532))
                .color(58, new WIFColorTripplet(999, 999, 532))
                .color(59, new WIFColorTripplet(999, 697, 532))
                .color(60, new WIFColorTripplet(999, 532, 532))
                .color(61, new WIFColorTripplet(999, 532, 799))
                .color(62, new WIFColorTripplet(940, 532, 999))
                .color(63, new WIFColorTripplet(195, 133, 932))
                .color(64, new WIFColorTripplet(133, 266, 932))
                .color(65, new WIFColorTripplet(133, 932, 932))
                .color(66, new WIFColorTripplet(133, 932, 133))
                .color(67, new WIFColorTripplet(278, 932, 133))
                .color(68, new WIFColorTripplet(932, 932, 133))
                .color(69, new WIFColorTripplet(932, 12, 12))
                .color(70, new WIFColorTripplet(932, 133, 133))
                .color(71, new WIFColorTripplet(932, 133, 360))
                .color(72, new WIFColorTripplet(477, 133, 932))
                .color(73, new WIFColorTripplet(501, 399, 932))
                .color(74, new WIFColorTripplet(399, 532, 932))
                .color(75, new WIFColorTripplet(399, 932, 932))
                .color(76, new WIFColorTripplet(399, 932, 399))
                .color(77, new WIFColorTripplet(732, 932, 399))
                .color(78, new WIFColorTripplet(932, 932, 399))
                .color(79, new WIFColorTripplet(932, 666, 399))
                .color(80, new WIFColorTripplet(932, 399, 399))
                .color(81, new WIFColorTripplet(932, 399, 732))
                .color(82, new WIFColorTripplet(799, 399, 932));
        return b;
    }

    private WIFColorTable.Builder test782Colors(WIFColorTable.Builder b) {
        b.color(1, new WIFColorTripplet(999, 999, 999))
                .color(2, new WIFColorTripplet(0, 0, 999))
                .color(3, new WIFColorTripplet(199, 0, 999))
                .color(4, new WIFColorTripplet(0, 266, 999))
                .color(5, new WIFColorTripplet(0, 999, 999))
                .color(6, new WIFColorTripplet(0, 999, 0))
                .color(7, new WIFColorTripplet(466, 999, 0))
                .color(8, new WIFColorTripplet(999, 999, 0))
                .color(9, new WIFColorTripplet(999, 532, 0))
                .color(10, new WIFColorTripplet(999, 0, 0))
                .color(11, new WIFColorTripplet(999, 0, 599))
                .color(12, new WIFColorTripplet(799, 0, 999))
                .color(13, new WIFColorTripplet(517, 399, 999))
                .color(14, new WIFColorTripplet(399, 607, 999))
                .color(15, new WIFColorTripplet(399, 999, 999))
                .color(16, new WIFColorTripplet(399, 999, 399))
                .color(17, new WIFColorTripplet(787, 999, 399))
                .color(18, new WIFColorTripplet(999, 999, 399))
                .color(19, new WIFColorTripplet(999, 677, 399))
                .color(20, new WIFColorTripplet(999, 399, 399))
                .color(21, new WIFColorTripplet(999, 399, 760))
                .color(22, new WIFColorTripplet(877, 399, 999))
                .color(23, new WIFColorTripplet(121, 0, 599))
                .color(24, new WIFColorTripplet(0, 160, 599))
                .color(25, new WIFColorTripplet(0, 599, 599))
                .color(26, new WIFColorTripplet(0, 599, 0))
                .color(27, new WIFColorTripplet(278, 599, 0))
                .color(28, new WIFColorTripplet(599, 599, 0))
                .color(29, new WIFColorTripplet(599, 321, 0))
                .color(30, new WIFColorTripplet(599, 0, 0))
                .color(31, new WIFColorTripplet(599, 0, 360))
                .color(32, new WIFColorTripplet(477, 0, 599))
                .color(33, new WIFColorTripplet(368, 266, 799))
                .color(34, new WIFColorTripplet(266, 399, 799))
                .color(35, new WIFColorTripplet(266, 799, 799))
                .color(36, new WIFColorTripplet(266, 799, 266))
                .color(37, new WIFColorTripplet(599, 799, 266))
                .color(38, new WIFColorTripplet(799, 799, 266))
                .color(39, new WIFColorTripplet(799, 532, 266))
                .color(40, new WIFColorTripplet(799, 266, 266))
                .color(41, new WIFColorTripplet(799, 266, 599))
                .color(42, new WIFColorTripplet(666, 266, 799))
                .color(43, new WIFColorTripplet(148, 0, 799))
                .color(44, new WIFColorTripplet(0, 199, 799))
                .color(45, new WIFColorTripplet(0, 799, 799))
                .color(46, new WIFColorTripplet(0, 799, 0))
                .color(47, new WIFColorTripplet(348, 799, 0))
                .color(48, new WIFColorTripplet(799, 799, 0))
                .color(49, new WIFColorTripplet(799, 399, 0))
                .color(50, new WIFColorTripplet(799, 0, 0))
                .color(51, new WIFColorTripplet(799, 0, 450))
                .color(52, new WIFColorTripplet(599, 0, 799))
                .color(53, new WIFColorTripplet(658, 532, 999))
                .color(54, new WIFColorTripplet(532, 666, 999))
                .color(55, new WIFColorTripplet(532, 999, 999))
                .color(56, new WIFColorTripplet(532, 999, 532))
                .color(57, new WIFColorTripplet(901, 999, 532))
                .color(58, new WIFColorTripplet(999, 999, 532))
                .color(59, new WIFColorTripplet(999, 697, 532))
                .color(60, new WIFColorTripplet(999, 532, 532))
                .color(61, new WIFColorTripplet(999, 532, 799))
                .color(62, new WIFColorTripplet(940, 532, 999))
                .color(63, new WIFColorTripplet(195, 133, 932))
                .color(64, new WIFColorTripplet(133, 266, 932))
                .color(65, new WIFColorTripplet(133, 932, 932))
                .color(66, new WIFColorTripplet(133, 932, 133))
                .color(67, new WIFColorTripplet(278, 932, 133))
                .color(68, new WIFColorTripplet(932, 932, 133))
                .color(69, new WIFColorTripplet(932, 321, 133))
                .color(70, new WIFColorTripplet(932, 133, 133))
                .color(71, new WIFColorTripplet(932, 133, 360))
                .color(72, new WIFColorTripplet(477, 133, 932))
                .color(73, new WIFColorTripplet(501, 399, 932))
                .color(74, new WIFColorTripplet(399, 532, 932))
                .color(75, new WIFColorTripplet(399, 932, 932))
                .color(76, new WIFColorTripplet(399, 932, 399))
                .color(77, new WIFColorTripplet(732, 932, 399))
                .color(78, new WIFColorTripplet(932, 932, 399))
                .color(79, new WIFColorTripplet(932, 666, 399))
                .color(80, new WIFColorTripplet(932, 399, 399))
                .color(81, new WIFColorTripplet(932, 399, 732))
                .color(82, new WIFColorTripplet(799, 399, 932));
        return b;
    }

    private InputStream getInputStream(String file) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        return classloader.getResourceAsStream(file);
    }

}
